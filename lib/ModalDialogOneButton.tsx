import * as React from 'react';
import * as mui from 'material-ui';
import { translate } from './i18n/translator';

type DataChangeProps = {
  open?: boolean;
  title?: string;
  handleYes();
  children?: React.ReactFragment;
};

type DataChangeState = {
  open?: boolean;
  requiredTextCode?: string;
};

export default class ModalDialogOneButton extends React.Component<DataChangeProps, DataChangeState> {
  state = {
    open: false,
  };

  getTitle(title: string) {
    if (title && title.trim().length > 0) {
      return title;
    } else {
      return translate('warning');
    }
  }
  render() {
    const actions = [<mui.FlatButton label="ОК" primary={true} onTouchTap={this.props.handleYes} />];

    return (
      <div>
        <mui.Dialog
          title={this.getTitle(this.props.title)}
          actions={actions}
          modal={true}
          open={this.props.open == undefined ? false : this.props.open}>
          {this.props.children}
        </mui.Dialog>
      </div>
    );
  }
}
