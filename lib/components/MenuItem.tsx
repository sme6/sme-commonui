import * as React from 'react';

interface MenuItemProps {
  onClick();
  label?: string;
  className?: string;
}

interface MenuItemState {}

export default class MenuItem extends React.Component<MenuItemProps, MenuItemState> {
  constructor(properties) {
    super(properties);
  }

  render() {
    return (
      <li className={this.props.className}>
        <a onClick={this.props.onClick}>
          <i />
          <span>{this.props.label}</span>
        </a>
      </li>
    );
  }
}
