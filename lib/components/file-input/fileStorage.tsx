import { put, del } from '../../backend';

export default class FileStorage {
  public static uploadFile(file): Promise<string> {
    return new Promise((resolve, reject) => {
      const fd = new FormData();
      fd.append('file', file);

      put('/files', fd)
        .then(response => {
          console.log('response = ' + JSON.stringify(response));
          resolve(response);
        })
        .catch(error => {
          console.log('ERROR CATCHED FROM SERVER RESPONSE: ' + JSON.stringify(error.message));
          reject(error);
        });
    });
  }

  public static deleteFile(fileUrl): Promise<string> {
    return new Promise((resolve, reject) => {
      del(`/files/${fileUrl}`)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          console.log('ERROR CATCHED FROM SERVER RESPONSE SERVER: ' + JSON.stringify(error));
          reject(error);
        });
    });
  }
}
