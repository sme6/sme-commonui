import * as React from 'react';
import LinearProgress from 'material-ui/LinearProgress';
import { translate } from '../../i18n/translator';
import './fileInput.css';

export enum UploadableFileStatus {
  Loading,
  LoadingSuccess,
}

export interface UploadableFile {
  id?: string;
  status?: UploadableFileStatus;
  name: string;
  url: string;
}

interface FileInputState {
  files: Array<UploadableFile>;
}

export enum FileType {
  IMAGE,
  ANY,
}

interface FileInputProps {
  inputId: string;
  files?: Array<UploadableFile>;
  onChange: (files: Array<UploadableFile>) => void;
  uploadFile: (rawFile) => Promise<string>;
  deleteFile: (fileUrl: string) => Promise<string>;
  buttonTitle: string;
  fileType?: FileType;
}

class FileInput extends React.Component<FileInputProps, FileInputState> {
  static defaultProps = {
    fileType: FileType.ANY,
  };

  constructor(props) {
    super(props);

    this.state = {
      files: this.props.files || new Array<UploadableFile>(),
    };
  }

  private currentId = 1;
  private fileInput: HTMLInputElement;

  /*  componentWillReceiveProps(nextProps) {
    console.log('nextProps='+JSON.stringify(nextProps))
    if (nextProps.files !== this.state.files) {
      this.setState({ files: nextProps.files || new Array<UploadableFile>()  });
    }
  }*/

  _triggerFileSelect() {
    this.fileInput.click();
  }

  _handleChange(selectedFile) {
    let validated = this.validateFileType();
    if (validated == true) {
      let uploadableFiles: Array<UploadableFile> = [];
      let uploadableFile: UploadableFile = {
        id: `${this.currentId++}`,
        status: UploadableFileStatus.Loading,
        name: selectedFile.name,
        url: null,
      };

      uploadableFiles.push(uploadableFile);
      let newFiles = [...this.state.files, ...uploadableFiles];

      this._uploadFile(uploadableFile, selectedFile);
      this.setState({ files: newFiles });
    }
  }

  validateFileType() {
    let fileName = document.getElementById('fileName').value;
    let idxDot = fileName.lastIndexOf('.') + 1;
    let extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
    switch (this.props.fileType) {
      case FileType.IMAGE:
        if (extFile == 'jpg' || extFile == 'jpeg' || extFile == 'png') {
          return true;
        } else {
          alert(translate('only_images_allowed'));
        }
      default:
        return true;
    }
  }

  _uploadFile(uploadableFile, rawFile) {
    this.props
      .uploadFile(rawFile)
      .then(fileUrl => {
        this._handleFileUploadSuccess(uploadableFile, fileUrl);
      })
      .catch(e => {
        this._handleFileUploadFailure(uploadableFile, e.message);
      });
  }

  _returnOnlyUploadedFiles() {
    let files = this.state.files.filter(file => file.status === UploadableFileStatus.LoadingSuccess);
    return files.length === 0 ? null : files;
  }

  _deleteFile(uploadableFile) {
    let files: Array<UploadableFile> = this.state.files.map(uplFile => {
      if (uplFile.id === uploadableFile.id) {
        uplFile.status = UploadableFileStatus.Loading;
      }
      return uplFile;
    });

    this.props
      .deleteFile(uploadableFile.url)
      .then(result => {
        this._handleFileDeletionSuccess(uploadableFile);
      })
      .catch(error => {
        this._handleFileDeletionFailure(uploadableFile, error.message);
      });
  }

  _deleteFileFromState(uploadableFile, callbak = () => {}) {
    let files: Array<UploadableFile> = this.state.files.filter(uplFile => {
      if (uplFile.id !== uploadableFile.id) return uplFile;
    });
    this.setState({ files }, () => {
      callbak();
    });
  }

  _handleFileUploadSuccess(uploadableFile, fileUrl) {
    let files: Array<UploadableFile> = this.state.files.map(uplFile => {
      if (uplFile.id === uploadableFile.id) {
        uplFile.status = UploadableFileStatus.LoadingSuccess;
        uplFile.url = fileUrl;
      }
      return uplFile;
    });
    this.setState({ files });
    console.log(`Success upload:\n ${JSON.stringify(this.state)}`);
    this.props.onChange(this._returnOnlyUploadedFiles()); // we give only successfully uploaded files to the parent componenet
  }

  _handleFileUploadFailure(uploadableFile, message) {
    this._deleteFileFromState(uploadableFile);
    alert(`Не удалось загрузить файл:\n${uploadableFile.name}\n\nПричина: ${message}\n\nПопробуйте чуть позже!`);
  }

  _handleFileDeletionSuccess(uploadableFile) {
    this._deleteFileFromState(uploadableFile, () => {
      console.log(`Success delete:\n ${JSON.stringify(this.state)}`);
      this.props.onChange(this._returnOnlyUploadedFiles()); // if a file gets deleted from the list, then the parent component should be aware of it
    });
  }

  _handleFileDeletionFailure(uploadableFile, message) {
    let files: Array<UploadableFile> = this.state.files.map(uplFile => {
      if (uplFile.id === uploadableFile.id) {
        uplFile.status = UploadableFileStatus.LoadingSuccess;
      }
      return uplFile;
    });
    this.setState({ files });
    alert(`Не удалось удалить файл:\n${uploadableFile.name}\n\nПричина: ${message}\n\nПопробуйте чуть позже!`);
  }

  _renderFileUploadingIcon(uploadableFile) {
    if (uploadableFile.status == UploadableFileStatus.Loading) {
      return <LinearProgress mode="indeterminate" />;
    }
  }

  _renderDeleteIcon(uploadableFile) {
    if (uploadableFile.status == UploadableFileStatus.LoadingSuccess) {
      return (
        <a
          href="#"
          className="fileStatusIcon"
          onClick={e => {
            e.preventDefault();
            this._deleteFile(uploadableFile);
          }}>
          X
        </a>
      );
    }
  }

  _renderFiles() {
    return this.state.files.map((uploadableFile, index) => {
      return (
        <div className="selectedFilesHolder" key={`file-${this.props.inputId}-${uploadableFile.id}`}>
          {this._renderDeleteIcon(uploadableFile)}
          <label>${uploadableFile.name}</label>
          {this._renderFileUploadingIcon(uploadableFile)}
        </div>
      );
    });
  }

  render() {
    return (
      <div>
        {this._renderFiles()}
        <input
          type="file"
          id="fileName"
          style={{ display: 'none' }}
          accept={this.props.fileType == FileType.IMAGE ? '.jpg,.jpeg,.png' : '.*'}
          onChange={e => {
            this._handleChange(e.target.files[0]);
          }}
          ref={input => {
            this.fileInput = input;
          }}
        />
        <button
          className="chooseFile"
          onClick={e => {
            e.preventDefault();
            this._triggerFileSelect();
          }}>
          {this.props.buttonTitle}
        </button>
      </div>
    );
  }
}

export default FileInput;
