import * as React from 'react';
import { CSSProperties } from 'react';

interface DialogProps {
  open: boolean;
  className?: string;
  style?: React.CSSProperties;
  actionsContainerClassName?: string;
  actionsContainerStyle?: React.CSSProperties;
  actions?: Array<React.ReactElement<any>>;
  bodyStyle?: React.CSSProperties;
  bodyClassName?: string;
  titleClassName?: string;
  titleStyle?: React.CSSProperties;
  title?: React.ReactNode;
  consultBlock?: React.CSSProperties;
  consultBlockClassName?: string;

  /* contentClassName?: string;
       contentStyle?: React.CSSProperties;

       overlayClassName?: string;
       overlayStyle?: React.CSSProperties;       */
  imageIcon?: any;

  children?: React.ReactFragment;
}

interface DialogState {
  requiredTextCode?: string;
}

const styles = {
  main: {
    width: '50%',
    maxWidth: '500px',
    height: 'auto',
    position: 'fixed',
    top: '40%',
    left: '25%',
    opacity: 1,
    textAlign: 'left',
    overflow: 'visible',
  } as CSSProperties,
  header: {
    color: 'rgba(178, 12, 0, .85)',
    textAlign: 'center',
    fontSize: 'larger',
  } as CSSProperties,
  body: {
    color: '#484848',
    textAlign: 'center',
  } as CSSProperties,
  label: {
    height: '100%',
    float: 'left',
  } as CSSProperties,
  btns: {
    boxSizing: 'border-box',
    padding: '8px',
    textAlign: 'right',
    marginLeft: 'auto',
    marginTop: '0px',
    borderTop: 'none',
    display: 'flex',
    WebkitJustifyContent: 'flex-end',
  } as CSSProperties,
};

export default class ModalDialog extends React.Component<DialogProps, DialogState> {
  constructor(properties) {
    super(properties);
  }

  render() {
    return (
      this.props.open && (
        <div style={this.props.style !== undefined ? this.props.style : styles.main} className={this.props.className}>
          <div className={this.props.consultBlockClassName || 'consult-block'} style={this.props.consultBlock}>
            <div className="consult-panel">
              <div className="consult-label"> {this.props.imageIcon} </div>
              <div className="consult-panel-ctrl">{/*<div className="consult-close"></div>*/}</div>
            </div>
            <div className="consult-footer">
              <div
                className={this.props.titleClassName}
                style={this.props.titleStyle !== undefined ? this.props.titleStyle : styles.header}>
                <b>{this.props.title}</b>
              </div>
              <div
                style={this.props.bodyStyle !== undefined ? this.props.bodyStyle : styles.body}
                className={this.props.bodyClassName}>
                {this.props.children}
              </div>
              <div className={this.props.actionsContainerClassName}>
                <div
                  style={
                    this.props.actionsContainerStyle !== undefined ? this.props.actionsContainerStyle : styles.btns
                  }>
                  {this.props.actions}
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    );
  }
}
