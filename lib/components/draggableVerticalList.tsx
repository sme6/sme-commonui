import * as React from 'react';
import ReactDragList from 'react-drag-list';
import './draggableVerticalList.css';

export interface DraggableListItem {
  id: number;
  code?: string;
  title: string;
  icon?: any;
}

export interface DraggableVerticalListsProps {
  shouldShowIndices?: boolean;
  items: Array<DraggableListItem>;
  onIconTapped?: (item) => void;
  onChange: (items: Array<DraggableListItem>) => void;
  indexOffset?: number;
}

interface DraggableVerticalListsState {
  items: DraggableListItem[];
}

export class DraggableVerticalList extends React.Component<DraggableVerticalListsProps, DraggableVerticalListsState> {
  constructor(props) {
    super(props);
    this.state = { items: this.props.items };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ items: nextProps.items });
  }

  render() {
    const items = this.state.items;
    return (
      <div className="draggableList vertical">
        <ReactDragList
          dataSource={items}
          rowKey="id"
          row={(record: DraggableListItem, index) => this.itemRenderer(record, index)}
          handles={false}
          className="simple-drag"
          rowClassName="simple-drag-row"
          onUpdate={(event, reorderedItems: Array<DraggableListItem>) => this.handleRLDDChange(reorderedItems)}
        />
      </div>
    );
  }

  private itemRenderer = (item: DraggableListItem, index: number): JSX.Element => {
    let resultIndex = this.props.shouldShowIndices
      ? index + (this.props.indexOffset > 0 ? this.props.indexOffset : 0) + '. '
      : '';
    return (
      <div className="item">
        <div className="body">
          <p className="title">
            {resultIndex}
            {item.title}
          </p>
        </div>
        <div
          className="icon"
          onClick={() => {
            this.props.onIconTapped(item);
          }}>
          {item.icon}
        </div>
      </div>
    );
  };

  private handleRLDDChange = (reorderedItems: Array<DraggableListItem>) => {
    this.setState({ items: reorderedItems });
    this.props.onChange(reorderedItems);
  };
}
