import InfiniteScrollSelect from './infinite-scroll-select.component';
import { HttpParamsOptions } from '../../models/models';

export interface InfiniteScrollSelectClassNames {
  wrapper: string;
  searchWrapper: string;
  searchInput: string;
  searchArrow: string;
  dropdown: string;
  dropdownItem: string;
  dropdownItemLabel: string;
  dropdownItemDescription: string;
}

export interface InfiniteScrollSelectProps<R> {
  endPoint: string;
  label: string | ((data: R) => string);
  size?: number;
  loader?: JSX.Element;
  onChange?: (data: R) => void;
  description?: string | ((data: R) => string);
  additionalParams?: HttpParamsOptions;
  classNames?: Partial<InfiniteScrollSelectClassNames>;
}

export interface InfiniteScrollSelectState<R> {
  data: R[];
  page: number;
  total: number;
  text: string;
  showDropdown: boolean;
  loading: boolean;
  context: InfiniteScrollSelect<R>;
}
