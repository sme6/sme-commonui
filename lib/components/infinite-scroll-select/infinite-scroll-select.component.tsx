import React, { Component } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import debounce from 'lodash/debounce';

import { GridResponse } from '../../grid/grid';
import { translate } from '../../i18n/translator';
import { cls } from '../../layout/views';
import { get } from '../../backend';
import { HttpParamsOptions } from '../../models/models';

import {
  InfiniteScrollSelectClassNames,
  InfiniteScrollSelectProps,
  InfiniteScrollSelectState,
} from './infinite-scroll-select.interfaces';
import './infinite-scroll-select.component.scss';

export default class InfiniteScrollSelect<R> extends Component<
  InfiniteScrollSelectProps<R>,
  Partial<InfiniteScrollSelectState<R>>
> {
  static defaultProps = {
    size: 20,
    loader: (
      <li className={'infinite-scroll-select-wrapper__dropdown-item'}>
        <h4>{translate('ib.loading')}</h4>
      </li>
    ),
    classNames: {},
  };

  private get dropdown() {
    if (!this.state.loading && !this.state.data.length) {
      return (
        <li className={'infinite-scroll-select-wrapper__dropdown-item'}>
          <h4>{translate('common_noData')}</h4>
        </li>
      );
    }

    return this.state.data.map(data => {
      return (
        <li
          className={this.getClassName('infinite-scroll-select-wrapper__dropdown-item', 'dropdownItem')}
          onClick={e => {
            this.setState({
              text: this.props.label instanceof Function ? (this.props.label(data) as string) : data[this.props.label],
              showDropdown: false,
            }),
              this.props.onChange(data);
          }}>
          <span
            className={this.getClassName('infinite-scroll-select-wrapper__dropdown-item-label', 'dropdownItemLabel')}>
            {this.props.label instanceof Function ? (this.props.label(data) as string) : data[this.props.label]}
          </span>
          {this.props.description && (
            <span
              className={this.getClassName(
                'infinite-scroll-select-wrapper__dropdown-item-description',
                'dropdownItemDescription',
              )}>
              {this.props.description instanceof Function
                ? (this.props.description(data) as string)
                : data[this.props.description]}
            </span>
          )}
        </li>
      );
    });
  }

  private getClassName = (always: string, optionalVariableName: keyof InfiniteScrollSelectClassNames): string => {
    const optionalVariable = this.props.classNames[optionalVariableName] || '';
    return cls({ [optionalVariable]: !!optionalVariable }, always);
  };

  context = null;
  inputRef: HTMLElement;
  dropdownRef: HTMLElement;

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      page: 0,
      total: 0,
      text: '',
      showDropdown: false,
      loading: false,
      context: this,
    };
  }

  componentDidMount() {
    document.body.addEventListener('click', this.documentEventListener, false);
  }

  componentWillUnmount() {
    this.context = null;
    document.body.removeEventListener('click', this.documentEventListener, false);
  }

  documentEventListener = e => {
    if (!this.state.context) return;
    if (
      this.state.context.inputRef &&
      this.state.context.dropdownRef &&
      (this.state.context.inputRef.contains(e.target) || this.state.context.dropdownRef.contains(e.target))
    ) {
      return;
    }
    this.state.context.hide();
  };

  fetchData = (page?: number) => {
    this.setState(
      {
        loading: true,
        page: page != undefined ? page : this.state.page + 1,
      },
      () => {
        const params: HttpParamsOptions = {
          page: this.state.page,
          size: this.props.size,
          ...this.props.additionalParams,
          ...(this.state.text && { searchText: this.state.text }),
        };

        get(this.props.endPoint, params)
          .then((data: GridResponse<R>) => {
            this.setState(
              {
                loading: false,
                data: this.state.data.concat(data.rows) || [],
                total: data.total && data.total.count ? data.total.count : 0,
              },
              () => {
                this.setState({
                  showDropdown: this.state.data.length > 0,
                });
              },
            );
          })
          .catch(() => {
            this.setState({
              loading: false,
              data: [],
              total: 0,
              page: 0,
              showDropdown: true,
            });
          });
      },
    );
  };

  onInputChange = debounce((value: string) => {
    this.props.onChange(null);
    this.setState(
      {
        page: 0,
        total: 0,
        data: [],
      },
      () => {
        this.fetchData(0);
      },
    );
  }, 400);

  hide = () => {
    this.setState({ showDropdown: false, data: [], page: 0, total: 0 });
  };

  render() {
    return (
      <div className={this.getClassName('infinite-scroll-select-wrapper', 'wrapper')}>
        <div
          className={this.getClassName('infinite-scroll-select-wrapper__search-wrapper', 'searchWrapper')}
          onClick={e => {
            e.preventDefault();
            if (!this.state.showDropdown) {
              this.setState(
                {
                  page: 0,
                  total: 0,
                  data: [],
                  showDropdown: true,
                },
                () => {
                  this.fetchData(0);
                },
              );
            }
          }}
          ref={ref => (this.inputRef = ref)}>
          <input
            className={this.getClassName('infinite-scroll-select-wrapper__search-input', 'searchInput')}
            type="text"
            value={this.state.text}
            onChange={e => {
              e.preventDefault();
              this.setState({
                text: e.target.value,
              });
              this.onInputChange(e.target.value);
            }}
          />
          <span
            className={cls(
              { open: this.state.showDropdown },
              this.getClassName('infinite-scroll-select-wrapper__search-arrow', 'searchArrow'),
            )}
            onClick={() => {
              this.setState({
                showDropdown: !this.state.showDropdown,
              });
            }}
          />
        </div>
        {this.state.showDropdown && (
          <ul
            className={this.getClassName('infinite-scroll-select-wrapper__dropdown', 'dropdown')}
            ref={ref => (this.dropdownRef = ref)}>
            <InfiniteScroll
              dataLength={this.state.data.length}
              next={this.fetchData}
              hasMore={this.state.total ? this.state.data.length < this.state.total : true}
              height={300}
              loader={this.state.loading && this.props.loader}
              children={this.dropdown}
            />
          </ul>
        )}
      </div>
    );
  }
}
