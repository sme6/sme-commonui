import React from 'react';
import './smebank-spinner.scss';

export default class SmebankSpinner extends React.Component<{ className?: string }, never> {
  constructor(properties) {
    super(properties);
  }

  render() {
    return (
      <div className={`smebank-spinner ${this.props.className || ''}`}>
        <div className="snebank-spinner__preloader">
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
          <div />
        </div>
      </div>
    );
  }
}
