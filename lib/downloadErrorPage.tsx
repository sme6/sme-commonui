import * as React from 'react';
import { Paper } from 'material-ui';
import { Header, Content } from './layout/views';

export class DownloadErrorPage extends React.Component<any, any> {
  constructor(properties) {
    super(properties);
  }

  render() {
    let msg = this.getParameterByName('msg');

    return (
      <Paper>
        <Header text={msg || 'Произошла ошибка закачки файла'} />
        <Content>{/*{ 'Произошла ошибка закачки файла' }*/}</Content>
      </Paper>
    );
  }

  getParameterByName = name => {
    let url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  };
}
