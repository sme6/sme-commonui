import { Translation2, translate } from './i18n/translator';

export interface Dictionary {
  code: string;
  id: number;
  label: string;
  subCode: string;
  subLabel: string;
}

export enum CodeCategory {
  DocumentState,
  DocumentStateShow,
  DocumentType,
  DomesticTransferType,
  DocumentTypeShow,
  BudgetClassification,
  EmployeeTransferCategory,
  FeeType,
  EmployeeReturnStatus,
  CustomRequestCategory,
  Currency,
  NationalCurrency,
  AccountType,
  CardType,
  CardStatusShow,
  CardLockReason,
  CardUnlockReason,
  PaymentPurposeCode,
  ChannelType,
  ManagerAction,
  CustomerAction,
  ResidencyAndEconomic,
  LimitType,
  SignLevel,
  CertificateType,
  OrganizationRank,
  OtpTokenStatus,
  CreditStatus,
  DepositeProductType,
  CreditProductType,
  CreditPaymentType,
  CardProductType,
  SystemModule,
  Regions,
  AccountStatus,
  CurrencyOperationType,
  ExternalSystem,
  PropertyType,
  District,
  InternationalDistrict,
  LetterType,
  EventType,
  CommissionType,
  UserType,
  City,
  CustomRequestFieldType,
  LoanTerm,
  CustomRequestSource,
  CustomRequestSourceCode,
  CustomRequestFieldsReference,
  UserStatus,
  BooleanType,
  DemandEndPoint,
  RelatedButton,
  OrganizationRole,
  StandingType,
  PersonType,
  ProductState,
  DemandState,
  CardTransferType,
  ForeignExchangeControlState,
}

export interface Option {
  id?: number;
  value?: string;
  label?: string;
}

export interface Code {
  id?: number;
  code: string;
  label: string;
}

/**
 * Copy of com.infin.it.ibank.service.FactoryService.Type enum
 */
export enum DocumentTypeRequestParam {
  counterparty_foreign,
  counterparty_national,
  document_type,
  document_state,
  persons_revoke,
  document_revoke,
  return_employee,
  return_document,
  document_type_documentwithdraws,
  document_type_standingorders,
}

export enum DocumentSubType {
  PaymentOrder,
  Payroll,
  PensionContribution,
  SocialContribution,
  P2P,
  MedicalContribution,
  MandatoryPensionContribution,
  ProfessionalPensionContribution,
  VoluntaryPensionContribution,
}

export enum DocType {
  AccountTransfer,
  DomesticTransfer,
  InternationalTransfer,
  DocumentWithdraw,
  ExposedOrder,
  Other,
  Demand,
  StandingOrder,
}

export interface CustomerUser {
  id?: number;
  deleted?: string;
  login?: string;
  fullName?: string;
  phone?: string;
  email?: string;
  require_sms?: boolean;
}

export interface PersonalCustomerUser {
  id?: number;
  deleted?: string;
  fio?: string;
  registrationAddress?: string;
  locationAddress?: string;
  deliveryAddress?: string;
  birthdayDate?: string;
  mobileTelephone?: string;
  homeTelephone?: string;
  workTelephone?: string;
  email?: string;
  iin?: string;
  facebook_login?: string;
  require_sms?: boolean;
}

export interface Personal {
  id?: number;
  company?: { id: number; organization: { name?: string } };
}

export interface CompanyPersonByUserAccountDTO {
  id: number;
  name?: string;
  taxCode?: string;
  blocked?: boolean;
  region?: string;
}

export interface CompanyPerson {
  id: number;
  deleted?: boolean;
  roles?: Role[];
  limits?: Limit[];
  authFactors?: AuthFactor[];
  resultPrivileges?: number[];
  sign_level?: string;
  organizationRole?: string;
  valid_from?: Date;
  valid_to?: Date;
  isRegisteredOnVC?: boolean;
  dn?: string;

  certificate?: string;
  certifValidFrom?: string;
  certifValidTo?: string;
  certificateSerialNumber?: string;
  tempCertificate?: boolean;
  company?: {
    id: number;
    organization?: {
      name?: string;
      taxCode?: string;
      blocked?: boolean;
      region?: string;
    };
  };
  userAccount?: UserAccount;
  otp?: OTPToken;
}

export interface UserAccount {
  id?: number;
  fullName?: string;
  login?: string;
  iin?: string;
}

export interface OTPToken {
  id?: number;
  tokenType?: string;
  serial?: string;
}

export interface AuthFactor {
  id?: number;
  deleted?: boolean;
  valid_from?: Date;
  valid_to?: Date;
  signAllow?: boolean;
  auth_factor?: Factor;
  roles?: number[];
  uuid?: string;
}

export interface Factor {
  id: number;
  code: string;
  name: string;
}

export interface Limit {
  id?: number;
  deleted?: boolean;
  amount?: number;
  currency?: string;
  currencyCode?: Code;
  type?: string;
  typeCode?: Code;
}

export interface Role {
  id: number;
  code?: string;
  name?: string;
}

export interface Currency {
  id: number;
  isoCode?: string;
  name?: string;
  fullName?: string;
  symbol?: string;
  digitalCode?: string;
  isCurrency?: boolean;
}

export interface CurrencyExt {
  id: number;
  isoCode: string;
  name: string;
  fullName: Translation2;
  symbol: string;
  digitalCode: string;
}

export enum DocumentState {
  Creating,
  New,
  Signed,
  Accepted,
  AcceptedInABS,
  AcceptedInRBS,
  Active,
  AwaitsSignature,
  Deleted,
  Draft,
  Executed,
  NotAcceptedInABS,
  Paid,
  Pay,
  Refused,
  Return,
  Revoked,
  SentForRevokation,
  SentToKISC,
  SentToRBS,
  SentToReceiver,
  Suspended,
}

export enum EmployeeTransferCategory {
  C,
  V,
  D,
}

export function getCodeLabel(codes: Option[], code: string) {
  let result: string = '';
  codes.map((key: Option) => {
    if (key != null && key.value === code) {
      result = key.label;
    }
  });
  return result;
}

export function getCodeLabel2(codes: Code[], code: string) {
  let result: string = '';
  codes.map((key: Code) => {
    if (key != null && key.code === code) {
      result = key.label;
    }
  });
  return result;
}

export const creatingDocumentStateCode = {
  label: 'В процессе создания',
  code: DocumentState[DocumentState.Creating],
};

export enum DocState {
  New,
  Draft,
  AwaitsSignature,
  Signed,
  Deleted,
  SentToRBS,
  AcceptedInRBS,
  AcceptedInABS,
  NotAccepted,
  Revoked,
  Executed,
  Paid,
  Refused,
  Updated,
  Return,
}

export enum ProductState {
  Active = 1,
  Closed = 2,
  Blocked = 3,
  Arrested = 4,
  Card = 5,
}

export enum CardState {
  NotActive,
  Opened,
  Lost,
  Stolen,
  Limited,
  VIP,
  Inner,
  Compromised = 8,
  Closed = 9,
  BeforeClarifying = 'A',
  Ordered = 'C',
  Overdue = 'F',
}

export enum ForeignExchangeState {
  Registered = '05',
  ToChange = '06',
  ToRegister = '07',
  Decorated = '08',
}

export const getStatusCode = (item: { status: { code: string } }, stateEnum: any = ProductState): string => {
  if (item && item.status) {
    const statusCode = stateEnum[item.status.code];
    return statusCode && statusCode.toLowerCase();
  } else {
    return '';
  }
};

export const DocumentStates = [
  { value: '', label: 'all_states' },
  { value: ['Draft'], label: 'domTransfer_status_new' },
  { value: ['AwaitsSignature'], label: 'domTransfer_status_waitForSign' },
  { value: ['Signed'], label: 'domTransfer_status_waitForSend' },
  { value: ['Accepted', 'SentToRBS', 'AcceptedInRBS', 'AcceptedInABS'], label: 'domTransfer_status_process' },
  { value: ['Executed'], label: 'domTransfer_status_exec' },
  { value: ['Revoked'], label: 'domTransfer_status_recall' },
  { value: ['NotAcceptedInABS', 'Refused'], label: 'domTransfer_status_error' },
  { value: ['OnRevision'], label: 'state_on_revision' },
  { value: ['Annulled'], label: 'ib.stateAnnulled' },
];

export const DocumentStatesSMEx = [
  { value: '', label: 'ib.allStatuses' },
  { value: ['AwaitsSignature'], label: 'ib.statusWaitForSign' },
  { value: ['Executed'], label: 'ib.statusExecuted' },
  { value: ['Closed'], label: 'ib.statusClosed' },
  { value: ['Error'], label: 'ib.statusError' },
  { value: ['ExecuteInFuture'], label: 'ib.statusExecuteInFuture' },
];

export const DocumentStatesExpo = [
  { value: '', label: 'ib.allStatuses' },
  { value: ['AwaitsSignature'], label: 'ib.statusWaitForSign' },
  { value: ['SentToRBS'], label: 'ib.statusSentToRBS' },
  { value: ['AcceptedInRBS', 'AcceptedInABS'], label: 'ib.statusAccepted' },
  { value: ['Executed'], label: 'ib.statusExecuted' },
  { value: ['Revoked'], label: 'ib.statusRevoked' },
  { value: ['NotAcceptedInABS', 'Refused'], label: 'ib.statusRefused' },
];

export interface TransactionHistory {
  id: number;
  documentNumber?: number;
  date?: Date;
  payer?: string;
  payerAccount?: string;
  payerBin?: number;
  payerResidencyCode?: number;
  payerBankBik?: number;
  payerBank?: string;
  receiver?: string;
  receiverAccount?: string;
  receiverBin?: number;
  receiverResidencyCode?: number;
  receiverAccountBik?: string;
  receiverBank?: string;
  amount?: number;
  currency?: string;
  purposeCode?: string;
  knpDesc?: string;
  paymentPurpose?: string;
  currentAccount?: string;
}

export interface CommonOneFieldDTO {
  value: any;
}

export interface History {
  id: number;
  fullName?: string;
  actionTime?: string;
  fromState?: string;
  toState?: string;
  description?: string;
  fromStateLabel?: string;
  toStateLabel?: string;
}

export interface LetterNotification {
  messageText: string;
  cmd: string;
}

export interface Letter {
  id: number;
  desc?: string;
  sendStatus?: number;
  documentStatus?: string;
  subject?: string;
  customerName?: string;
  organizationName?: string;
  managerName?: string;
  date?: string;
  attachmentFiles?: MessageAttachment[];
  isFromCustomer?: boolean;
  customerTaxCode?: string;
  customerExternalId?: string;
}

export interface MessageAttachment {
  id?: number;
  name?: string;
  contentType?: string;
}
