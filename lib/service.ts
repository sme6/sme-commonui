import moment from 'moment';

/*** Get the largest date from the list of moment dates(+current day) (format DD.MM.YYYY) ***/
export const getMaxDate = (...datesList) => {
  const dates = [...datesList, moment(new Date()).format('DD.MM.YYYY')]; // add current day
  return moment(
    dates
      .filter(Boolean) // remove all falsy values
      .map(item => moment(item, 'DD.MM.YYYY').toDate()) // convert to Date[]
      .filter(Boolean) // remove all falsy values
      .reduce((a, b) => (a > b ? a : b)), // reduce to max Date object
  ).format('DD.MM.YYYY'); // convert back to string
};
