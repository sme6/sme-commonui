import React from 'react';
import { post } from './backend';
import { translate } from './i18n/translator';

export const ConstantName = {
  // integer constants
  OTP_TOKEN_LENGTH: 'OTP_TOKEN_LENGTH',
  LOOK_AHEAD_RESYNCHRONIZATION_WINDOW: 'LOOK_AHEAD_RESYNCHRONIZATION_WINDOW',
  INCORRECT_ATTEMPTS: 'INCORRECT_ATTEMPTS',
  PWD_NOTIFICATION: 'PWD_NOTIFICATION',
  PWD_LIFE: 'PWD_LIFE',
  PWD_INCORRECT_ATTEMPTS: 'PWD_INCORRECT_ATTEMPTS',
  PWD_HISTORY_DEEP: 'PWD_HISTORY_DEEP',
  MAX_PWD_LENGTH: 'MAX_PWD_LENGTH',
  MIN_PWD_LENGTH: 'MIN_PWD_LENGTH',
  MAX_LOGIN_LENGTH: 'MAX_LOGIN_LENGTH',
  MIN_LOGIN_LENGTH: 'MIN_LOGIN_LENGTH',
  FILE_SIZE: 'FILE_SIZE',
  SESSION_TIME: 'SESSION_TIME',
  DEFAULT_TARIFF: 'DEFAULT_TARIFF',
  USER_BLOCK_TIME: 'USER_BLOCK_TIME',
  LETTER_BODY_SIZE: 'LETTER_BODY_SIZE',
  LETTER_SUBJECT_SIZE: 'LETTER_SUBJECT_SIZE',
  SMSC_PORT: 'SMSC_PORT',
  SMSC_TON: 'SMSC_TON',
  SMSC_NPI: 'SMSC_NPI',
  ACCOUNT_STM: 'ACCOUNT_STM',
  CARD_STM: 'CARD_STM',
  SMS_VALID_PERIOD: 'SMS_VALID_PERIOD',
  SMS_PASSWORD_LENGTH: 'SMS_PASSWORD_LENGTH',
  // default timeout for integration requests (mainly used in realtime synchronization)
  INTEGRATION_REQUEST_DEFAULT_TIME: 'INTEGRATION_REQUEST_DEFAULT_TIME',

  // string constants
  VAT_CODE: 'VAT_CODE',
  CURRENCY_CODE: 'CURRENCY_CODE',
  COUNTRY_CODE: 'COUNTRY_CODE',
  BANK_ADDRESS: 'BANK_ADDRESS',
  OKPO_CODE: 'OKPO_CODE',
  KBE: 'KBE',
  BIN: 'BIN',
  BIK: 'BIK',
  SHORT_BANK_NAME: 'SHORT_BANK_NAME',
  FULL_BANK_NAME: 'FULL_BANK_NAME',
  DEMAND_OPEN: 'DEMAND_OPEN',
  DEMAND_CLOSE: 'DEMAND_CLOSE',
  FILE_TYPE: 'FILE_TYPE',
  PROFILE: 'PROFILE',
  PROFILE_PASSWORD: 'PROFILE_PASSWORD',
  KEY_SERIAL_NUMBER: 'KEY_SERIAL_NUMBER',
  NEWS_ACTUALITY: 'NEWS_ACTUALITY',
  MAX_VALUE_DAYS: 'MAX_VALUE_DAYS',
  ALLOWED_PWD_SYMBOLS: 'ALLOWED_PWD_SYMBOLS',
  EUR_CORRESPONDENT_BANK_NAME: 'EUR_CORRESPONDENT_BANK_NAME',
  USD_CORRESPONDENT_BANK_NAME: 'USD_CORRESPONDENT_BANK_NAME',
  GBP_CORRESPONDENT_BANK_NAME: 'GBP_CORRESPONDENT_BANK_NAME',
  RUB_CORRESPONDENT_BANK_NAME: 'RUB_CORRESPONDENT_BANK_NAME',
  CHF_CORRESPONDENT_BANK_NAME: 'CHF_CORRESPONDENT_BANK_NAME',
  EUR_CORRESPONDENT_BANK_SWIFT: 'EUR_CORRESPONDENT_BANK_SWIFT',
  USD_CORRESPONDENT_BANK_SWIFT: 'USD_CORRESPONDENT_BANK_SWIFT',
  GBP_CORRESPONDENT_BANK_SWIFT: 'GBP_CORRESPONDENT_BANK_SWIFT',
  RUB_CORRESPONDENT_BANK_SWIFT: 'RUB_CORRESPONDENT_BANK_SWIFT',
  CHF_CORRESPONDENT_BANK_SWIFT: 'CHF_CORRESPONDENT_BANK_SWIFT',
  EUR_CORRESPONDENT_BANK_ACCOUNT_NUMBER: 'EUR_CORRESPONDENT_BANK_ACCOUNT_NUMBER',
  USD_CORRESPONDENT_BANK_ACCOUNT_NUMBER: 'USD_CORRESPONDENT_BANK_ACCOUNT_NUMBER',
  GBP_CORRESPONDENT_BANK_ACCOUNT_NUMBER: 'GBP_CORRESPONDENT_BANK_ACCOUNT_NUMBER',
  RUB_CORRESPONDENT_BANK_ACCOUNT_NUMBER: 'RUB_CORRESPONDENT_BANK_ACCOUNT_NUMBER',
  CHF_CORRESPONDENT_BANK_ACCOUNT_NUMBER: 'CHF_CORRESPONDENT_BANK_ACCOUNT_NUMBER',
  CURRENCY_BUY_PURPOSE_CODE: 'CURRENCY_BUY_PURPOSE_CODE',
  CURRENCY_SELL_PURPOSE_CODE: 'CURRENCY_SELL_PURPOSE_CODE',
  SMSC_HOST: 'SMSC_HOST',
  SMSC_USER: 'SMSC_USER',
  SMSC_PASSWORD: 'SMSC_PASSWORD',
  EDS_URL_OCSP: 'EDS_URL_OCSP',
  EDS_URL_LDAP: 'EDS_URL_LDAP',
  EDS_URL_WEBRA: 'EDS_URL_WEBRA',
  EDS_DEFAULT_DEVICE: 'EDS_DEFAULT_DEVICE',
  EDS_DEFAULT_PROFILE: 'EDS_DEFAULT_PROFILE',
  EDS_DEFAULT_PERSON_PROFILE: 'EDS_DEFAULT_PERSON_PROFILE',
  PROVIDER_SERVICE_LOGO_URL: 'PROVIDER_SERVICE_LOGO_URL',
  CREATE_ACCOUNT_FROM_CREDIT: 'CREATE_ACCOUNT_FROM_CREDIT',
  CREATE_ACCOUNT_FROM_DEPOSIT: 'CREATE_ACCOUNT_FROM_DEPOSIT',
  CREATE_ACCOUNT_FROM_CARD: 'CREATE_ACCOUNT_FROM_CARD',
  SMS_ENGINE: 'SMS_ENGINE',
  DEFAULT_CHAT_PHONE_NUMBER: 'DEFAULT_CHAT_PHONE_NUMBER',
  DEFAULT_CHAT_MANAGER_NAME: 'DEFAULT_CHAT_MANAGER_NAME',

  // boolean constant
  SYNC_ORGANIZATION_ON_LOGIN: 'SYNC_ORGANIZATION_ON_LOGIN',
  SYNC_ACCOUNTS_ON_LOGIN: 'SYNC_ACCOUNTS_ON_LOGIN',
  SYNC_CARDS_ON_LOGIN: 'SYNC_CARDS_ON_LOGIN',
  SYNC_CREDITS_ON_LOGIN: 'SYNC_CREDITS_ON_LOGIN',
  SYNC_DEPOSITS_ON_LOGIN: 'SYNC_DEPOSITS_ON_LOGIN',
  SYNC_ACCOUNT_TRANSACTIONS_ON_VIEW: 'SYNC_ACCOUNT_TRANSACTIONS_ON_VIEW',
  SYNC_CARD_TRANSACTIONS_ON_VIEW: 'SYNC_CARD_TRANSACTIONS_ON_VIEW',
  SYNC_CREDIT_SCHEDULES_ON_VIEW: 'SYNC_CREDIT_SCHEDULES_ON_VIEW',
  SYNC_DEPOSIT_TRANSACTIONS_ON_VIEW: 'SYNC_DEPOSIT_TRANSACTIONS_ON_VIEW',
  SYNC_ACCOUNTS_BY_DOC_STATE: 'SYNC_ACCOUNTS_BY_DOC_STATE',
  SYNC_CARDS_BY_DOC_STATE: 'SYNC_CARDS_BY_DOC_STATE',
  SYNC_CREDITS_BY_DOC_STATE: 'SYNC_CREDITS_BY_DOC_STATE',
  SYNC_DEPOSITS_BY_DOC_STATE: 'SYNC_DEPOSITS_BY_DOC_STATE',
  USER_SINGLE_SESSION: 'USER_SINGLE_SESSION',
  SHOW_SUB_DOCUMENT_TYPES: 'SHOW_SUB_DOCUMENT_TYPES',

  // SmsEngineType
  Demo: 'Demo',
  KazInfoTech: 'KazInfoTech',
  BankIntegrator: 'BankIntegrator',

  FILE_STORAGE_URL: 'http://localhost:8088/files/',
  EMPTY: '',
};

/**
 * Return object where property name equals to array item
 *
 * @param {Array<string>} constants
 * @returns {Promise<Object>}
 */
export function getConstants(constants: Array<string>): Promise<Object> {
  return post(`/constants/list`, { codes: constants }) as Promise<Object>;
}

export const mimeTypes = {
  mt: [
    {
      extension: 'txt',
      mimeTypes: [
        'text/plain',
        'application/txt',
        'browser/internal',
        'text/anytext',
        'widetext/plain',
        'widetext/paragraph',
      ],
    },
    {
      extension: 'dat',
      mimeTypes: ['application/octet-stream', 'zz-application/zz-winassoc-dat'],
    },
    {
      extension: 'grs',
      mimeTypes: ['text/plain'],
    },
  ],
  mtjde: [
    {
      extension: 'txt',
      mimeTypes: [
        'text/plain',
        'application/txt',
        'browser/internal',
        'text/anytext',
        'widetext/plain',
        'widetext/paragraph',
      ],
    },
  ],
  '1c': [
    {
      extension: 'txt',
      mimeTypes: [
        'text/plain',
        'application/txt',
        'browser/internal',
        'text/anytext',
        'widetext/plain',
        'widetext/paragraph',
      ],
    },
  ],
  xml: [
    {
      extension: 'xml',
      mimeTypes: ['text/xml', 'application/xml', 'application/x-xml'],
    },
  ],
};

export const DomTransferType = {
  paymentOrder: 'payment-order',
  payroll: 'payroll',
  socialContribution: 'social',
  pensionContribution: 'pension',
  medicalContribution: 'medical',
};

const passwordRegexText = `
Внимание!
При создании нового пароля, пожалуйста соблюдайте следующие требования:

Пароль должен содержать следующие группы символов – буквы, цифры, специальные символы на латинице:

минимум 1 заглавная буква
минимум 1 строчная буква
минимум одна цифра от 0 до 9
минимум один спец символ (*, [, !, #, $, %, ^, &, (, ), @, ])

Длина пароля должна быть не менее 8 символов

Пароль не должен соответствовать логину (номеру телефона)

Пароль не должен повторять предыдущие 10 паролей

Пароль не более 2-х повторяющихся подряд символов
`;

const splitPasswordRegexText = (text: string) => text.split('\\r\\n').filter(Boolean);

export const PasswordPolicyText = ({
  translations = splitPasswordRegexText(translate('password.regex.text', passwordRegexText)),
}) => (
  <div>
    <p>
      <b>{translations[0]}</b>
    </p>
    <p> {translations[1]} </p>
    <ul>
      <li>
        {translations[2]}
        <ul>
          <li>{translations[3]}</li>
          <li>{translations[4]}</li>
          <li>{translations[5]}</li>
          <li>{translations[6]}</li>
        </ul>
      </li>
      <li>{translations[7]}</li>
      <li>{translations[8]}</li>
      <li>{translations[9]}</li>
      <li>{translations[10]}</li>
    </ul>
  </div>
);

export const SIGNER_VERSION: number = 4;
