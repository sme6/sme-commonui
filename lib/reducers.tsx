export function reduce(reducers: any[]) {
  return function reduction(state, action) {
    return reducers.reduce((st, r) => r(st, action), state);
  };
}

export function combine(reducers: any) {
  const reducerKeys = Object.keys(reducers);

  return function combination(state, action) {
    let hasChanged = false;
    const nextState = { ...state };

    for (const key of reducerKeys) {
      const reducer = reducers[key];

      const previousStateForKey = state[key];
      const nextStateForKey = reducer(previousStateForKey, action);

      nextState[key] = nextStateForKey;
      hasChanged = hasChanged || nextStateForKey !== previousStateForKey;
    }

    return hasChanged ? nextState : state;
  };
}
