import * as React from 'react';
import { Link } from 'react-router';
import * as mui from 'material-ui';
import { translate } from './i18n/translator';
import { Content, Header } from './layout/views';

export class Page420 extends React.Component<any, any> {
  constructor() {
    super();
  }

  render() {
    return (
      <mui.Paper>
        <Header text={translate('errorpage420_title', 'Session expired')} />

        <Content>
          {translate('errorpage420_message', 'Чтобы войти в систему, нажмите') + ' '}
          <Link to="/login"> {translate('login_enter', 'Enter')}</Link>
        </Content>
      </mui.Paper>
    );
  }
}
