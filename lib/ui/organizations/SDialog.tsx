import * as React from 'react';
import * as mui from 'material-ui';
import { translate } from '../../i18n/translator';
import { get, post } from '../../backend';
import { CommonOneFieldDTO } from '../../types';
import { TariffDetailDTO } from '../../models/models';
import { CertificateDTO } from './types';

require('./cp.css');

let socket: WebSocket;
let url: string = 'wss://localhost:9876';
const COMMAND_profileList = 'profileList';
const COMMAND_certificateRequestInit = 'certificateRequestInit';
const COMMAND_certificateRequest = 'certificateRequest';
const COMMAND_saveCertificate = 'saveCertificate';
const SIGNER_VERSION: number = 4;

type SDialogProps = {
  open: boolean;
  ownerId: string;
  certificate?: CertificateDTO;
  onRequestClose(close: boolean): void;
};

type SDialogState = {
  pass?: string;
  detailsRequest?: TariffDetailsRequestDTO;
  gostCert?: number;
  disableOKBtn?: boolean;
  disableCancelBtn?: boolean;

  processing?: boolean;
  errorText?: string;
  keyStore?: string;
};

type RequestCertDTO = {
  tariffDetailId: number;
  pkcs7: string;
  personId: number;
};
let requestedCerts: Array<RequestCertDTO> = [];

let reissueRequests: Array<string> = [];
let processedReissueRequests: Array<string> = [];

export default class SDialog extends React.Component<SDialogProps, SDialogState> {
  constructor(props) {
    super(props);

    this.state = {
      pass: '',
      detailsRequest: null,

      disableOKBtn: true,
      disableCancelBtn: false,

      processing: false,
      errorText: '',
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.open && nextProps.open != this.props.open) {
      this.getCertDetails();
      this.initSocket();
    }
  }

  componentWillMount() {
    get('/constants/name/' + 'EDS_DEFAULT_PERSON_PROFILE')
      .then((r: CommonOneFieldDTO) => {
        this.setState({ keyStore: r.value as string });
      })
      .catch(e => console.log('error in /constants/name' + 'EDS_DEFAULT_PERSON_PROFILE' + e));
  }

  private getCertDetails() {
    get('/certificate/details', { ownerId: this.props.ownerId })
      .then((r: TariffDetailsRequestDTO) => {
        this.setState({ detailsRequest: r });
      })
      .catch(e => console.log(e));
  }

  private initSocket() {
    if (!socket) socket = new WebSocket(url);

    socket.onopen = () => {
      console.log('Socket is opened');
      socket.send(this.profileRequest());
    };

    socket.onmessage = message => {
      let response = JSON.parse(message.data);
      switch (response.command) {
        case COMMAND_certificateRequestInit: {
          if (response.status == 'ok') {
            requestedCerts.push({ tariffDetailId: response.id, pkcs7: response.pkcs7, personId: +this.props.ownerId });
            if (requestedCerts.length == this.state.detailsRequest.details.length) this.handleCertificateRequestInit();
          } else this.setState({ errorText: response.message });
          break;
        }
        case COMMAND_certificateRequest: {
          if (response.status == 'ok') {
            requestedCerts.push({ tariffDetailId: response.id, pkcs7: response.pkcs7, personId: +this.props.ownerId });
            processedReissueRequests.push(
              JSON.stringify({ tariffDetailId: response.id, pkcs7: response.pkcs7, personId: +this.props.ownerId }),
            );

            if (!this.props.ownerId || +this.props.ownerId < 1) {
              alert(translate('data_is_old_reload_page'));
              break;
            }

            if (reissueRequests.length == processedReissueRequests.length) {
              this.handleCertificateRequest();
            }
          } else this.setState({ errorText: response.message });
          break;
        }
        case COMMAND_saveCertificate: {
          if (response.status == 'ok')
            this.setState({ disableOKBtn: false, processing: false, errorText: translate('signature_createdKey') });
          else
            this.setState({ disableOKBtn: false, processing: false, errorText: translate('cannot_save_certificate') });
          break;
        }
        default: {
          this.setState({ processing: false });
          break;
        }
      }
    };

    socket.onerror = e => {
      this.props.onRequestClose(false);
      alert(translate('couldnt_connect_to_cryptoprovider') + '\n' + translate('check_for_page_lauch'));
    };
    socket.onclose = c => console.log('Socket closed');
  }

  componentWillUnmount() {
    if (socket) socket.close();
    socket = null;
  }

  handleClose() {
    this.props.onRequestClose(false);
    if (socket) {
      socket.close();
      socket = null;
    }
    this.setState({ disableOKBtn: true, disableCancelBtn: false, errorText: '' });
  }

  handleSubmit() {
    this.setState({ disableOKBtn: true, disableCancelBtn: false, errorText: '' });
  }

  render() {
    const buttons = [
      <mui.FlatButton
        label={translate('common_cancel')}
        primary={true}
        disabled={this.state.disableCancelBtn}
        onTouchTap={() => this.handleClose()}
      />,
      <mui.FlatButton
        label={translate('common_save')}
        primary={true}
        disabled={this.state.disableOKBtn}
        onTouchTap={() => this.handleClose()}
      />,
    ];

    return (
      <mui.Dialog
        title={translate('keys_store_choose', 'Укажите пароль хранилища')}
        actions={buttons}
        contentStyle={{ width: '30%' }}
        modal={true}
        open={this.props.open}
        autoScrollBodyContent={true}
        onRequestClose={this.handleClose}>
        <div className="eds-content">
          {this.renderProfilePassword()}
          {this.renderRequestCertBtn()}
          {this.state.errorText.length > 0 ? this.renderErrors() : null}
        </div>
      </mui.Dialog>
    );
  }

  private profileRequest() {
    return JSON.stringify({
      version: SIGNER_VERSION,
      command: COMMAND_profileList,
    });
  }

  private renderProfilePassword = () => {
    return (
      <div className="eds-content-row">
        <div className="eds-content-row-label">
          <label className="eds-label">{translate('repository_password', 'Пароль хранилища:')}</label>
        </div>
        <div className="eds-content-row-next">
          <input type="text" name="pass" className="eds-pass" onChange={e => this.handlePasswordInput(e)} />
        </div>
      </div>
    );
  };

  private handlePasswordInput(e: React.FormEvent<HTMLInputElement>) {
    this.setState({ pass: e.currentTarget.value });
  }

  private renderRequestCertBtn = () => {
    return (
      <div className="eds-content-row">
        <button
          name="cert-request-btn"
          className="eds-request-cert-btn"
          disabled={this.state.processing}
          onClick={e => {
            this.setState({ errorText: '' });
            this.props.certificate
              ? this.handleReIssuanceKey(this.props.certificate)
              : this.handleCertificateRequestInitBtn();
          }}>
          {this.props.certificate ? translate('reissuance') : translate('vc_keysRequest')}
        </button>
      </div>
    );
  };

  private handleCertificateRequestInitBtn() {
    this.setState({ processing: true, errorText: translate('signature_creatingKey') });
    this.state.detailsRequest.details.map(detail => {
      let certRequest: string = JSON.stringify({
        version: SIGNER_VERSION,
        id: '' + detail.serverId, // серверное ИД деталей тарифа (т.к. уникальное)
        command: COMMAND_certificateRequestInit,
        profile: this.state.detailsRequest.managerProfile, // серверный профиль (FSystemMGR)
        profilePassword: this.state.detailsRequest.managerProfilePass, // серверный пароль (пустой)
        key: this.state.detailsRequest.managerKey, // серийный номер системного ключа
        algo: detail.algType,
        customerProfile: this.state.keyStore, // выбранный профиль для текущего пользователя
        customerProfilePassword: this.state.pass, // пароль на профиль текущего пользователя
        customerDN: this.state.detailsRequest.dn, // DN текущего пользователя
        template: detail.certificateInit,
      });
      socket.send(certRequest);
    });
  }

  /**
   * Метод получает RSA ключ по полю UUID ключа GOST и отправляет оба ключа в WS для инициализации
   * новых ключей на год
   * @param gost - сертификат GOST, который перевыпускает пользователь
   */
  private handleReIssuanceKey(gost: CertificateDTO) {
    this.setState({ gostCert: gost.id, processing: true, errorText: translate('reissue_request_generating') });
    get(`/certificate/rsa-by-gost?uuid=${gost.uuid}`)
      .then((rsa: CertificateDTO) => {
        this.state.detailsRequest.details.map(detail => {
          if (detail.algType == gost.alg) {
            let certRequest: string = JSON.stringify({
              version: SIGNER_VERSION,
              id: '' + detail.serverId,
              command: COMMAND_certificateRequest,
              profile: this.state.keyStore,
              profilePassword: this.state.pass,
              key: gost.certificateSN,
              template: detail.certificate,
            });
            reissueRequests.push(certRequest);
            socket.send(certRequest);
          } else {
            let certRequest: string = JSON.stringify({
              version: SIGNER_VERSION,
              id: '' + detail.serverId,
              command: COMMAND_certificateRequest,
              profile: this.state.keyStore,
              profilePassword: this.state.pass,
              key: rsa.certificateSN,
              template: detail.certificate,
            });
            reissueRequests.push(certRequest);
            socket.send(certRequest);
          }
        });
      })
      .catch(e => {
        console.error(JSON.stringify(e));
        this.state.detailsRequest.details.map(detail => {
          if (detail.algType == gost.alg) {
            let certRequest: string = JSON.stringify({
              version: SIGNER_VERSION,
              id: '' + detail.serverId,
              command: COMMAND_certificateRequest,
              profile: this.state.keyStore,
              profilePassword: this.state.pass,
              key: gost.certificateSN,
              template: detail.certificate,
            });

            reissueRequests.push(certRequest);
            socket.send(certRequest);
          }
        });
      });
  }

  private handleCertificateRequestInit() {
    post('/certificate/create-certificate-request', {
      uuid: '',
      certificateType: 'MOBILE',
      requestType: 'INIT',
      dn: this.state.detailsRequest.dn,
      list: requestedCerts,
    })
      .then((r: Array<CertificateDTO>) => {
        requestedCerts = [];
        reissueRequests = [];
        processedReissueRequests = [];
        this.saveCertificates(r);
      })
      .catch(e => {
        this.setState({ errorText: translate('vcReqError'), processing: false });
        requestedCerts = [];
        reissueRequests = [];
        processedReissueRequests = [];
      });
  }

  private handleCertificateRequest() {
    post('/certificate/create-certificate-request', {
      uuid: this.props.certificate.uuid,
      certificateType: 'WEB',
      requestType: 'REISSUE',
      dn: this.state.detailsRequest.dn,
      list: requestedCerts,
    })
      .then(r => {
        requestedCerts = [];
        reissueRequests = [];
        processedReissueRequests = [];
        this.setState({
          errorText: translate('certReqCreated'),
          processing: false,
          disableOKBtn: false,
          disableCancelBtn: false,
        });
      })
      .catch(e => {
        requestedCerts = [];
        reissueRequests = [];
        processedReissueRequests = [];
        this.setState({
          errorText: translate('vcReqError'),
          processing: false,
          disableOKBtn: false,
          disableCancelBtn: false,
        });
      });
  }

  private renderErrors() {
    return (
      <div style={{ marginTop: 30, color: 'red' }}>
        <span>{this.state.errorText}</span>
      </div>
    );
  }

  private saveCertificates(certificates: Array<CertificateDTO>) {
    certificates.map(certificate => this.saveCertificate(certificate));
  }
  private saveCertificate(certificate: CertificateDTO) {
    let save: string = JSON.stringify({
      version: SIGNER_VERSION,
      id: String(certificate.id),
      command: COMMAND_saveCertificate,
      profile: this.state.keyStore,
      profilePassword: this.state.pass,
      x509: certificate.certificate,
    });
    socket.send(save);
  }
}

type TariffDetailsRequestDTO = {
  managerProfile: string; // manager profile from server
  managerProfilePass: string; // manager profile password from server
  managerKey: string; // manager key sn from server
  dn: string; // dn current user
  details: Array<TariffDetailDTO>;
};
