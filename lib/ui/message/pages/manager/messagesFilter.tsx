import * as React from 'react';
import * as mui from 'material-ui';
import { GridFilter } from '../../../../grid/grid';
import { translate } from '../../../../i18n/translator';
import { Customer, DBCustomer } from '../../../../../../../src/manager/pages/demand/types';
import { get } from 'commonui_modules/lib/backend';
import { Manager } from '../../../../../../../src/manager/pages/management/managersManagement';
import './styles.scss';
import { BankBranch } from '../../../../../../../src/manager/pages/handbooks/banks/bankBranch';

type Filter = {
  type?: string;
  selectedPeriod?: string;
  dateFrom?: any;
  dateTo?: any;
  text?: string;
  customerName?: string;
  taxCode?: string;
  bankBranchCityId?: number;
  managerId?: number;
};

type MessagesFilterProps = {
  open: boolean;
  updateFilter?(filter: Filter): void;
};

type MessagesFilterState = {
  wide?: boolean;
  type?: string;
  text?: string;
  selectedPeriod?: string;
  dateFrom?: Date;
  dateTo?: Date;
  errorSearch?: boolean;
  arrCustomer?: DBCustomer[];
  arrBranch?: BankBranch[];
  arrManager?: Manager[];
  managerName?: string;
  managerId?: number;
  orgName?: string;
  customer?: Customer;
  customerName?: string;
  taxCode?: string;
  bankBranchName?: string;
  cityId?: number;
};

export class MessagesFilter extends React.Component<MessagesFilterProps, MessagesFilterState> {
  constructor() {
    super();

    this.state = {
      wide: false,
      type: 'all',
      text: '',
      selectedPeriod: null,
      dateFrom: null,
      dateTo: null,
      arrCustomer: [],
      orgName: '',
      arrBranch: [],
      managerName: '',
      managerId: null,
      arrManager: [],
      customer: null,
      customerName: '',
      taxCode: '',
      bankBranchName: '',
      cityId: null,
    };
  }

  componentDidMount() {
    this.getBranches('');
  }

  clear() {
    this.setState(
      {
        type: 'all',
        text: '',
        selectedPeriod: null,
        dateFrom: null,
        dateTo: null,
        orgName: '',
        customer: null,
        customerName: '',
        managerName: '',
        managerId: null,
        taxCode: '',
        bankBranchName: '',
        cityId: null,
      },
      () => {
        this.search();
      },
    );
  }

  search() {
    this.props.updateFilter({
      type: this.state.type,
      selectedPeriod: this.state.selectedPeriod,
      dateFrom: this.state.dateFrom,
      dateTo: this.state.dateTo,
      text: this.state.text,
      bankBranchCityId: this.state.cityId,
      customerName: this.state.customer ? this.state.customer.name : '',
      managerId: this.state.managerId,
    });
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.state.text && nextState.text.length > 60) {
      nextState.errorSearch = `${translate('common_maxLength')} 60 ${translate('common_symbolsForMaxLength')}`;
      return;
    } else {
      nextState.errorSearch = null;
    }
  }

  getBranches(text: string) {
    get('/bank-branch', {
      searchText: text,
      page: 0,
      size: 25,
      sort: 'id',
      order: 'asc',
      isDistinctCity: true,
    })
      .then(r => {
        const arrRes = r.rows as BankBranch[];
        this.setState({ arrBranch: arrRes });
      })
      .catch(e => console.error('arrCustomer' + e));
  }

  getOrganizations(text: string, column: string) {
    get('/customer/get-all', {
      page: 0,
      size: 25,
      sort: 'id',
      order: 'asc',
      search: text,
      column,
    })
      .then(r => {
        const arrRes = r as DBCustomer[];
        const arrTaxCode = arrRes.map((item, index) => {
          return item.taxCode;
        });
        arrRes.unshift(null);
        this.setState({ arrCustomer: arrRes });
      })
      .catch(e => console.error('arrCustomer' + e));
  }

  getManagers(text: string) {
    get('/management/managers', {
      searchText: text,
      page: 0,
      size: 25,
      sort: 'fullName',
      order: 'asc',
    })
      .then(r => {
        const arrRes = r.rows;
        this.setState({ arrManager: arrRes });
      })
      .catch(e => console.error('arrManager' + e));
  }

  getBranchLabel(branch: BankBranch): string {
    return branch ? `${branch.shortName} (${branch.mailAddress})` : '';
  }

  render() {
    return (
      <GridFilter open={this.props.open}>
        <div className="message-filter">
          <div className="filter-row">
            <div className="filter-element">
              <mui.TextField
                id={'searchText'}
                hintText={translate('common_search')}
                errorText={this.state.errorSearch}
                value={this.state.text}
                onChange={e => this.setState({ text: (e.target as HTMLInputElement).value })}
              />
            </div>
            <div className="filter-element" style={{ flex: 1 }}>
              <mui.RadioButtonGroup
                name="selectPeriod"
                className={'period-group'}
                defaultSelected={null}
                valueSelected={this.state.selectedPeriod}
                onChange={(event, value) => {
                  this.setState({
                    selectedPeriod: value,
                    dateFrom: null,
                    dateTo: null,
                  });
                }}>
                <mui.RadioButton className={'period-button'} value={null} label={translate('common_all')} />
                <mui.RadioButton className={'period-button'} value="today" label={translate('common_today')} />
                <mui.RadioButton className={'period-button'} value="week" label={translate('common_week')} />
                <mui.RadioButton className={'period-button'} value="month" label={translate('common_month')} />
              </mui.RadioButtonGroup>
            </div>
          </div>

          <div className="filter-row">
            <div className="filter-element">
              <mui.AutoComplete
                menuStyle={{ maxHeight: '300px' }}
                hintText={translate('common_organization')}
                textFieldStyle={{ textAlign: 'right', fontSize: '13px' }}
                filter={mui.AutoComplete.caseInsensitiveFilter}
                dataSource={this.state.arrCustomer.map(o => o && o.name)}
                openOnFocus={true}
                searchText={this.state.orgName}
                onNewRequest={(e, i) => {
                  this.setState({
                    customer: this.state.arrCustomer[i],
                  });
                  this.getOrganizations(e, 'name');
                }}
                onUpdateInput={e => {
                  this.setState({ orgName: e });
                  this.getOrganizations(e, 'name');
                }}
              />
            </div>
            <div className="filter-element">
              <mui.AutoComplete
                menuStyle={{ maxHeight: '300px' }}
                listStyle={{ width: '400px' }}
                hintText={translate('common_branch')}
                textFieldStyle={{ textAlign: 'right', fontSize: '13px' }}
                filter={mui.AutoComplete.caseInsensitiveFilter}
                dataSource={this.state.arrBranch ? this.state.arrBranch.map(o => this.getBranchLabel(o)) : []}
                menuProps={{
                  menuItemStyle: {
                    minHeight: 'unset',
                    padding: '10px 0',
                    lineHeight: '22px',
                    whiteSpace: 'break-spaces',
                  },
                }}
                openOnFocus={true}
                searchText={this.state.bankBranchName}
                onNewRequest={(e, i) => {
                  this.setState({
                    bankBranchName: this.getBranchLabel(this.state.arrBranch[i]),
                    cityId: this.state.arrBranch[i].cityId,
                  });
                  this.getBranches(e);
                }}
                onUpdateInput={e => {
                  this.setState({ bankBranchName: e, cityId: null });
                  this.getBranches(e.split(' (')[0]);
                }}
              />
            </div>
            <div className="filter-element">
              <mui.AutoComplete
                menuStyle={{ maxHeight: '300px' }}
                hintText={translate('common_manager')}
                textFieldStyle={{ textAlign: 'right', fontSize: '13px' }}
                filter={mui.AutoComplete.caseInsensitiveFilter}
                dataSource={this.state.arrManager ? this.state.arrManager.map(o => o && o.fullName) : []}
                openOnFocus={true}
                searchText={this.state.managerName}
                onNewRequest={(e, i) => {
                  this.setState({
                    managerName: this.state.arrManager[i].fullName,
                    managerId: this.state.arrManager[i].id,
                  });
                  this.getManagers(e);
                }}
                onUpdateInput={e => {
                  this.setState({ managerName: e, managerId: null });
                  this.getManagers(e);
                }}
              />
            </div>
          </div>

          <div className="filter-row">
            <div className="filter-element">
              <mui.DatePicker
                hintText={translate('common_periodFrom')}
                value={this.state.dateFrom}
                onChange={(event, date) => {
                  this.setState({
                    selectedPeriod: null,
                    dateFrom: date,
                  });
                }}
              />
            </div>
            <div className="filter-element">
              <mui.DatePicker
                hintText={translate('common_periodTo')}
                value={this.state.dateTo}
                onChange={(event, date) => {
                  this.setState({
                    selectedPeriod: null,
                    dateTo: date,
                  });
                }}
              />
            </div>
          </div>

          <br />

          <mui.RaisedButton
            label={translate('common_search')}
            primary={true}
            style={{ margin: '0 10px 0 0' }}
            onTouchTap={() => this.search()}
          />
          <mui.RaisedButton
            label={translate('common_clean')}
            primary={true}
            onTouchTap={() => this.clear()}
            style={{ margin: '0 10px 0 0' }}
          />
        </div>
      </GridFilter>
    );
  }
}
