import * as React from 'react';
import * as mui from 'material-ui';
import * as backend from '../../../../backend';
import icons from '../../../../layout/icons';
import { visibility } from '../../../../layout/visibility';
import { Content, Page } from '../../../../layout/views';
import { Grid, GridContextMenu, GridHeader } from '../../../../grid/grid';
import { translate } from '../../../../i18n/translator';
import { siteConfig } from '../../../../siteConfig';
import { MessagesFilter } from './messagesFilter';
import { Letter } from './inbox';

type MessageAttachment = {
  id?: number;
  name?: string;
};

type MessageState = {
  messages?: Array<Letter>;
  filter?: Filter;
  item?: any;
  openDetail?: boolean;
  openCreate?: boolean;
  filterOpen?: boolean;
  documentStatus?: string;
  file?: any;
  content?: string;
  load?: MessageAttachment;
  processing?: boolean;
};

type Filter = {
  type?: string;
  from?: any;
  to?: any;
  text?: string;
};

export class OutboxMessagePage extends React.Component<any, MessageState> {
  constructor() {
    super();

    this.state = {
      filter: {},
      filterOpen: false,
      processing: false,
    };
  }

  fetchRows = window => {
    return backend.get('/letter/manager', Object.assign(window, { sendStatus: false }));
  };

  processReload() {
    this.setState({ processing: true }, () => {
      this.setState({ processing: false });
    });
  }

  updateFilter(filter) {
    this.setState({ filter: Object.assign({}, this.state.filter, filter) });
  }

  renderStatus(obj) {
    if (obj.sendStatus) {
      if (obj.documentStatus == 'Draft') {
        return 'Новое';
      } else if (obj.documentStatus == 'Signed') {
        return 'Отправлено';
      } else {
        return 'Прочитано клиентом';
      }
    } else {
      if (obj.isRead) {
        // TODO
        return 'Прочитано';
      } else if (obj.documentStatus == 'Signed') {
        return 'Отправлено';
      } else {
        return 'Новое';
      }
    }
  }

  update(obj, change = {}) {
    backend
      .put('/letter/manager', Object.assign(obj, change))
      .then(() => this.updateFilter(this.state.filter))
      .catch(e => console.log(e));
  }

  renderDocumentMenu = doc => {
    const { privileges } = this.props;

    return (
      <GridContextMenu title={translate('common_documentOperations')}>
        {(doc.documentStatus === 'Signed' || doc.documentStatus === 'Read') && (
          <mui.MenuItem
            key="details"
            primaryText={translate('common_details')}
            leftIcon={icons.info}
            onTouchTap={() => {
              navigate.to('message/detail/' + doc.id + '/false');
            }}
          />
        )}

        {OutboxMessagePage.isShowReadMenu(doc) && (
          <mui.MenuItem
            key="read"
            primaryText="Прочитано"
            leftIcon={icons.info}
            onTouchTap={() => {
              backend
                .put('/letter/manager/read/' + doc.id, {})
                .then(() => this.updateFilter(this.state.filter))
                .catch(e => console.log(e));
            }}
          />
        )}

        {OutboxMessagePage.isShowSendMenu(doc) &&
          privileges &&
          visibility('m_message_sent', privileges) && (
            <mui.MenuItem
              key="send"
              primaryText="Отправить"
              leftIcon={icons.info}
              onTouchTap={() => {
                backend
                  .put('/letter/send/' + doc.id, {})
                  .then(() => this.updateFilter(this.state.filter))
                  .catch(e => console.log(e));
              }}
            />
          )}

        {doc.sendStatus &&
          privileges &&
          visibility('m_message_reply', privileges) && (
            <mui.MenuItem
              key="reply"
              primaryText="Ответить"
              leftIcon={icons.info}
              onTouchTap={() => {
                navigate.to('message/detail/' + doc.id + '/true');
              }}
            />
          )}

        {doc.documentStatus === 'Draft' && (
          <mui.MenuItem
            primaryText="Удалить"
            leftIcon={icons.del}
            onTouchTap={() => {
              backend
                .del('/letter/manager/' + doc.id)
                .then(() => this.updateFilter(this.state.filter))
                .catch(e => console.log(e));
            }}
          />
        )}

        {doc.documentStatus === 'Draft' && (
          <mui.MenuItem
            primaryText="Просмотреть/Редактировать"
            leftIcon={icons.edit}
            onTouchTap={() => navigate.to('message/create/' + doc.id)}
          />
        )}
      </GridContextMenu>
    );
  };

  renderHeader() {
    const getButtons = searchButton => [
      <mui.IconButton
        key="filter"
        title={translate('filterSettings')}
        onTouchTap={e => {
          this.setState({ filterOpen: !this.state.filterOpen });
        }}>
        {icons.filter}
      </mui.IconButton>,
      this.props.privileges &&
        visibility('m_message_add', this.props.privileges) && (
          <mui.IconButton
            key="add"
            title="Создать"
            onTouchTap={() => {
              navigate.to('message/create');
            }}>
            {icons.add}
          </mui.IconButton>
        ),
      <mui.IconButton onClick={() => this.processReload()}>{icons.replay}</mui.IconButton>,
    ];

    return <GridHeader headerText={translate('outbox')} setSearchText={searchText => {}} getButtons={getButtons} />;
  }

  private static isShowReadMenu(obj) {
    return !!(obj.sendStatus && !obj.isRead);
  }

  private static isShowSendMenu(obj) {
    return !obj.sendStatus && obj.documentStatus === 'Draft';
  }

  create() {
    let jsonFile = new FormData();
    jsonFile.append('file', this.state.file);
    jsonFile.append('desc', this.state.content);
    jsonFile.append('documentStatus', this.state.documentStatus);
    jsonFile.append('send', true);
    backend
      .post('/letter/manager', jsonFile)
      .then(() => this.updateFilter(this.state.filter))
      .catch(e => console.log(e));
  }

  private gridMap = {
    letterDate: () => {
      return { label: translate('common_date'), align: 'center', render: doc => doc.date };
    },
    organizationName: () => {
      return { label: translate('common_organization'), align: 'center', render: doc => doc.customerName };
    },
    customerInitials: () => {
      return { label: translate('customerInitials'), align: 'center', render: doc => doc.companyPersonName };
    },
    bankBranchName: () => {
      return { label: translate('common_branch'), render: doc => doc.bankBranchName };
    },
    managerName: () => {
      return { label: translate('common_manager'), align: 'center', render: doc => doc.managerName };
    },
    letterSubject: () => {
      return { label: translate('subject'), align: 'center', render: doc => doc.subject };
    },
    letterState: () => {
      return { label: translate('common_state'), align: 'center', render: doc => this.renderStatus(doc) };
    },
  };

  render() {
    type DocumentGrid = new (props) => Grid<Letter>;
    const DocumentGrid = Grid as DocumentGrid;
    let columns = this.buildColumns();

    return (
      <Page>
        {this.renderHeader()}
        <Content padding={0}>
          <MessagesFilter open={this.state.filterOpen} updateFilter={filter => this.updateFilter(filter)} />
          {!this.state.processing && (
            <DocumentGrid
              selectable={false}
              fetchRows={this.fetchRows}
              defaultSort="id"
              defaultOrder="desc"
              filter={this.state.filter}
              columns={columns}
            />
          )}
        </Content>
      </Page>
    );
  }

  private buildColumns() {
    let columns = [];

    if (siteConfig.letter && siteConfig.letter.grid_outbox_fields) {
      let gridInboxFields: Array<string> = siteConfig.letter.grid_outbox_fields;
      gridInboxFields.forEach(item => {
        let curr = this.gridMap[item]();
        columns.push(curr);
      });
    } else {
      columns.push(this.gridMap['letterDate']());
      columns.push(this.gridMap['organizationName']());
      columns.push(this.gridMap['customerInitials']());
      columns.push(this.gridMap['bankBranchName']());
      columns.push(this.gridMap['managerName']());
      columns.push(this.gridMap['letterSubject']());
      columns.push(this.gridMap['letterState']());
    }

    columns.push({ label: '', align: 'right', render: doc => this.renderDocumentMenu(doc) });
    return columns;
  }
}
