import * as React from 'react';
import * as mui from 'material-ui';
import * as backend from '../../../../backend';
import icons from '../../../../layout/icons';
import { visibility } from '../../../../layout/visibility';
import { Content, Page, letterEventEmitter } from '../../../../layout/views';
import { Grid, GridContextMenu, GridHeader } from '../../../../grid/grid';
import { MessagesFilter } from './messagesFilter';
import { translate } from '../../../../i18n/translator';
import { Letter, MessageAttachment } from '../../../../types';
import { siteConfig } from '../../../../siteConfig';

interface MessageState {
  messages?: Letter[];
  filter?: Filter;
  item?: any;
  openDetail?: boolean;
  openCreate?: boolean;
  filterOpen?: boolean;
  documentStatus?: string;
  file?: any;
  content?: string;
  load?: MessageAttachment;
  processing?: boolean;
}

interface Filter {
  type?: string;
  from?: any;
  to?: any;
  text?: string;
}

interface InboxMessagePageProps {
  privileges?: string[];
}

export class InboxMessagePage extends React.Component<InboxMessagePageProps, MessageState> {
  private gridMap = {
    letterDate: () => {
      return { label: translate('common_date'), render: doc => doc.date };
    },
    organizationName: () => {
      return { label: translate('common_organization'), render: doc => doc.customerName };
    },
    customerInitials: () => {
      return { label: translate('customerInitials'), render: doc => doc.companyPersonName };
    },
    bankBranchName: () => {
      return { label: translate('common_branch'), render: doc => doc.bankBranchName };
    },
    managerName: () => {
      return { label: translate('common_manager'), render: doc => doc.managerName };
    },
    letterSubject: () => {
      return { label: translate('subject'), render: doc => doc.subject };
    },
    letterState: () => {
      return { label: translate('common_state'), render: doc => this.renderStatus(doc) };
    },
  };

  constructor() {
    super();

    this.state = {
      filter: {},
      filterOpen: false,
      processing: false,
    };
  }

  componentDidMount() {
    letterEventEmitter.addListener('NEW_LETTER_FROM_CUSTOMER_EVENT', this.listenerNewLetterFromCustomer);
  }

  componentWillUnmount() {
    letterEventEmitter.removeListener('NEW_LETTER_FROM_CUSTOMER_EVENT', this.listenerNewLetterFromCustomer);
  }

  fetchRows = window => {
    return backend.get('/letter/manager', { ...window, sendStatus: true });
  };

  processReload() {
    this.setState({ processing: true }, () => {
      this.setState({ processing: false });
    });
  }

  updateFilter(filter) {
    this.setState({ filter: { ...this.state.filter, ...filter } });
  }

  renderStatus(obj) {
    if (obj.sendStatus) {
      switch (obj.documentStatus) {
        case 'Read':
          return 'Прочитано';
        case 'Signed':
          return 'Отправлено';
        default:
          return 'Новое';
      }
    }

    return obj.documentStatus === 'Draft'
      ? 'Новое'
      : obj.documentStatus === 'Signed'
        ? 'Отправлено'
        : 'Прочитано клиентом';
  }

  update(obj, change = {}) {
    backend
      .put('/letter/send/' + obj.id, { ...obj, ...change })
      .then(() => this.updateFilter(this.state.filter))
      .catch(e => console.log(e));
  }

  renderDocumentMenu = doc => {
    const { privileges } = this.props;
    return (
      <GridContextMenu title={translate('common_documentOperations')}>
        <mui.MenuItem
          key="details"
          primaryText={translate('common_details')}
          leftIcon={icons.info}
          onTouchTap={() => {
            navigate.to('message/detail/' + doc.id + '/false');
          }}
        />
        {InboxMessagePage.isShowReadMenu(doc) ? (
          <mui.MenuItem
            key="read"
            primaryText="Прочитано"
            leftIcon={icons.info}
            onTouchTap={() => {
              backend
                .put('/letter/manager/read/' + doc.id, {})
                .then(() => {
                  this.updateFilter(this.state.filter);
                })
                .catch(e => console.log(e));
            }}
          />
        ) : null}
        {InboxMessagePage.isShowSendMenu(doc) &&
          privileges &&
          visibility('m_message_sent', privileges) && (
            <mui.MenuItem
              key="send"
              primaryText="Отправить"
              leftIcon={icons.info}
              onTouchTap={() => {
                this.update(doc, { documentStatus: 'Signed' });
              }}
            />
          )}
        {doc.sendStatus &&
          privileges &&
          visibility('m_message_reply', privileges) && (
            <mui.MenuItem
              key="reply"
              primaryText="Ответить"
              leftIcon={icons.info}
              onTouchTap={() => {
                navigate.to('message/detail/' + doc.id + '/true');
              }}
            />
          )}
        <mui.MenuItem
          key="closed"
          primaryText={translate('common_close')}
          leftIcon={icons.info}
          onTouchTap={() => {
            backend
              .post('/letter/manager/close/' + doc.id, {})
              .then(() => {
                this.updateFilter(this.state.filter);
              })
              .catch(e => console.log(e));
          }}
        />
      </GridContextMenu>
    );
  };

  renderHeader() {
    const { privileges } = this.props;

    const getButtons = searchButton => [
      <mui.IconButton
        key="filter"
        title={translate('filterSettings')}
        onTouchTap={e => {
          this.setState({ filterOpen: !this.state.filterOpen });
        }}>
        {icons.filter}
      </mui.IconButton>,
      privileges &&
        visibility('m_message_add', privileges) && (
          <mui.IconButton
            key="add"
            title="Создать"
            onTouchTap={() => {
              navigate.to('message/create');
            }}>
            {icons.add}
          </mui.IconButton>
        ),
      <mui.IconButton onClick={() => this.processReload()}>{icons.replay}</mui.IconButton>,
    ];

    return <GridHeader headerText={translate('inbox')} setSearchText={searchText => {}} getButtons={getButtons} />;
  }

  private static isShowReadMenu(obj) {
    return obj.sendStatus && obj.documentStatus === 'Signed';
  }

  private static isShowSendMenu(obj) {
    return !obj.sendStatus && obj.documentStatus === 'Draft';
  }

  create() {
    const jsonFile = new FormData();

    jsonFile.append('file', this.state.file);
    jsonFile.append('desc', this.state.content);
    jsonFile.append('documentStatus', this.state.documentStatus);
    jsonFile.append('send', true);

    backend
      .post('/letter/manager', jsonFile)
      .then(() => this.updateFilter(this.state.filter))
      .catch(e => console.log(e));
  }

  render() {
    type DocumentGrid = new (props) => Grid<Letter>;
    const DocumentGrid = Grid as DocumentGrid;
    const columns = this.buildColumns();

    return (
      <Page>
        {this.renderHeader()}
        <Content padding={0}>
          <MessagesFilter open={this.state.filterOpen} updateFilter={filter => this.updateFilter(filter)} />
          {!this.state.processing && (
            <DocumentGrid
              selectable={false}
              fetchRows={this.fetchRows}
              defaultSort="id"
              defaultOrder="desc"
              filter={this.state.filter}
              columns={columns}
            />
          )}
        </Content>
      </Page>
    );
  }

  private buildColumns = () => {
    const columns = [];

    if (siteConfig.letter && siteConfig.letter.grid_inbox_fields) {
      const gridInboxFields: string[] = siteConfig.letter.grid_inbox_fields;
      gridInboxFields.forEach(item => {
        const curr = this.gridMap[item]();
        columns.push(curr);
      });
    } else {
      columns.push(this.gridMap['letterDate']());
      columns.push(this.gridMap['organizationName']());
      columns.push(this.gridMap['customerInitials']());
      columns.push(this.gridMap['bankBranchName']());
      columns.push(this.gridMap['managerName']());
      columns.push(this.gridMap['letterSubject']());
      columns.push(this.gridMap['letterState']());
    }

    columns.push({ label: '', align: 'right', render: doc => this.renderDocumentMenu(doc) });
    return columns;
  };

  private listenerNewLetterFromCustomer = () => {
    this.updateFilter(this.state.filter);
  };
}
