import * as React from 'react';
import * as mui from 'material-ui';
import { Page } from '../../../../layout/views';
import { translate } from '../../../../i18n/translator';
import { InboxMessagePage } from './inbox';
import { OutboxMessagePage } from './outbox';
import { AllMessagePage } from './all';
import { ClosedMessagePage } from './closed';

export class MessagesManager extends React.Component<void, void> {
  render() {
    sessionStorage.setItem('bread_url', `${translate('navigation_mail')}/${translate('mailing_correspondence')}`);

    return (
      <Page>
        <mui.Tabs>
          <mui.Tab label={translate('common_all')}>
            <AllMessagePage privileges={this.props.privileges} />
          </mui.Tab>
          <mui.Tab label={translate('inbox')}>
            <InboxMessagePage privileges={this.props.privileges} />
          </mui.Tab>
          <mui.Tab label={translate('outbox')}>
            <OutboxMessagePage privileges={this.props.privileges} />
          </mui.Tab>
          <mui.Tab label={translate('common_closed')}>
            <ClosedMessagePage privileges={this.props.privileges} />
          </mui.Tab>
        </mui.Tabs>
      </Page>
    );
  }
}
