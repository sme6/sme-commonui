import * as React from 'react';
import * as mui from 'material-ui';
import { get, post, props } from '../../../../backend';
import { HomeWidget } from '../../../../layout/views';
import { translate } from '../../../../i18n/translator';
import { DownloadWidget, DownloadWidgetType } from '../../../downloads/downloads';
import { Letter } from '../../../../types';
import { errorMessage, GlobalErrorComponent } from '../../../../util/serverErrors';
import { siteConfig } from '../../../../siteConfig';

const Dropzone = require('react-dropzone');
let dropzone;

interface MessageDetailState {
  baseLetter?: Letter;
  chain?: Letter[];
  isProgress?: boolean;
  content?: string;
  error?: string;
  size?: number;
  uploadTypes?: string[];
  selectedFiles?: { file: File; error: string }[];
  urlPrefix?: 'manager' | 'customer';
  errorMessage?: string;
}

const styles = {
  chip: {
    margin: 4,
  },
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  messageDetails: {
    whiteSpace: 'normal',
    wordWrap: 'break-word',
    wordBreak: 'break-all',
    width: '80%',
  },
};

export class MessageDetailPage extends React.Component<any, MessageDetailState> {
  constructor(props) {
    super(props);

    this.state = {
      isProgress: true,
      content: '',
      selectedFiles: [],
      urlPrefix: location.href.includes('manager') ? 'manager' : 'customer',
    };
  }

  create() {
    const jsonFile = new FormData();

    this.state.selectedFiles.length &&
      this.state.selectedFiles.forEach((item, index) => {
        if (item.file) {
          jsonFile.append('files', item.file);
        }
      });

    jsonFile.append('subject', this.state.baseLetter.subject);
    jsonFile.append('desc', this.state.content);
    jsonFile.append('reply', this.props.params.messageId);

    this.setState({ isProgress: true });

    post('/letter/' + this.state.urlPrefix + '/reply', jsonFile)
      .then(() => navigate.to('/letter'))
      .catch(e => {
        console.log(e);
        this.setState({ isProgress: false });
        errorMessage(e, errorMessage => this.setState({ errorMessage }));
      });
  }

  renderFileChip(file, key) {
    return (
      <div style={{ display: 'flex', marginTop: '5px' }}>
        <mui.Chip key={key} onRequestDelete={() => this.deleteFile(key)} style={styles.chip}>
          {file.file ? file.file.name : ''}
        </mui.Chip>
        <span style={{ color: 'red', whiteSpace: 'pre-wrap' }}> {file.error} </span>
      </div>
    );
  }

  onDrop = acceptedFiles => {
    const arr = this.state.selectedFiles.slice(0);

    acceptedFiles.forEach(file => {
      const fileError =
        this.state.uploadTypes.indexOf(file.type) === -1
          ? translate('common_WrongFileType') + ' ' + this.state.uploadTypes
          : '';

      const sizeError =
        file.size && file.size >= this.state.size * 1024 * 1024 ? 'Не более ' + this.state.size + 'Мб; ' : '';

      arr.push({
        file,
        error: sizeError.concat(fileError),
      });
    });

    this.setState({ selectedFiles: arr });
  };

  deleteFile(key) {
    const arr = this.state.selectedFiles.slice(0);
    arr.splice(key, 1);
    this.setState({ selectedFiles: arr });
  }

  validateFiles() {
    let b = false;
    this.state.selectedFiles.length &&
      this.state.selectedFiles.forEach(item => {
        if (item.error) {
          b = true;
        }
      });
    return b;
  }

  componentWillMount() {
    Promise.all([
      get('/constants/max-file-size'),
      get('/constants/upload-file-types'),
      get(
        '/letter/by_id/' +
          this.state.urlPrefix +
          '/chain/' +
          this.props.params.messageId +
          '?isLocking=' +
          this.props.params.isReplay,
      ),
    ])
      .then(values => {
        this.setState({
          size: values[0] as number,
          uploadTypes: values[1] as string[],
          baseLetter: values[2]['baseLetter'] as Letter,
          chain: values[2]['chain'] as Letter[],
          isProgress: false,
        });
      })
      .catch(e => console.log(e));
  }

  isLegalEntity = () => {
    return siteConfig.mail.showCompanyPersonBlock === true;
  };

  render() {
    sessionStorage.setItem('bread_lvl', '1');
    sessionStorage.setItem('bread_parentUrl', '/letter');
    if (this.props.params.isReplay === 'false') {
      sessionStorage.setItem(
        'bread_url',
        `${translate('navigation_mail')}/${translate('mailing_correspondence')}/${translate('common_details')}`,
      ); // breadcrumbs
    } else {
      sessionStorage.setItem(
        'bread_url',
        `${translate('navigation_mail')}/${translate('mailing_correspondence')}/${translate('reply')}`,
      );
    }
    return (
      <div style={styles.messageDetails}>
        <HomeWidget title="Просмотр письма">
          <GlobalErrorComponent errorMessage={this.state.errorMessage} />

          <div>{this.state.isProgress ? <mui.LinearProgress /> : null}</div>
          {this.state.chain ? (
            <div>
              <div>
                <mui.List>
                  {this.state.baseLetter ? (
                    <mui.ListItem>
                      <div style={{ padding: '3px' }}>
                        {translate('common_date') + ': ' + this.state.baseLetter.date}
                      </div>
                      <div style={{ padding: '3px' }}>
                        {translate('subject') + ': ' + this.state.baseLetter.subject}
                      </div>
                      {this.isLegalEntity() &&
                        this.state.baseLetter.organizationName && (
                          <div style={{ padding: '3px' }}>
                            {translate('common_organization') + ': ' + this.state.baseLetter.organizationName}
                          </div>
                        )}
                      {this.state.baseLetter.customerName && (
                        <div style={{ padding: '3px' }}>
                          {translate('message_details_user', 'Пользователь') +
                            ': ' +
                            this.state.baseLetter.customerName}
                        </div>
                      )}
                      {this.state.urlPrefix == 'manager' &&
                        this.state.baseLetter.customerExternalId && (
                          <div style={{ padding: '3px' }}>
                            {translate('message_customer_external_id', 'ID по АБС') +
                              ': ' +
                              this.state.baseLetter.customerExternalId}
                          </div>
                        )}
                      {this.state.urlPrefix == 'manager' &&
                        this.state.baseLetter.customerTaxCode && (
                          <div style={{ padding: '3px' }}>
                            {translate('message_customer_tax_code', 'БИН') +
                              ': ' +
                              this.state.baseLetter.customerTaxCode}
                          </div>
                        )}
                      {this.state.baseLetter.managerName && (
                        <div style={{ padding: '3px' }}>{'Менеджер: ' + this.state.baseLetter.managerName}</div>
                      )}
                    </mui.ListItem>
                  ) : null}

                  {this.props.params.isReplay === 'true' && (
                    <mui.ListItem>
                      <div>
                        <mui.TextField
                          hintText="Сообщение"
                          floatingLabelText="Ответ:"
                          onChange={e =>
                            this.setState({
                              content: (e.target as HTMLInputElement).value,
                            })
                          }
                          multiLine={true}
                          fullWidth={true}
                          rows={5}
                        />
                        <div>
                          <div style={{ marginTop: 10, display: 'inline-block' }}>
                            <mui.RaisedButton
                              label={translate('intTransfer_uploadFile')}
                              primary={true}
                              onTouchTap={() => dropzone.open()}
                            />
                            {this.state.selectedFiles &&
                              this.state.selectedFiles.map((file, key) => this.renderFileChip(file, key))}
                          </div>
                        </div>
                        <div style={{ display: 'none' }}>
                          <Dropzone
                            ref={node => {
                              dropzone = node;
                            }}
                            onDrop={this.onDrop}
                            multiple={true}
                          />
                        </div>
                        <div style={{ marginTop: '10px' }}>
                          <mui.RaisedButton
                            label="Ответить"
                            disabled={this.validateFiles() || this.state.isProgress}
                            primary={true}
                            onTouchTap={() => {
                              this.create();
                            }}
                          />
                        </div>
                      </div>
                    </mui.ListItem>
                  )}

                  <mui.ListItem>
                    {this.state.chain.map((n, i) => (
                      <div
                        style={{
                          padding: '5px',
                          margin: '5px',
                          whiteSpace: 'pre-line',
                          color: n.sendStatus ? 'rgba(0,0,0,0.5)' : 'white',
                          fontSize: '14px',
                          marginBottom: '0px',
                          wordWrap: 'break-word',
                          backgroundColor: n.isFromCustomer ? '#1F497D' : '#9C014B',
                          borderBottomLeftRadius: '5px',
                          borderTopLeftRadius: '5px',
                          borderTopRightRadius: '5px',
                          borderBottomRightRadius: '5px',
                        }}>
                        {`${n.date}
                                                `}
                        {n.isFromCustomer ? this.state.baseLetter.customerName + ': ' : n.managerFullName + ': '}
                        {n.desc}

                        {n.attachmentFiles
                          ? n.attachmentFiles.map((item, j) => (
                              <div style={{ marginTop: '3px' }}>
                                <mui.Chip style={styles.chip}>
                                  <DownloadWidget
                                    key={'exportAnyFile' + (32 * i + j)}
                                    method={'GET'}
                                    type={DownloadWidgetType.ANY_FIlE}
                                    formId={'exportAnyFile' + (32 * i + j)}
                                    action={props.apiPrefix + props.v1 + '/letter/download/' + item.id}
                                    title={item.name}
                                  />
                                </mui.Chip>
                              </div>
                            ))
                          : null}
                      </div>
                    ))}
                  </mui.ListItem>
                </mui.List>
              </div>
            </div>
          ) : null}
        </HomeWidget>
      </div>
    );
  }
}
