export const paymentUrls = {
  domestic_transfer: '/payment/domestic-transfer/',
  get_domestic_transfers: '/payment/domestic-transfer',
  domestic_transfer_get_excel: '/payment/domestic-transfer/get-excel/',

  get_payment_pdf: '/payment/domestic-transfer/get-payment-pdf/',
  get_payroll_pdf: '/payment/domestic-transfer/get-payroll-pdf/',
  get_pensionContribution_pdf: '/payment/domestic-transfer/get-pensionContribution-pdf/',
  get_socialContribution_pdf: '/payment/domestic-transfer/get-pensionContribution-pdf/',
};

export interface Document {
  id?: number;
  paymentId?: number;
  account?: AccountView;
  amount?: number;
  valueDate?: Date;
  purpose?: string;
  purposeCode?: string;
  purposeCodeLabel?: string;
  priority?: boolean;

  benefName?: string;
  benefTaxCode?: string;
  benefAccount?: string;
  benefBankCode?: string;
  benefResidencyCode?: string;

  custId?: number;
  custName?: string;
  custIntlName?: string;
  custTaxCode?: string;
  custResidencyCode?: string;

  created?: Date;
  number?: string;
  isTemplate?: boolean;
  templateName?: string;
  template?: ShortPaymentView;
  listTemplates?: Array<ShortPaymentView>;

  state?: string;
  type?: string;
  director?: CompanyPerson;
  accountant?: CompanyPerson;
  bankResponse?: string;
  manager?: string;
  info?: string;
  actions?: any;
}

export interface AccountTransfer extends Document {
  debitAccount?: AccountView;
  debitCurrency?: string;
  debitSum?: number;
  creditAccount?: AccountView;
  creditCurrency?: string;
  creditSum?: number;
  feeAccount?: AccountView;
  exchangeRate?: number;
  individualExchangeRate?: boolean;
}

export interface DomesticTransfer extends Document {
  domesticTransferType?: string;
  budgetCode?: string;
  vinCode?: string;
  vat?: number;
  employeeTransferCategory?: string;
  employeeTransferPeriod?: Date;
  budgetCodeLabel?: string;
  bankName?: string;
  employees?: Array<TransferEmployee>;
}

export interface InternationalTransfer extends Document {
  transliterate?: boolean;
  feeTypeCode?: string;
  feeAccount?: AccountView;
  feeAccountNum?: string;
  currencyOperationType?: string;
  currencyOperationTypeLabel?: string;
  contractNumber?: string;
  contractDate?: Date;
  invoice?: string;
  invoiceDate?: Date;
  contractAuditNumber?: string;
  contractAuditDate?: Date;
  additionalInfo?: string;
  confirmation?: boolean;
  // beneficiary
  benefKpp?: string;
  benefCountry?: string;
  benefCountryCode?: string;
  benefCity?: string;
  benefAddress?: string;
  benefBankCorrAccount?: string;
  benefBankName?: string;
  benefBankCountry?: string;
  benefBankCountryCode?: string;
  benefBankCity?: string;
  benefBankAddress?: string;
  // agent
  agentCorrAccount?: string;
  agentBankName?: string;
  agentBankCode?: string;
  agentBankCountry?: string;
  agentBankCountryCode?: string;
  agentBankCity?: string;
  agentBankAddress?: string;

  fileAttributes?: Array<FileAttribute>;
}

export interface FileAttribute {
  fileId?: number;
  fileName?: string;
  fileSize?: number;
  file?: File;

  fileError?: string;
}

export interface DocumentError {
  id?: string;

  account?: string;
  accountNum?: string;
  amount?: string;
  valueDate?: string;
  purpose?: string;
  purposeCode?: string;
  priority?: string;

  customerName?: string;
  created?: string;
  number?: string;
  state?: string;
  type?: string;
  bankResponse?: string;
  manager?: string;
  info?: string;

  directorName?: string;
  accountant?: string;

  benefName?: string;
  benefTaxCode?: string;
  benefAccount?: string;
  benefBankCode?: string;
  benefResidencyCode?: string;
}

export type TransferEmployee = {
  firstName?: string;
  lastName?: string;
  middleName?: string;
  amount?: number;
  account?: string;
  taxCode?: string;
  birthDate?: Date;
  reason?: string;
  period?: Date;
};

export type CustomerView = {
  id?: number;
  name?: string;
  intlName?: string;
  taxCode?: string;
  residencyCode?: string;
};

export type CustomerViewAdmin = {
  id?: number;
  name?: string;
  intlName?: string;
  taxCode?: string;
};

export type Customer = {
  id?: number;
  organization?: Organization;
};

export type Organization = {
  id?: number;
  name?: string;
  taxCode?: string;
  residencyAndEconomicCode?: string;
};

export type Account = {
  id?: number;
  plannedBalance?: number;
  number?: string;
  balance?: number;
  currency?: string;
};

export type AccountView = {
  id?: number;
  number?: string;
  balance?: number;
  plannedBalance?: number;
  currency?: string;
  alias?: string;
};

export type CompanyPerson = {
  id?: number;
  fullName?: string;
  position?: string;
  sign_level?: string;
};

export type Counterparty = {
  id?: number;
  customer?: { id?: number };
  name?: string;
  internationalName?: string;
  bin?: string;
  beneficiaryCode?: string;
  country?: Country;
  city?: string;
  address?: string;
  notes?: string;
  kpp?: string;
};

export type CounterpartyCount = {
  id?: number;
  counterpartyId?: number;
  counterparty?: Counterparty;
  iban?: string;
  agentIban?: string;
  iso?: string;
  // for local counterparty
  bank?: LocalBank;
  agent?: LocalBank;
  // for international counterparty
  internationalBank?: Bank;
  internationalBankCity?: string;
  internationalBankAddress?: string;
  internationalBankAccount?: string;

  internationalAgent?: Bank;
  internationalAgentCity?: string;
  internationalAgentAddress?: string;
  internationalAgentAccount?: string;
};

export type Country = {
  id?: number;
  countryCode?: string;
  twoLetterCode?: string;
  threeLetterCode?: string;
  countryName?: string;
  fullCountryName?: string;
};

export type LocalBank = {
  id?: number;
  nationalBankBik?: string;
  bankName?: string;
  country?: Country;
};

export type Bank = {
  id?: number;
  internationalBankBik?: string;
  nationalBankBik?: string;

  country?: Country;

  bankName?: string;
  internationalBankName?: string;
  city?: string;
  address?: string;
  bankCode?: string;
  corrBankAccount?: string;
};

export type WorkingAndRestDays = {
  payDays?: Array<Date>;
  nonPayDays?: Array<Date>;
};

export enum TransferEmployeeType {
  PensionContribution,
  SocialContribution,
}

export type ShortPaymentView = {
  id?: number;
  templateName?: string;
};
