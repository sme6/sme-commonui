import * as React from 'react';
import * as mui from 'material-ui';
import icons from '../../layout/icons';
import { translate } from '../../i18n/translator';
import { SS } from '../../backend';

interface Props {
  action: string;
  type: string;
  id?: number;
  listId?: any[];
  typeDocument?: string;
  title?: string;
  dateFrom?: any;
  dateTo?: any;
  fileType?: any;
  formId?: string;
  purpose?: string;
  moneyFrom?: string;
  moneyTo?: string;
  accounts?: string[];
  text?: string;
  accountId?: string;
  bntMessage?: string;
  filterDocument?: string;
  filter?: object;
  style?: React.CSSProperties;
  method?: string;
  btnType?: string;
}

interface State {
  form: () => void;
}

export const DownloadWidgetType = {
  PDF: 'pdf',
  PDF_NEW: 'pdf_new',
  PDF_ALL: 'pdf_all',
  PDF_BUTTON: 'pdf_button',
  PRIMARY_BUTTON: 'primary_button',
  EXCEL: 'excel',
  EXCEL_EXPORT: 'excel_export',
  TXT: 'txt',
  PDF_MENU: 'pdf_menu',
  STATEMENT: 'statement',
  MT: 'mt',
  MTJDE: 'mtjde',
  XML: 'xml',
  '1C': '1c',
  TRANSACTION: 'transaction',
  DEFAULT: 'default',
  ANY_FIlE: 'any_file',
};

export class DownloadWidget extends React.Component<Props, State> {
  static defaultProps = {
    style: { display: 'inline-block' },
    method: 'POST',
  };
  constructor() {
    super();
  }

  componentWillMount() {
    const renderFormType = {};
    const renderSubmitBtnType = {};

    renderSubmitBtnType[DownloadWidgetType.PDF] = () => (
      <mui.MenuItem
        key="print"
        primaryText={translate('common_exporttopdf', 'PDF')}
        leftIcon={icons.pdf}
        href="javascript:document.submitForm.submit()"
      />
    );
    renderSubmitBtnType[DownloadWidgetType.PDF_NEW] = () => (
      <div onClick={() => document.getElementById(this.props.formId).submit()}>
        {this.props.title ? this.props.title : translate('paymentOrder_print', 'Print')}
      </div>
    );

    renderSubmitBtnType[DownloadWidgetType.PDF_ALL] = () => (
      <a
        className="doc-ctrl doc-ctrl-print"
        style={{ position: 'relative' }}
        href="#"
        onClick={() =>
          this.props.listId && this.props.listId.length > 0
            ? window.document[this.props.formId].submit()
            : navigate.to(
                `/download/error?msg=${translate(
                  'document.printError.emptyList',
                  'Список для печати документов пуст',
                )}`,
              )
        }>
        {translate('paymentOrder_print', 'Print')}
      </a>
    );

    renderSubmitBtnType[DownloadWidgetType.EXCEL] = () => (
      <a
        style={{ position: 'relative' }}
        className="doc-ctrl doc-ctrl-export"
        href="javascript:document.submitForm.submit()">
        {translate('common_exporttoexcel')}
      </a>
    );

    renderSubmitBtnType[DownloadWidgetType.EXCEL_EXPORT] = () => (
      <a
        className="doc-ctrl doc-ctrl-print"
        style={{ position: 'relative' }}
        href="#"
        onClick={() => window.document[this.props.formId].submit()}>
        {translate('common_exporttoexcel')}
      </a>
    );

    renderSubmitBtnType[DownloadWidgetType.PDF_BUTTON] = () => (
      <button
        className="btn btn-exchange"
        href="javascript:document.submitForm.submit()"
        style={{ marginRight: '5px' }}>
        {translate('paymentOrder_print')}
      </button>
    );

    renderSubmitBtnType[DownloadWidgetType.TXT] = () => (
      <mui.RaisedButton
        label={this.props.title}
        icon={icons.upload}
        onTouchTap={() => window.document['submitForm'].submit()}
      />
    );

    renderSubmitBtnType[DownloadWidgetType.DEFAULT] = () => (
      <mui.IconButton key="export" title={translate('common_export')} href="javascript:document.submitForm.submit()">
        {icons.downLoad}
      </mui.IconButton>
    );

    renderSubmitBtnType[DownloadWidgetType.PRIMARY_BUTTON] = () => (
      <a className="btn btn-primary" href="#" onClick={() => window.document[this.props.formId].submit()}>
        {this.props.title ? this.props.title : translate('paymentOrder_print', 'Print')}
      </a>
    );

    renderFormType[DownloadWidgetType.PDF_MENU] = () => (
      <form target="_blank" name="submitFormPdf" method="POST" action={this.props.action}>
        <input type="hidden" name="token" value={sessionStorage[SS.TOKEN]} />
        <input type="hidden" name="id" value={this.props.id} />
        <mui.MenuItem
          key="print"
          primaryText={translate('paymentOrder_print', 'Print')}
          leftIcon={icons.print}
          href="javascript:document.submitFormPdf.submit()"
        />
      </form>
    );

    renderFormType[DownloadWidgetType.PDF_NEW] = () => (
      <form id={this.props.formId} name={this.props.formId} target="_blank" method="POST" action={this.props.action}>
        <input type="hidden" name="token" value={sessionStorage['token']} />
        {renderSubmitBtnType[this.props.type.toLowerCase()]() || renderSubmitBtnType[DownloadWidgetType.DEFAULT]()}
      </form>
    );

    renderFormType[DownloadWidgetType.PDF_ALL] = () => (
      <form name={this.props.formId} target="_blank" method="POST" action={this.props.action}>
        <input type="hidden" name="token" value={sessionStorage['token']} />
        {this.props.listId.map(id => <input key={id} type="hidden" name="listId" value={id} />)}
        <input type="hidden" name="typeDocument" value={this.props.typeDocument} />
        {renderSubmitBtnType[this.props.type.toLowerCase()]() || renderSubmitBtnType[DownloadWidgetType.DEFAULT]()}
      </form>
    );

    renderFormType[DownloadWidgetType.EXCEL_EXPORT] = () => (
      <form name={this.props.formId} target="_blank" method="POST" action={this.props.action}>
        <input type="hidden" name="token" value={sessionStorage['token']} />
        {this.props.filter &&
          Object.keys(this.props.filter).map(key => (
            <input key={key} type="hidden" name={key} value={this.props.filter[key]} />
          ))}
        {renderSubmitBtnType[this.props.type.toLowerCase()]() || renderSubmitBtnType[DownloadWidgetType.DEFAULT]()}
      </form>
    );

    renderFormType[DownloadWidgetType.STATEMENT] = () => (
      <form target="_blank" name="submitForm" method="POST" action={this.props.action}>
        <input type="hidden" name="token" value={sessionStorage['token']} />
        <input type="hidden" name="listId" value={this.props.listId} />
        <input type="hidden" name="from" value={this.props.dateFrom} />
        <input type="hidden" name="to" value={this.props.dateTo} />
        <input type="hidden" name="fileType" value={this.props.fileType} />
        <a className="doc-ctrl doc-ctrl-send" href="javascript:document.submitForm.submit()">
          {translate('common_orderTheExcerpt')}
        </a>
      </form>
    );

    renderFormType[DownloadWidgetType.MTJDE] = () => (
      <form name="exportToMtjde" target="_blank" method="POST" action={this.props.action}>
        <input type="hidden" name="token" value={sessionStorage['token']} />

        {this.props.listId.map(id => <input key={id} type="hidden" name="listId" value={id} />)}

        <a
          style={{ textDecoration: 'none' }}
          href="javascript:{}"
          onClick={() => {
            if (this.props.listId && this.props.listId.length > 0) {
              document.getElementsByName('exportToMtjde')[0].submit();
              return false;
            } else {
              navigate.to(
                `/download/error?msg=${translate(
                  'document.printError.emptyList',
                  'Список для печати документов пуст',
                )}`,
              );
            }
          }}>
          {translate('common_export') + ' ' + this.props.type.toUpperCase()}
        </a>
      </form>
    );

    renderFormType[DownloadWidgetType.XML] = () => (
      <form name="exportToXml" target="_blank" method="POST" action={this.props.action}>
        <input type="hidden" name="token" value={sessionStorage['token']} />

        {this.props.listId.map(id => <input key={id} type="hidden" name="listId" value={id} />)}

        <a
          style={{ textDecoration: 'none' }}
          href="javascript:{}"
          onClick={() => {
            if (this.props.listId && this.props.listId.length > 0) {
              document.getElementsByName('exportToXml')[0].submit();
              return false;
            } else {
              navigate.to(
                `/download/error?msg=${translate(
                  'document.printError.emptyList',
                  'Список для печати документов пуст',
                )}`,
              );
            }
          }}>
          {translate('common_export') + ' ' + this.props.type.toUpperCase()}
        </a>
      </form>
    );

    renderFormType[DownloadWidgetType['1C']] = () => (
      <form name="exportTo1c" target="_blank" method="POST" action={this.props.action}>
        <input type="hidden" name="token" value={sessionStorage['token']} />

        {this.props.listId.map(id => <input key={id} type="hidden" name="listId" value={id} />)}

        <a
          style={{ textDecoration: 'none' }}
          href="javascript:{}"
          onClick={() => {
            if (this.props.listId && this.props.listId.length > 0) {
              document.getElementsByName('exportTo1c')[0].submit();
              return false;
            } else {
              navigate.to(
                `/download/error?msg=${translate(
                  'document.printError.emptyList',
                  'Список для печати документов пуст',
                )}`,
              );
            }
          }}>
          {translate('common_export') + ' ' + this.props.type.toUpperCase()}
        </a>
      </form>
    );

    renderFormType[DownloadWidgetType.MT] = () => (
      <form name="exportToMt" target="_blank" method="POST" action={this.props.action}>
        <input type="hidden" name="token" value={sessionStorage['token']} />

        {this.props.listId.map(id => <input key={id} type="hidden" name="listId" value={id} />)}

        <a
          style={{ textDecoration: 'none' }}
          href="javascript:{}"
          onClick={() => {
            if (this.props.listId && this.props.listId.length > 0) {
              document.getElementsByName('exportToMt')[0].submit();
              return false;
            } else {
              navigate.to(
                `/download/error?msg=${translate(
                  'document.printError.emptyList',
                  'Список для печати документов пуст',
                )}`,
              );
            }
          }}>
          {translate('common_export') + ' ' + this.props.type.toUpperCase()}
        </a>
      </form>
    );

    const commonForm = () => (
      <form target="_blank" name="submitForm" method="POST" action={this.props.action}>
        <input type="hidden" name="token" value={sessionStorage['token']} />
        <input type="hidden" name="id" value={this.props.id} />
        <input type="hidden" name="listId" value={this.props.listId} />
        <input type="hidden" name="typeDocument" value={this.props.typeDocument} />
        <input type="hidden" name="filterDocument" value={this.props.filterDocument} />
        {renderSubmitBtnType[this.props.type.toLowerCase()]() || renderSubmitBtnType[DownloadWidgetType.DEFAULT]()}
      </form>
    );

    renderFormType[DownloadWidgetType.TRANSACTION] = () => (
      <form target="_blank" name={this.props.formId} method="POST" action={this.props.action}>
        <input type="hidden" name="token" value={sessionStorage['token']} />
        <input type="hidden" name="type" value={this.props.typeDocument} />
        <input type="hidden" name="accountId" value={this.props.accountId} />
        <input type="hidden" name="from" value={this.props.dateFrom} />
        <input type="hidden" name="to" value={this.props.dateTo} />
        <input type="hidden" name="purpose" value={this.props.purpose} />
        <input type="hidden" name="moneyFrom" value={this.props.moneyFrom} />
        <input type="hidden" name="moneyTo" value={this.props.moneyTo} />
        {this.props.accounts &&
          this.props.accounts.map(id => <input key={id} type="hidden" name="accounts" value={id} />)}
        <input type="hidden" name="text" value={this.props.text} />
        <mui.RaisedButton
          label={this.props.bntMessage}
          primary={true}
          style={{ margin: '0 10px 0 0' }}
          onTouchTap={() => window.document[this.props.formId].submit()}
        />
      </form>
    );

    renderFormType[DownloadWidgetType.PDF] = commonForm;
    renderFormType[DownloadWidgetType.EXCEL] = commonForm;
    renderFormType[DownloadWidgetType.PDF_BUTTON] = commonForm;
    renderFormType[DownloadWidgetType.TXT] = commonForm;
    renderFormType[DownloadWidgetType.DEFAULT] = commonForm;

    renderFormType[DownloadWidgetType.ANY_FIlE] = () => (
      <form name={this.props.formId} target="_blank" method={this.props.method} action={this.props.action}>
        <input type="hidden" name="token" value={sessionStorage['token']} />
        {this.props.filter &&
          Object.keys(this.props.filter).map(key => (
            <input key={key} type="hidden" name={key} value={this.props.filter[key]} />
          ))}

        {this.props.btnType ? (
          renderSubmitBtnType[this.props.btnType.toLowerCase()]()
        ) : (
          <mui.FlatButton label={this.props.title} onTouchTap={() => window.document[this.props.formId].submit()} />
        )}
      </form>
    );

    this.setState({
      form: renderFormType[this.props.type.toLowerCase()] || renderFormType[DownloadWidgetType.DEFAULT],
    });
  }

  render() {
    return <div style={this.props.style}>{this.state.form()}</div>;
  }
}
