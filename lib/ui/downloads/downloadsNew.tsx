import * as React from 'react';
import * as mui from 'material-ui';
import icons from '../../layout/icons';
import { translate } from '../../i18n/translator';

interface Props {
  action: string;
  type: string;
  id?: number;
  listId?: any[];
  typeDocument?: string;
  title?: string;
  dateFrom?: any;
  dateTo?: any;
  fileType?: any;
  formId?: string;
  purpose?: string;
  moneyFrom?: string;
  moneyTo?: string;
  accounts?: string[];
  text?: string;
  accountId?: string;
  bntMessage?: string;
  filterDocument?: string;
  closeAction?(): void;
}

interface State {
  form: () => void;
}

export const DownloadWidgetType = {
  PDF: 'pdf',
  PDF_BUTTON: 'pdf_button',
  EXCEL: 'excel',
  TXT: 'txt',
  PDF_MENU: 'pdf_menu',
  STATEMENT: 'statement',
  MT: 'mt',
  TRANSACTION: 'transaction',
  DEFAULT: 'default',
};

export class DownloadWidgetNew extends React.Component<Props, State> {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    const renderFormType = {};
    const renderSubmitBtnType = {};

    renderSubmitBtnType[DownloadWidgetType.PDF] = () => (
      <mui.MenuItem
        key="print"
        primaryText={translate('common_exporttopdf', 'PDF')}
        leftIcon={icons.pdf}
        href="javascript:document.submitForm.submit()"
      />
    );

    renderSubmitBtnType[DownloadWidgetType.EXCEL] = () => (
      <mui.IconButton
        key="export"
        title={translate('common_exporttoexcel')}
        href="javascript:document.submitForm.submit()">
        {icons.excel}
      </mui.IconButton>
    );

    renderSubmitBtnType[DownloadWidgetType.PDF_BUTTON] = () => (
      <button
        className="btn btn-exchange"
        href="javascript:document.submitForm.submit()"
        style={{ marginRight: '10px' }}>
        {translate('paymentOrder_print')}
      </button>
    );

    renderSubmitBtnType[DownloadWidgetType.TXT] = () => (
      <mui.RaisedButton
        label={this.props.title}
        icon={icons.upload}
        onTouchTap={() => window.document['submitForm'].submit()}
      />
    );

    renderSubmitBtnType[DownloadWidgetType.DEFAULT] = () => (
      <mui.IconButton key="export" title={translate('common_export')} href="javascript:document.submitForm.submit()">
        {icons.downLoad}
      </mui.IconButton>
    );

    renderFormType[DownloadWidgetType.PDF_MENU] = () => (
      <form target="_blank" name="submitFormPdf" method="POST" action={this.props.action}>
        <input type="hidden" name="token" value={sessionStorage['token']} />
        <input type="hidden" name="id" value={this.props.id} />
        <mui.MenuItem
          key="print"
          primaryText={translate('paymentOrder_print', 'Print')}
          leftIcon={icons.print}
          href="javascript:document.submitFormPdf.submit()"
        />
      </form>
    );

    renderFormType[DownloadWidgetType.STATEMENT] = () => (
      <form
        target="_blank"
        name="submitForm"
        method="POST"
        action={this.props.action}
        style={{ margin: '5px', width: '100px' }}>
        <input type="hidden" name="token" value={sessionStorage['token']} />
        <input type="hidden" name="listId" value={this.props.listId} />
        <input type="hidden" name="from" value={this.props.dateFrom} />
        <input type="hidden" name="to" value={this.props.dateTo} />
        <input type="hidden" name="fileType" value={this.props.fileType} />
        <button
          key="STATEMENTa1"
          className="consult-input-send consult-input-btn"
          href="javascript:document.submitForm.submit()">
          {'Заказать'}
        </button>
      </form>
    );

    renderFormType[DownloadWidgetType.MT] = () => (
      <form name={this.props.formId} target="_blank" method="POST" action={this.props.action}>
        <input type="hidden" name="token" value={sessionStorage['token']} />

        {this.props.listId.map(id => <input key={id} type="hidden" name="listId" value={id} />)}

        <mui.RaisedButton
          label={this.props.title}
          icon={icons.upload}
          onTouchTap={() => window.document[this.props.formId].submit()}
        />
      </form>
    );

    const commonForm = () => (
      <form target="_blank" name="submitForm" method="POST" action={this.props.action}>
        <input type="hidden" name="token" value={sessionStorage['token']} />
        <input type="hidden" name="id" value={this.props.id} />
        <input type="hidden" name="listId" value={this.props.listId} />
        <input type="hidden" name="typeDocument" value={this.props.typeDocument} />
        <input type="hidden" name="filterDocument" value={this.props.filterDocument} />
        {renderSubmitBtnType[this.props.type.toLowerCase()]() || renderSubmitBtnType[DownloadWidgetType.DEFAULT]()}
      </form>
    );

    renderFormType[DownloadWidgetType.TRANSACTION] = () => (
      <form target="_blank" name={this.props.formId} method="POST" action={this.props.action}>
        <input type="hidden" name="token" value={sessionStorage['token']} />
        <input type="hidden" name="type" value={this.props.typeDocument} />
        <input type="hidden" name="accountId" value={this.props.accountId} />
        <input type="hidden" name="from" value={this.props.dateFrom} />
        <input type="hidden" name="to" value={this.props.dateTo} />
        <input type="hidden" name="purpose" value={this.props.purpose} />
        <input type="hidden" name="moneyFrom" value={this.props.moneyFrom} />
        <input type="hidden" name="moneyTo" value={this.props.moneyTo} />
        {this.props.accounts &&
          this.props.accounts.map(id => <input key={id} type="hidden" name="accounts" value={id} />)}
        <input type="hidden" name="text" value={this.props.text} />
        <mui.RaisedButton
          label={this.props.bntMessage}
          primary={true}
          style={{ margin: '0 10px 0 0' }}
          onTouchTap={() => window.document[this.props.formId].submit()}
        />
      </form>
    );

    renderFormType[DownloadWidgetType.PDF] = commonForm;
    renderFormType[DownloadWidgetType.EXCEL] = commonForm;
    renderFormType[DownloadWidgetType.PDF_BUTTON] = commonForm;
    renderFormType[DownloadWidgetType.TXT] = commonForm;
    renderFormType[DownloadWidgetType.DEFAULT] = commonForm;

    this.setState({
      form: renderFormType[this.props.type.toLowerCase()] || renderFormType[DownloadWidgetType.DEFAULT],
    });
  }

  render() {
    return <div style={{ display: 'inline-block' }}>{this.state.form()}</div>;
  }
}
