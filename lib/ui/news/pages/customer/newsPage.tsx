import * as React from 'react';
import { get } from '../../../../backend';
import { date } from '../../../../format';
import { translate } from '../../../../i18n/translator';
import { siteConfig } from '../../../../siteConfig';

interface News {
  id: number;
  newsDate: Date;
  newsDateCreate: Date;
  newsDatePublication: Date;
  head: string;
  newsDesc: string;
  link: string;
  status: boolean;
}

interface GridResponse<R> {
  rows: R[];
  total?: {
    count?: number;
  };
}

interface NewsWidgetState {
  loading?: boolean;
  news?: News[];
  total?: number;
  showActualNews?: boolean;
}

export class NewsPage extends React.Component<any, NewsWidgetState> {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      news: [],
      showActualNews: true,
    };
  }

  loadNewsFromServer(isActual = true) {
    get('/news/customer-news', {
      sort: 'newsDate',
      isActual,
    })
      .then(d => {
        this.setState({ news: (d as GridResponse<News>).rows, loading: false });
      })
      .catch(e => {
        this.setState({ loading: false });
        console.log(e);
      });
  }

  loadActualNews = async () => {
    this.setState({ showActualNews: true });
    await this.loadNewsFromServer(true);
  };

  loadOldNews = async () => {
    this.setState({ showActualNews: false });
    await this.loadNewsFromServer(false);
  };

  async componentWillMount() {
    await this.loadNewsFromServer();
  }

  render() {
    const { news, showActualNews } = this.state;

    return (
      <div className="content-fullwidth">
        <div className="container">
          <div className="dep-page">
            <div className="dep-tabs tab-wrap-link">
              <div
                className={'dep-tabs-item tab-item' + (showActualNews ? ' active' : '')}
                onClick={this.loadActualNews}>
                {translate('navigation_news')}
              </div>
              <div className={'dep-tabs-item tab-item' + (!showActualNews ? ' active' : '')} onClick={this.loadOldNews}>
                {translate('archive_news')}
              </div>
            </div>
            <div className="dep-content">
              <div className="dep-wrap news-container">
                <aside className="dep-sidebar">
                  <div className="w-wrapper w-news">
                    <div className="w-header w-header-hl">
                      <h3 className="w-title w-title-no-icon">{translate('news_main')}</h3>
                    </div>
                    <div className="w-content w-closest-block">
                      {news.slice(0, 3).map((item, index) => (
                        <div className="w-news-item" key={`main-news${index}`}>
                          <time className="w-news-time">{date(item.newsDate)}</time>
                          <a className="w-news-link" href={item.link} target="_blank">
                            {item.head}
                          </a>
                        </div>
                      ))}
                      {!news.length && <p>{translate('noNews')}</p>}
                    </div>
                  </div>
                </aside>
                <div className="dep-tabs-news-content-wrap">
                  <div className="dep-tabs-content tab-item-cont active">
                    <div className="news-banner">
                      {news[0] && (
                        <div className="news-banner-caption">
                          <div className="news-banner-time">{date(news[0].newsDate)}</div>
                          <div className="news-banner-link">
                            <a href={news[0].link} target="_blank">
                              {news[0].head}
                            </a>
                          </div>
                        </div>
                      )}
                    </div>
                    <div className="news-list">
                      {news.map((item, index) => (
                        <div className="news-list-item" key={`news${index}`}>
                          <div className="news-list-item-time">{date(item.newsDate)}</div>
                          <h3 className="news-list-item-title">
                            <a href={item.link} target="_blank">
                              {item.head}
                            </a>
                          </h3>
                          <div
                            className="news-list-item-content"
                            style={{
                              whiteSpace: 'pre-line',
                              wordWrap: 'break-word',
                            }}>
                            {item.newsDesc}
                          </div>
                        </div>
                      ))}
                      {!news.length && <p>{translate('noNews')}</p>}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
