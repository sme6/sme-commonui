import React, { CSSProperties } from 'react';
import { cls } from './layout/views';
import * as validator from './validator';
import { message, validateOneField, Constraint, letterAndNumbers } from './validator';

export interface ControlProps {
  label?: any;
  name: string; // key in the state & fieldErrors
  blockClassName?: string;
  invalidClassName?: string;
  labelClassName?: string;
  wrapperClassName?: string;
  wrapperStyle?: CSSProperties;
  errorClassName?: string;
  nativeOnChange?: boolean; // when true - class Control does not override method onChange(), bu default false.
  nativeOnBlur?: boolean; // when true - class Control does not override method onChange(), bu default false.
  onlyValue?: boolean;
  innerName?: string;
  customClassName?: string;
  customStyle?: object;
  callback?: (val) => any;
  handleOnChange?: any;
}

/**
 * Provides:
 * - retreiving/update value from control to parent's state
 * - validation triggers
 * - invalid control indication
 *
 * Nested component should accept properties
 * - className
 * - value
 * - onChange
 * - onBlur
 *
 * Validation triggers.
 * If there was an error in the field, trigger validation onChange
 * If there was no error in the field, trigger validation onBlur
 *
 * Full markup example
 * <div class="$blockClassName $invalidClassName">
 *     <div title="$error" class="$labelClassName $invalidClassName">
 *         $label
 *     </div>
 *     <div class="$wrapperClassName">
 *         $children
 *     </div>
 *     <div class="$errorClassName">
 *         $error
 *      </div>
 * </div>
 */
export class Control extends React.Component<ControlProps, any> {
  static defaultProps = {
    onChange: () => {},
    onBlur: () => {},
    callback: val => {},
    nativeOnChange: false,
    nativeOnBlur: false,
    onlyValue: false,
    customClassName: '',
    customStyle: null,
  };

  static contextTypes = {
    formState: React.PropTypes.object,
    formClassNames: React.PropTypes.object,
  };

  render() {
    const classNames: FormClassNames = {
      block: this.props.blockClassName || this.context.formClassNames.block,
      invalid: this.props.invalidClassName || this.context.formClassNames.invalid,
      label: this.props.labelClassName || this.context.formClassNames.label,
      wrapper: this.props.wrapperClassName || this.context.formClassNames.wrapper,
      error: this.props.errorClassName || this.context.formClassNames.error,
    };

    if (this.props.label && !classNames.block) {
      throw new Error('blockClassName is required when using rendering label');
    }

    if (!classNames.invalid) {
      throw new Error('invalidClassName is required');
    }

    const child = this.props.children as React.ReactElement<any>;
    const { name, onlyValue, nativeOnBlur } = this.props;

    const { state } = this.context.formState;
    const { fieldErrors } = state;

    const hasError = fieldErrors ? !!fieldErrors[name] : false;
    let fieldValue = state[name] ? state[name] : '';
    if (state[name] && this.props.innerName && this.props.innerName.length > 0) {
      fieldValue = state[name][this.props.innerName];
    }
    const controlElement =
      onlyValue === true
        ? state[name]
        : React.cloneElement(child, {
            className: cls({ [classNames.invalid]: hasError }, child.props.className),
            value: fieldValue,
            onChange: this.onChange,
            onBlur: nativeOnBlur ? child.props.onBlur : this.onBlur,
            title: fieldErrors ? fieldErrors[name] : '',
          });

    if (!classNames.block) {
      return controlElement;
    }

    return (
      <div className={cls({ [classNames.invalid]: hasError }, classNames.block)}>
        {this.props.label && (
          <div title={fieldErrors[name]} className={cls({ [classNames.invalid]: hasError }, classNames.label)}>
            {this.props.label}
          </div>
        )}
        {classNames.wrapper ? (
          <div
            className={`${classNames.wrapper} ${this.props.customClassName}`}
            style={{ ...this.props.wrapperStyle, ...this.props.customStyle }}>
            {controlElement}
          </div>
        ) : (
          controlElement
        )}
        {classNames.error && hasError && <div className={classNames.error}>{fieldErrors[name]}</div>}
      </div>
    );
  }

  onChange = (e, index, ...others) => {
    this.props.handleOnChange && this.props.handleOnChange();
    const child = this.props.children as React.ReactElement<any>;
    const { name, callback } = this.props;
    if (name === 'birthDate' && e) {
      e = e._d.toLocaleDateString('ru-ru').replace(/[^ -~]/g, '');
    }
    const { setState, state, constraints } = this.context.formState;
    const { fieldErrors } = state;
    const val = e.target ? e.target.value : e;

    if (this.props.nativeOnChange === true) {
      child.props.onChange(e, index, ...others);
      return;
    }
    const update = { [name]: val };
    // if have field error - update validation message
    if (fieldErrors && fieldErrors[name]) {
      const error = validator.message(constraints[name], val);
      if (error !== fieldErrors[name]) {
        update.fieldErrors = { ...state.fieldErrors, [name]: error };
      }
    }

    // if (this.props.nativeOnChange === true) {
    //     delete update[name];
    // }
    setState(update, () => callback(val));
  };

  onBlur = () => {
    const { name } = this.props;
    const { state, constraints, setState } = this.context.formState;
    const { fieldErrors } = state;

    if (!fieldErrors || !constraints) {
      return;
    }
    const error = validator.message(constraints[name], state[name]);
    if (error !== fieldErrors[name]) {
      setState({
        fieldErrors: {
          ...state.fieldErrors,
          [name]: error,
        },
      });
    }
  };
}

/**
 * All Controls should be wrapped inside FormContext
 */
interface FormState {
  state?: object;
  constraints?: object;
  classNames?: FormClassNames;
  setState(update: object, callback?: any): void;
}

interface FormClassNames {
  block?: string;
  invalid?: string;
  label?: string;
  wrapper?: string;
  error?: string;
}

export class FormContext extends React.Component<FormState, any> {
  static defaultProps = {
    classNames: {
      label: 'doc-filter-label',
      invalid: 'invalid',
    },
  };

  render() {
    return <div>{this.props.children}</div>;
  }

  getChildContext() {
    return {
      formState: {
        state: this.props.state,
        setState: this.props.setState,
        constraints: this.props.constraints,
      },
      formClassNames: this.props.classNames,
    };
  }

  static childContextTypes = {
    formState: React.PropTypes.object,
    formClassNames: React.PropTypes.object,
  };
}

export function hasFieldErrors(state): boolean {
  return !!Object.keys(state.fieldErrors).find(key => state.fieldErrors[key]);
}

export function validateFields(constraints, fields) {
  const result = {};

  Object.keys(constraints).map(item => {
    result[item] = message(constraints[item], fields ? fields[item] : null);
  });

  return result;
}

export function validateInnerFields(constraints: { [P: string]: Constraint }, innerFields) {
  const innerConstraints = {};

  Object.keys(innerFields).forEach(key => {
    constraints[key] && (innerConstraints[key] = constraints[key]);
  });

  return validateFields(innerConstraints, innerFields);
}

export function validateFieldsDeep(constraints: { [P: string]: Constraint }, ...fieldsList) {
  let result = {};

  fieldsList.forEach(fields => {
    result = { ...result, ...validateInnerFields(constraints, fields) };
  });

  return result;
}

export class FieldErrors {
  fieldErrors: Object;
  blockSave?: boolean;

  constructor() {
    this.fieldErrors = {};
    this.blockSave = false;
  }
}

export class FieldError {
  fieldError: string;
  isRequired?: boolean;

  constructor() {
    this.fieldError = '';
    this.isRequired = false;
  }
}

// uses for new type of validation
export function validateFields1(constraints, fields): FieldErrors {
  const result: FieldErrors = new FieldErrors();

  Object.keys(constraints).map(item => {
    const error: FieldError = validateOneField(constraints[item], fields ? fields[item] : null, '', fields);
    result.fieldErrors[item] = error.fieldError;

    if (error.isRequired) {
      result.blockSave = true;
    }
  });

  return result;
}
