import { sideEffect } from 'redux-action-object';
import { setAuthToken } from '../backend';

export interface Document {
  id?: number;
  entityId?: number;
  name?: string;
  url?: string;
  created?: Date;
}

export interface HttpParamsOptions {
  [param: string]: string | number | boolean | Readonly<string | number | boolean>[];
}

export const PersonType = {
  LegalEntity: 'LegalEntity',
  PrivatePerson: 'PrivatePerson',
};

export interface MenuItem {
  link?: string;
  label: string;
  children?: MenuItem[];
  visibility: string;
}

export interface SelectorItem {
  value: any;
  label: string;
  smLabel?: string;
}

export class ApplicationModel implements State, Actions {
  token: string;
  privileges = [];

  login(token) {
    sideEffect(() => {
      sessionStorage.setItem('token', token);
      setAuthToken(token);
    });
    return {
      token,
    };
  }

  logout() {
    sideEffect(() => {
      setAuthToken(null);
      sessionStorage.removeItem('token');
      sessionStorage.setItem('visibility', '[]');
      this.setPrivileges([]);
    });

    return {
      token: null,
    };
  }

  setPrivileges(privileges: string[]) {
    this.privileges = privileges;
    sessionStorage.setItem('visibility', JSON.stringify(privileges));

    return {
      privileges: this.privileges,
    };
  }

  setSessionExpireTime(sessionExpire: SessionExpire) {
    return {
      sessionExpire,
    };
  }
}

export interface AccountView {
  id?: number;
  number?: string;
  balance?: number;
  plannedBalance?: number;
  currency?: string;
}

export interface Counterparty {
  id?: number;
  customer?: { id?: number };
  name?: string;
  internationalName?: string;
  bin?: string;
  beneficiaryCode?: string;
  country?: Country;
  city?: string;
  address?: string;
  notes?: string;
  kpp?: string;
}

export interface CounterpartyCount {
  id?: number;
  counterpartyId?: number;
  counterparty?: Counterparty;
  iban?: string;
  agentIban?: string;
  iso?: string;
  // for local counterparty
  bank?: LocalBank;
  agent?: LocalBank;
  // for international counterparty
  internationalBank?: Bank;
  internationalBankCity?: string;
  internationalBankAddress?: string;
  internationalBankAccount?: string;

  internationalAgent?: Bank;
  internationalAgentCity?: string;
  internationalAgentAddress?: string;
  internationalAgentAccount?: string;
}

export interface Country {
  id?: number;
  countryCode?: string;
  twoLetterCode?: string;
  threeLetterCode?: string;
  countryName?: string;
  fullCountryName?: string;
}

export interface LocalBank {
  id?: number;
  nationalBankBik?: string;
  bankName?: string;
  country?: Country;
}

export interface TariffDetailDTO {
  serverId: string;
  algType: string;
  certificate: string;
  certificateInit: string;
}

export interface ChatMessage {
  id?: number;
  count?: number;
  idConfirm?: number;
  messageText?: string;
  companyPersonId?: number;
  customerUserName?: string;
  customerCompanyName?: string;
  customerTaxCode?: string;
  managerId?: number;
  managerName?: string;
  messageDate?: Date;
  isFromCompanyPerson?: boolean;
  isRead?: boolean;
  files?: ChatMessageAttachment[];
  cmd?: string;
  dialogId?: number;
}

export interface ChatDialog {
  id?: number;
  dialogId?: number;
  companyPersonId?: number;
  customerUserName?: string;
  customerCompanyName?: string;
  customerTaxCode?: string;
  managerId?: number;
  managerName?: string;
  files?: ChatMessageAttachment[];
  cmd?: string;
  idConfirm?: number;
  isFromCompanyPerson?: boolean;
  count?: number;
  isOpened?: boolean;
  LastSentMessage?: string;
}

export interface ChatMessageAttachment {
  id?: number;
  chatMessageId?: number;
  name?: string;
  contentType?: string;
}

export interface DialogItem {
  companyPersonId?: number;
  customerUserName?: string;
  customerCompanyName?: string;
  customerTaxCode?: string;
  managerId?: number;
  managerName?: string;
  count?: number;
  isOpened?: boolean;
}

export interface DemandCategoryDTO {
  id: number;
  isDeleted?: boolean;
  code?: string;
  name?: string;
}

export interface DemandGrid {
  id: number;
  organization?: string;
  created?: string;
  number?: string;
  category?: string;
  documentType?: string;
  docType?: string;
  state?: string;
  actions?: any;
}
