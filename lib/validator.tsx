import XRegExp from 'xregexp';
import { translate } from './i18n/translator';
import { decodeMaskedAmount } from './format';
import { FieldError, FieldErrors } from './controls';
import { SelectorItem } from './models/models';
import { get } from './backend';

const IBAN = require('iban');

export const lettersOrDash = XRegExp('^[\\pL-әіңғүұқөһӘІҢҒҮҰҚӨҺ._ 0-9]*$');
export const phone = XRegExp('^[0-9+]+$');
export const numbers = XRegExp('^[0-9]+$');
export const numbersAndPoints = XRegExp('^[0-9.]+$');
export const date = XRegExp('^[0-3][0-9][.][0-1][0-9][.][0-9]{4}?$');
export const numbersWithoutLeadingZero = XRegExp('^([1-9][0-9]{0,10})$');
export const letterAndNumbers = XRegExp('^[a-zA-Z0-9]+$');
export const intTransferSenderName = XRegExp("^[a-zA-Z0-9а-яА-Я,' ]+$");
export const latinCharacterAndSlash = XRegExp('^[^а-яА-Я]+$');
export const notUsedSymbols = XRegExp('');
export const letters = XRegExp("^[a-zA-Z0-9-/?:().,‘'+\r\n ]+$");
export const latinLetters = XRegExp('^[a-zA-Z]+$');
export const latinAndCyrillicLetters = XRegExp("^[а-яА-ЯёЁa-zA-Z0-9-/?':().,‘+\r\n ]+$");
export const upperLetters = XRegExp('^[A-Z ]+$');
export const upperLetterAndNumbers = XRegExp('^[A-Z0-9]+$');
export const vinCode = /^[A-Z0-9]+$/;
export const amount = XRegExp('^[0-9 ]+([.][0-9|]{1,2})?$');
export const exchangeRate = XRegExp('^[0-9]+([.][0-9|]{1,4})?$');
export const residencyCode = XRegExp('^[1-2]+[0-9]');
export const KZTAccount = XRegExp('^[KZ]+[a-zA-Z0-9]+$');
export const name = XRegExp('^[ ,а-яА-ЯёЁa-zA-Z,-әіңғүұқөһӘІҢҒҮҰҚӨҺ]+$');
export const shortName = XRegExp('^[ ,а-яА-ЯёЁa-zA-Z,-.]+$');
export const password = XRegExp('/(?=^.{2,}$)((?=.*d)|(?=.*W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/g');
export const link = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/;

export const constraints = {
  lettersOrDash,
  phone,
  numbers,
  numbersAndPoints,
  date,
  numbersWithoutLeadingZero,
  letterAndNumbers,
  upperLetters,
  latinCharacterAndSlash,
  notUsedSymbols,
  letters,
  latinAndCyrillicLetters,
  upperLetterAndNumbers,
  intTransferSenderName,
  vinCode,
  amount,
  exchangeRate,
  residencyCode,
  KZTAccount,
  name,
  shortName,
  password,
  link,
};

export const genders = {
  male: 'Male',
  female: 'Female',
};

// http://blog.mattheworiordan.com/post/13174566389/url-regular-expression-for-links-with-or-without

export interface Constraint {
  required?: boolean;
  regexp?: RegExp;
  regexpr?: string;
  email?: boolean;
  account?: boolean;
  validateAccount?: boolean;
  checkBenefCard?: boolean;
  taxCode?: boolean;
  greaterThanZero?: boolean;
  maxLength?: number;
  minLength?: number;
  length?: number;
  regExchangeRate?: RegExp;
  password?: boolean;
  amount?: boolean;
  amountNotNull?: boolean; // uses for maskedAmount
  checkbox?: boolean;
  selected?: boolean; // filter invalid selection (none)
  iban?: boolean;
  hasProperty?: string;
  disabledNames?: string[];
  checkDB?: string; // On back check if value present in database. On front check if present as label im Array<SelectorItem> in state
  validateValueDate?: boolean; // validate on backend value date. value date should be in permitted payment days
  labelOf?: SelectorItem[];

  errorText?: string;
}

export interface GlobalConstraint {
  redBalance: boolean | 'warn';
  uniqueMainFields: boolean | 'warn';
  validateAccountByBankCode: boolean | 'warn';
}

export function message(constraints, value, regexSpecialTranslation?: string, state = null) {
  // uses for old type of validation
  if (!Array.isArray(constraints)) {
    return getError(constraints, value);
  }

  // uses for new type of validation
  for (const item of constraints) {
    const error = getError(convertToConstraint(item), value, regexSpecialTranslation, state);
    if (error && item.isRequired !== 'FALSE') {
      return error;
    }
  }
}

// uses for new type of validation
export function validateOneField(constraints, value, regexSpecialTranslation?: string, state = null): FieldError {
  const result = new FieldError();

  for (const item of constraints) {
    const error = getError(convertToConstraint(item), value, regexSpecialTranslation, state);
    if (error && item.isRequired !== 'FALSE') {
      result.fieldError = error;
      if (item.isRequired === 'TRUE') {
        result.isRequired = true;
        return result;
      }
    }
  }

  return result;
}

function validateTranslation(value) {
  return value['ru'] === undefined || value['kk'] === undefined || value['en'] === undefined;
}

function notUsedSymbolsWithOutRegex(constraint, value) {
  const mustNotUseSymbols = [
    '_',
    '@',
    '"',
    '^',
    '~',
    ';',
    '!',
    '{',
    '}',
    '[',
    ']',
    '<',
    '>',
    '|',
    '%',
    '#',
    '$',
    '&',
    '*',
    '№',
    String.fromCodePoint(92),
  ];
  if (String(constraint.regexp) === 'latinCharacterAndSlash' || String(constraint.regexp) === 'notUsedSymbols') {
    return Array.from(value).some(item => mustNotUseSymbols.includes(String(item)));
  }
  return false;
}

export function getError(constraint: Constraint, value: string, regexSpecialTranslation?: string, state = null) {
  if (!constraint) {
    return;
  }

  if (value == null) {
    value = '';
  }

  const customErrorText = constraint.errorText; // get error text from database. If its empty uses default error text

  if (constraint.required) {
    if (typeof value === 'object') {
      if (Array.isArray(value)) {
        if (value.length == 0) return customErrorText || translate('common_mandatory_field');
      } else if (value instanceof Date) {
        if (!value) {
          return customErrorText || translate('common_mandatory_field');
        }
      } else if (
        value['id'] == undefined &&
        value['isoCode'] == undefined &&
        value['hour'] == undefined &&
        validateTranslation(value)
      ) {
        return customErrorText || translate('common_mandatory_field');
      }
    } else {
      if (value === '') {
        return customErrorText || translate('common_mandatory_field');
      }
    }
  }

  if (value === '') {
    return;
  } // by default field is not required!

  if (constraint.latinCharacter) {
    const regex = /^[a-zA-Z.,0-9-_\n\s]+$/; // latin character
    if (!regex.test(value)) {
      return customErrorText || translate('latin_character');
    }
  }

  if (constraint.regexp) {
    if (notUsedSymbolsWithOutRegex(constraint, value)) {
      return customErrorText || regexSpecialTranslation || translate('common_incorrectSymbol');
    }
    if (typeof constraint.regexp === 'string') {
      constraint.regexp = constraints['' + constraint.regexp];
    }
    if (!constraint.regexp.test(value)) {
      return customErrorText || regexSpecialTranslation || translate('common_incorrectSymbol');
    }
  }

  if (constraint.maxLength) {
    if (value.length > constraint.maxLength) {
      return (
        customErrorText ||
        `${translate('common_maxLength')} ${constraint.maxLength} ${translate('common_symbolsForMaxLength')}`
      );
    }
  }

  if (constraint.minLength) {
    if (value.length < constraint.minLength) {
      return (
        customErrorText ||
        `${translate('common_minLength')} ${constraint.minLength} ${translate('common_symbolsForLength')}`
      );
    }
  }

  if (constraint.length) {
    if (value.length !== +constraint.length) {
      return (
        customErrorText || `${translate('common_length')} ${constraint.length} ${translate('common_symbolsForLength')}`
      );
    }
  }

  if (constraint.email) {
    if (!validateEmail(value)) {
      return customErrorText || translate('common_incorrectEmail');
    }
  }

  if (constraint.password) {
    if (!validatePassword(value)) {
      return customErrorText || translate('password_policy');
    }
  }

  if (constraint.taxCode) {
    if (!validateTaxCode(value)) {
      return customErrorText || translate('paymentOrder_valid_wrongCode');
    }
  }

  if (constraint.account) {
    if (!validateAccount(value.toLocaleLowerCase())) {
      return customErrorText || translate('common_incorrectSymbol');
    }
  }

  if (constraint.checkBenefCard) {
    if (!validateCard(value)) {
      return customErrorText || translate('error_card_does_not_supported');
    }
  }

  if (constraint.amount) {
    if (!isValidAmount(value)) {
      return customErrorText || translate('paymentOrder_valid_wrongFormat');
    }
  }

  if (constraint.regExchangeRate) {
    if (!constraint.regExchangeRate.test(value)) {
      return customErrorText || translate('paymentOrder_valid_wrongFormat');
    }
  }

  if (constraint.greaterThanZero) {
    if (!(+value > 0)) {
      return customErrorText || translate('common_greaterThanZero');
    }
  }

  if (constraint.amountNotNull) {
    if (!(decodeMaskedAmount(value) > 0)) {
      return customErrorText || translate('common_greaterThanZero');
    }
  }

  if (constraint.selected) {
    if (value === 'none') {
      return customErrorText || translate('common_invalidSelect', 'Пожалуйста, выберите значения из списка');
    }
  }

  if (constraint.checkbox) {
    if (!value) {
      return customErrorText || translate('common_mandatory_field');
    }
  }

  if (constraint.iban) {
    if (!IBAN.isValid(value)) {
      return customErrorText || translate('common.iban.wrongCode');
    }
  }

  if (constraint.hasProperty) {
    if (value[constraint.hasProperty] === undefined) {
      return customErrorText || translate('common_mandatory_field');
    }
  }

  if (constraint.disabledNames) {
    const disabledNames = constraint.disabledNames;
    const disableName = disabledNames.find(item => value.toLowerCase().includes(item));
    if (disableName) {
      return customErrorText || `${translate('should_not_contain')}: ${disabledNames}`;
    }
  }

  if (constraint.checkDB && state != null) {
    const map = state[constraint.checkDB];
    if (map == null) {
      console.error('ERROR: Could not find array ' + constraint.checkDB);
      return;
    }

    return !map.find(item => item.label === value)
      ? customErrorText || translate('paymentOrder_valid_codeNotExist')
      : '';
  }

  if (constraint.labelOf && value) {
    const item = constraint.labelOf.find(i => i.label === value);

    if (!item) {
      return customErrorText || 'Пожалуйста, выберите значения из списка';
    }
  }
}

export function validateEmailByGeneralRegex(email) {
  const generalRegexForEmailValidation = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
  return generalRegexForEmailValidation.test(email);
}

export function validatePhoneByRegex(phone) {
  const phoneRegex = /^[+7][(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;
  return phoneRegex.test(phone);
}

const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export function validateEmail(email) {
  return re.test(email);
}

export function validatePassword(pass) {
  const pattern = sessionStorage.getItem('password_validation_constant');
  if (pattern) {
    return pass.match(new RegExp(pattern));
  } else {
    const regex = /(?=^\S{2,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z])(?=.*[!#$%^&()@*]).*$/g;
    return regex.test(pass);
  }
}

export function isValidAmount(s) {
  return decodeMaskedAmount(s) > 0;
}

export function isZeroAmount(s) {
  return s === '' || s.match(/^0(,|.)?(0*)$/);
}

export function validateFormatAmount(amount, maxLength) {
  const amountStr = '' + amount;
  if (amountStr.length === 0) {
    return '';
  }
  if (amountStr.length > maxLength) {
    return translate(
      'WorngNumberFormat',
      'Сумма должна быть не более 20 символов, и не более дувух символов после заяпятой',
    );
  }
  const testNumber = parseFloat(amountStr);
  if (isNaN(testNumber)) {
    return translate('WorngNumberFormat', 'Сумма имеет не правильный формат');
  }
  if (amountStr.charAt(0) === '0') {
    return translate('WorngNumberFormat', 'Ведущие ноли не допускаются');
  }
  return '';
}

export function validateTaxCode(value) {
  const weight1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
  const weight2 = [3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2];

  function controlSum(weight) {
    let r = 0;

    for (let i = 0; i < weight.length; i++) {
      r += weight[i] * parseInt(value[i], 10);
    }

    return r % 11;
  }

  let cs = controlSum(weight1);

  if (cs === 10) {
    cs = controlSum(weight2);
  }

  return '' + cs === value[11];
}

export function validateAccount(value) {
  return !(iso7064Mod97_10(iso13616Prepare(value)) !== 1);
}

function iso13616Prepare(iban) {
  iban = iban.toUpperCase();
  iban = iban.substr(4) + iban.substr(0, 4);
  return iban
    .split('')
    .map(function(n) {
      return '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'.indexOf(n);
    })
    .join('');
}

function iso7064Mod97_10(iban) {
  let remainder = iban;
  let block;

  while (remainder.length > 2) {
    block = remainder.slice(0, 9);
    remainder = (parseInt(block, 10) % 97) + remainder.slice(block.length);
  }
  return parseInt(remainder, 10) % 97;
}

/** @deprecated use controls.tsx instead */
export { validateFields } from './controls';

/*
 * Returns 1 if the IBAN is valid
 * Returns FALSE if the IBAN's length is not as should be (for CY the IBAN Should be 28 chars long starting with CY )
 * Returns any other number (checksum) when the IBAN is invalid (check digits do not match)
 */
export function isValidIBANNumber(input) {
  const CODE_LENGTHS = {
    AD: 24,
    AE: 23,
    AT: 20,
    AZ: 28,
    BA: 20,
    BE: 16,
    BG: 22,
    BH: 22,
    BR: 29,
    CH: 21,
    CR: 21,
    CY: 28,
    CZ: 24,
    DE: 22,
    DK: 18,
    DO: 28,
    EE: 20,
    ES: 24,
    FI: 18,
    FO: 18,
    FR: 27,
    GB: 22,
    GI: 23,
    GL: 18,
    GR: 27,
    GT: 28,
    HR: 21,
    HU: 28,
    IE: 22,
    IL: 23,
    IS: 26,
    IT: 27,
    JO: 30,
    KW: 30,
    KZ: 20,
    LB: 28,
    LI: 21,
    LT: 20,
    LU: 20,
    LV: 21,
    MC: 27,
    MD: 24,
    ME: 22,
    MK: 19,
    MR: 27,
    MT: 31,
    MU: 30,
    NL: 18,
    NO: 15,
    PK: 24,
    PL: 28,
    PS: 29,
    PT: 25,
    QA: 29,
    RO: 24,
    RS: 22,
    SA: 24,
    SE: 24,
    SI: 19,
    SK: 24,
    SM: 27,
    TN: 24,
    TR: 26,
  };
  const iban = String(input)
    .toUpperCase()
    .replace(/[^A-Z0-9]/g, ''); // keep only alphanumeric characters
  const code = iban.match(/^([A-Z]{2})(\d{2})([A-Z\d]+)$/); // match and capture (1) the country code,
  // (2) the check digits, and (3) the rest
  let digits;
  // check syntax and length
  if (!code || iban.length !== CODE_LENGTHS[code[1]]) {
    return false;
  }
  // rearrange country code and check digits, and convert chars to ints
  digits = (code[3] + code[1] + code[2]).replace(/[A-Z]/g, function(letter) {
    return String.fromCharCode(letter.charCodeAt(0) - 55);
  });
  // final check
  return mod97(digits);
}
function mod97(str: string) {
  const checksum = str.slice(0, 2);
  let fragment;
  let retCheckSum: number;
  for (let offset = 2; offset < str.length; offset += 7) {
    fragment = String(checksum) + str.substring(offset, offset + 7);
    retCheckSum = parseInt(fragment, 10) % 97;
  }
  return retCheckSum;
}

function convertToConstraint(item: FieldConstrain): Constraint {
  const constraint: Constraint = {};
  constraint[item.type] = item.value;
  constraint.errorText = item.error;
  return constraint;
}

export interface FieldConstrain {
  type?: string;
  value?: string;
  isRequired?: string;
  error?: string;
}

function validateCard(PAN) {
  if (!cardValues.includes(PAN.substring(0, BIN_PART_LENGTH_SECOND))) {
    if (!cardValues.includes(PAN.substring(0, BIN_PART_LENGTH_FIRST))) {
      return false;
    }
  }
  return true;
}

const BIN_PART_LENGTH_FIRST = 6;
const BIN_PART_LENGTH_SECOND = 7;
const cardValues =
  '489331	Visa Gold	Debit' +
  '489332	Visa Classic	Credit' +
  '489333	Visa Classic	Debit' +
  '489334	Visa Business	Debit' +
  '489335	Visa Electron	Debit' +
  '483778	Visa Classic	Debit' +
  '462522	Visa Gold	Credit' +
  '462523	Visa Electron	Credit' +
  '474807	Visa Platinum	Debit' +
  '474808	Visa Infinite	Debit' +
  '474809	Visa Business (Gold)	Debit' +
  '489351	Acquirer BIN	VD/ATM Acq' +
  '489333	Clearing BIN	VbV Acq' +
  '512356	MC Standard' +
  '511574	MC Gold' +
  '521050	MC Platinum' +
  '6234170	UPI Classic	Debit' +
  '6234171	UPI Classic	Debit' +
  '6234172	UPI Classic	Debit' +
  '6234180	UPI Gold	Debit' +
  '6234181	UPI Gold	Debit' +
  '6234182	UPI Gold	Debit' +
  '6234190	UPI Platinum	Debit' +
  '6234191	UPI Platinum	Debit' +
  '6234192	UPI Platinum	Debit' +
  '6234200	UPI Diamond	Debit' +
  '6234201	UPI Diamond	Debit' +
  '6234202	UPI Diamond	Debit' +
  '6234210	UPI Corporate Platinum	Debit' +
  '6234211	UPI Corporate Platinum	Debit' +
  '6234212	UPI Corporate Platinum	Debit' +
  '6291800	UPI Classic Travelmate	Debit' +
  '6291801	UPI Classic Travelmate	Debit' +
  '6291802	UPI Classic Travelmate	Debit';
