import * as React from 'react';
import * as mui from 'material-ui';
import { get, SS } from '../backend';
import { siteConfig } from '../siteConfig';
import * as validator from '../validator';

export function getLocale(defValue) {
  let result = defValue;
  if (sessionStorage['dictionary_saved']) {
    result = sessionStorage[SS.LOCALE] || localStorage.getItem('TRANSLATE_LANGUAGE') || 'ru';
    document.cookie = 'TRANSLATE_LANGUAGE=' + result + ';path=/';
    localStorage.setItem('TRANSLATE_LANGUAGE', result);
  } else if (siteConfig && siteConfig.settings && siteConfig.settings.defaultLanguage) {
    result = siteConfig.settings.defaultLanguage;
    document.cookie = 'TRANSLATE_LANGUAGE=' + result + ';path=/';
    localStorage.setItem('TRANSLATE_LANGUAGE', result);
  } else {
    const cookies = document.cookie;
    cookies &&
      cookies.split(';').forEach(function(cookie) {
        const parts = cookie.split('=');
        if (parts[0] === 'TRANSLATE_LANGUAGE') {
          result = parts[1];
        }
      });
  }
  return result;
}

export function getDictionary(callback) {
  sessionStorage.removeItem('dictionary_saved');
  return get('/translate', { outputChannel: 'W' }).then(res => {
    if (Object.keys(res).length > 2) {
      Object.keys(res).map(function(key) {
        sessionStorage[key] = res[key];
      });
      sessionStorage['dictionary_saved'] = '1';
      callback();
    }
  });
}

export const defaultLocales = { kk: 'ҚАЗ', ru: 'РУС', en: 'ENG' };

export function getLocales(callback) {
  return get('/translate/locales').then((res: any[]) => {
    callback(res);
  });
}

export function translate(key: string, defvalue: string = '', values: any[] = null, matcher = /{([0-9]+)}/gi): string {
  const value = sessionStorage[key] || defvalue || key;
  if (values && values.length > 0) {
    return value.replace(matcher, (_, group) => {
      return values[group] || '';
    });
  }
  return value;
}

export function t(literals: TemplateStringsArray, ...placeholders): string {
  let result = literals[0];

  for (let i = 0; i < placeholders.length; i++) {
    result += placeholders[i] + literals[i + 1];
  }

  return translate(result);
}

export function initDictionary(callback) {
  if (!sessionStorage['dictionary_saved']) {
    getDictionary(callback);
  } else {
    callback();
  }
}

export function translateBoolean(val: boolean) {
  if (val) {
    return translate('boolean_true', 'Да');
  } else {
    return translate('boolean_false', 'Нет');
  }
}
export interface Translation {
  id?: number;
  value?: string;
  translations?: any;
}

export interface Translation2 {
  ru?: string;
  en?: string;
  kk?: string;
  current?: string;
}

interface MultyInputType {
  locales?: any;
  values?: Translation2;
}

interface FieldErrors {
  en?: string;
  ru?: string;
  kk?: string;
}

interface TranslationEditState {
  lines?: Translation2;
  locales?: any;
  fieldErrors?: FieldErrors;
}
interface TranslationEditProps {
  labelText?: string;
  values: Translation2;
  errorText?: string;
  locales?: any;
  style?: React.CSSProperties;
  zDepth?: number;
  constrains?: any;
  firstColumnWidth?: string;
  secondColumnWidth?: string;
  isRead?: boolean;
  onChange(translation: Translation2);
  onValidate?(isValidate: boolean);
}
export class MultyInput extends React.Component<TranslationEditProps, TranslationEditState> {
  static defaultProps = {
    locales: {},
    values: {},
    firstColumnWidth: '25%',
    secondColumnWidth: '70%',
  };

  constructor(props: TranslationEditProps) {
    super(props);

    this.state = {
      lines: props.values == null ? {} : props.values,
      locales: props.locales,
      fieldErrors: { en: '', ru: '', kk: '' },
    };
  }

  componentDidMount() {
    this.update();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ lines: nextProps.values == null ? {} : nextProps.values });
    if (Object.keys(nextProps.locales).length > 0) {
      this.setState({ locales: nextProps.locales });
    }
    this.update();
    if (nextProps.constrains) {
      const isValid = this.validateTranslations(nextProps.constrains);
    }
  }

  update() {
    if (Object.keys(this.state.locales).length === 0) {
      getLocales(res => {
        this.setState({ locales: res });
      });
    }
  }

  validateTranslations(constraints: any): boolean {
    let errMes = '';
    let isValidate = true;

    const { fieldErrors, lines } = this.state;

    ['en', 'ru', 'kk'].forEach(n => {
      errMes = validator.message(constraints, lines[n]);
      fieldErrors[n] = errMes;
      if (errMes) {
        isValidate = false;
      }
    });

    this.setState({ fieldErrors });

    return !isValidate;
  }

  valueChanged(value, locale) {
    this.state.lines[locale] = (value.target as HTMLInputElement).value;
    this.setState({ lines: this.state.lines });
    let isValid: boolean = true;
    if (this.props.constrains) {
      isValid = this.validateTranslations(this.props.constrains);
    }
    this.props.onChange(this.state.lines);
    if (this.props.onValidate) {
      this.props.onValidate(isValid);
    }
  }

  render() {
    let zdepth = 1;
    if (this.props.zDepth >= 0) {
      zdepth = this.props.zDepth;
    }
    const renderAray = [];

    const propsObj = this.props;

    const isRead = propsObj.hasOwnProperty('isRead') ? propsObj.isRead : false;

    Object.keys(this.state.locales).map(item => {
      renderAray.push(
        <tr key={item}>
          <td style={{ width: this.props.firstColumnWidth, marginRight: '5%' }}>{this.state.locales[item]}</td>
          <td style={{ width: this.props.secondColumnWidth }}>
            <mui.TextField
              value={this.state.lines && this.state.lines[item] !== undefined ? (this.state.lines[item] as string) : ''}
              errorText={this.props.errorText}
              style={this.props.style}
              onChange={e => this.valueChanged(e, item)}
              disabled={isRead}
            />
          </td>
        </tr>,
      );
      renderAray.push(<tr style={{ color: 'red' }}>{this.state.fieldErrors[item]}</tr>);
    });
    return (
      <mui.Paper zDepth={zdepth}>
        <div>{this.props.labelText}</div>
        <table style={{ width: '100%' }}>
          <tbody>{renderAray}</tbody>
        </table>
      </mui.Paper>
    );
  }
}
