import React from 'react';
import { grey200, green500 } from 'material-ui/styles/colors';

import { Treebeard, decorators } from 'react-treebeard';

const style = {
  tree: {
    base: {
      listStyle: 'none',
      margin: 0,
      padding: 0,
      color: '#333333',
      fontSize: '14px',
    },
    node: {
      base: {
        position: 'relative',
      },
      link: {
        cursor: 'pointer',
        position: 'relative',
        padding: '0px 5px',
        display: 'block',
      },
      activeLink: {
        background: grey200,
      },
      toggle: {
        base: {
          position: 'relative',
          display: 'inline-block',
          verticalAlign: 'top',
          marginLeft: '-5px',
          height: '24px',
          width: '24px',
        },
        wrapper: {
          position: 'absolute',
          top: '50%',
          left: '50%',
          margin: '-7px 0 0 -7px',
          height: '14px',
        },
        height: 14,
        width: 14,
        arrow: {
          strokeWidth: 0,
        },
      },
      header: {
        base: {
          display: 'inline-block',
          verticalAlign: 'top',
        },
        connector: {
          width: '2px',
          height: '12px',
          borderLeft: 'solid 2px black',
          borderBottom: 'solid 2px black',
          position: 'absolute',
          top: '0px',
          left: '-21px',
        },
        title: {
          lineHeight: '24px',
          verticalAlign: 'middle',
        },
      },
      subtree: {
        listStyle: 'none',
        paddingLeft: '19px',
      },
      loading: {
        color: green500,
      },
    },
  },
};

export interface TreeViewProps {
  data: AppPage;
  onToggle: any;
}

interface TreeViewState {
  data?: AppPage;
  cursor?: any;
}

export interface AppPage {
  id?: string;
  name: string;
  actions?: any;
  isLink?: boolean;
  link?: string;
  children?: AppPage[];
  loading?: boolean;
  toggled?: boolean;
  active?: boolean;
  decorators?: any;
  animations?: any;
}

export class TreeView extends React.Component<TreeViewProps, TreeViewState> {
  constructor(props: TreeViewProps) {
    super(props);
    this.state = {
      data: props.data,
    };
    this.onToggle = this.onToggle.bind(this);
  }

  componentWillReceiveProps(nextProps: TreeViewProps) {
    this.setState({
      data: nextProps.data,
    });
  }

  onToggle(node, toggled) {
    if (this.state.cursor) {
      this.state.cursor.active = false;
    }
    node.active = true;
    if (node.children) {
      node.toggled = toggled;
    }
    this.setState({ cursor: node });
    this.props.onToggle(node);
  }

  render() {
    return <Treebeard data={this.state.data} style={style} onToggle={this.onToggle} decorators={decorators} />;
  }
}
