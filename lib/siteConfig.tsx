import * as backend from './backend';
import { Constraint, GlobalConstraint } from './validator';
import { PersonType, MenuItem } from './models/models';
import { SS } from './backend';
import { CustomizableForm } from './util/CustomizableForm';
import { TabbedView } from './util/tabbedView';

export const Default = 'Default';

export const Required = {
  TRUE: true,
  FALSE: false,
  WARN: 'warn',
};

// wrapped value uses to split ui for privet person, legal entity or default views (for banks with only one type of person).
interface ValueWrapper<T> {
  def?: T; // default value. Use if web interface does not split for person type
  pp?: T; // for PrivetPerson
  le?: T; // for LegalEntity
}

export interface SiteConfig {
  bank: 'Default';

  // split customer ui by customer person type (or LegalEntity or PrivetPerson) or use one default.
  // If true variables of site config with type ValueWrapper can have use pp or le field (depend of person type).
  // If false use - def
  splitWebForPersonType: boolean;

  branding: {
    copyright: string;
    logo: string;
    managerLogo: string;
    managerPhoto: string;
    slides: {
      slide1: string;
      slide2: string;
      slide3: string;
      slide4: string;
      slide5: string;
    };
    menuLogo: string;
    loginLogo: string;
    newsLogo: string;
    phone: string;
    customTheme: boolean;
  };

  login: {
    firstPage: string;
    constraints: {
      code: Constraint;
      password: Constraint;
      login: Constraint;
      phone: Constraint;
    };
  };

  mainMenu: ValueWrapper<MenuItem[]>; // main menu settings. Consist data of menu item, links and translations key

  manager_index_page_main_block_config?: {
    // main menu page configuration
    hidden_element_keys?: string[]; // list of hidden elements on main menu page
  };

  personal: {
    hideOrganization: boolean;
    hideCertificates: boolean;
    showLanguageSelector: boolean;
    allowToSkipSMS: boolean;
  };

  response: {
    code420: {
      redirect: string;
    };
  };

  documents: {
    director: boolean;
    showImport: boolean;
    states: {
      AwaitsSignature: string;
    };
    workflow: {
      hideGroupButtons: string[];
      sign: {
        translate: string;
      };
      submit: {
        translate: string;
      };
      revoke: {
        createWithdraw: boolean;
        ifIsNotToday: boolean;
      };
    };
    valueDaysType: string;
    bankPageURL: string;
    balance: boolean;
    amountForConfirmation: number;

    domesticTransfer: {
      types: ValueWrapper<string[]>;

      showStates: boolean;
      P2P: {
        settings: ValueWrapper<PaymentPageSettings>;
        constraints: {};
      };
      paymentOrder: {
        settings: ValueWrapper<PaymentPageSettings>;
        hideFields: ValueWrapper<string[]>;
        globalConstraints: GlobalConstraint;
        constraints: {
          account: Constraint;
          valueDate: Constraint;
          purpose: Constraint;
          purposeCode: Constraint;
          benefTaxCode: Constraint;
          benefName: Constraint;
          benefBankCode: Constraint;
          benefResidencyCode: Constraint;
          benefAccount: Constraint;
          budgetCode: Constraint;
          vinCode: Constraint;
          number: Constraint;
          info: Constraint;
          amount: Constraint;
        };
      };
      payroll: {
        globalConstraints: GlobalConstraint;
        constraints: {
          account: Constraint;
          director: Constraint;
          valueDate: Constraint;
          purpose: Constraint;
          purposeCode: Constraint;
          benefTaxCode: Constraint;
          benefName: Constraint;
          benefBankCode: Constraint;
          benefResidencyCode: Constraint;
          benefAccount: Constraint;
          number: Constraint;
          info: Constraint;
        };
        employeeConstraints: {
          amount: Constraint;
          firstName: Constraint;
          lastName: Constraint;
          middleName: Constraint;
          taxCode: Constraint;
          birthDate: Constraint;
          account: Constraint;
        };
      };
      pensionContribution: {
        globalConstraints: GlobalConstraint;
        constraints: {
          account: Constraint;
          director: Constraint;
          valueDate: Constraint;
          purpose: Constraint;
          purposeCode: Constraint;
          benefTaxCode: Constraint;
          benefName: Constraint;
          benefBankCode: Constraint;
          benefResidencyCode: Constraint;
          benefAccount: Constraint;
          number: Constraint;
          info: Constraint;
        };
        employeeConstraints: {
          amount: Constraint;
          firstName: Constraint;
          lastName: Constraint;
          middleName: Constraint;
          taxCode: Constraint;
          birthDate: Constraint;
          account: Constraint;
        };
      };
      socialContribution: {
        globalConstraints: GlobalConstraint;
        constraints: {
          account: Constraint;
          director: Constraint;
          valueDate: Constraint;
          purpose: Constraint;
          purposeCode: Constraint;
          benefTaxCode: Constraint;
          benefName: Constraint;
          benefBankCode: Constraint;
          benefResidencyCode: Constraint;
          benefAccount: Constraint;
          number: Constraint;
          info: Constraint;
        };
        employeeConstraints: {
          amount: Constraint;
          firstName: Constraint;
          lastName: Constraint;
          middleName: Constraint;
          taxCode: Constraint;
          birthDate: Constraint;
          period: Constraint;
        };
      };
    };

    accountTransfer: {
      settings: ValueWrapper<PaymentPageSettings>;
      types: string[];
      hideFields: ValueWrapper<string[]>;
      globalConstraints: GlobalConstraint;
      constraints: {
        account: Constraint;
        creditSum: Constraint;
        debitSum: Constraint;
        debitCurrency: Constraint;
        creditAccount: Constraint;
        creditCurrency: Constraint;
        valueDate: Constraint;
        purpose: Constraint;
        purposeCode: Constraint;
        number: Constraint;
      };
      transfer: {
        settings: ValueWrapper<PaymentPageSettings>;
        expoTransfer: boolean;
        hideFields: ValueWrapper<string[]>;
        globalConstraints: GlobalConstraint;
        constraints: {
          account: Constraint;
          creditSum: Constraint;
          debitSum: Constraint;
          debitCurrency: Constraint;
          creditAccount: Constraint;
          creditCurrency: Constraint;
          valueDate: Constraint;
          purpose: Constraint;
          purposeCode: Constraint;
          number: Constraint;
        };
      };
      conversion: {
        settings: ValueWrapper<PaymentPageSettings>;
        expoConversion: boolean;
        hideFields: ValueWrapper<string[]>;
        globalConstraints: GlobalConstraint;
        constraints: {
          account: Constraint;
          creditSum: Constraint;
          debitSum: Constraint;
          debitCurrency: Constraint;
          creditAccount: Constraint;
          creditCurrency: Constraint;
          valueDate: Constraint;
          purpose: Constraint;
          purposeCode: Constraint;
          number: Constraint;
        };
      };
    };

    intTransfer: {
      settings: ValueWrapper<ITransferFieldsConfig>;
      feeTypeCode: string;
      showBenefKpp: boolean;
      constraintsType: string;
      globalConstraints: GlobalConstraint;
      constraints: {
        director: Constraint;
        amount: Constraint;
        valueDate: Constraint;
        purpose: Constraint;
        purposeCode: Constraint;
        accountCurrency: Constraint;
        account: Constraint;
        benefName: Constraint;
        benefResidencyCode: Constraint;
        benefAccount: Constraint;
        benefBankCode: Constraint;
        benefCountryCode: Constraint;
        benefCity: Constraint;
        benefAddress: Constraint;
        benefBankCorrAccount: Constraint;
        benefBankCity: Constraint;
        benefBankAddress: Constraint;
        benefKpp: Constraint;
        agentBankCode: Constraint;
        agentCorrAccount: Constraint;
        agentBankCity: Constraint;
        agentBankAddress: Constraint;
        number: Constraint;
        contractNumber: Constraint;
        contractAuditNumber: Constraint;
        invoice: Constraint;
        feeTypeCode: Constraint;
        feeAccount: Constraint;
        info: Constraint;
        additionalInfo: Constraint;
      };
    };

    exposedOrder: {
      globalConstraints: GlobalConstraint;
      constraints: {
        globalConstraints: GlobalConstraint;
        number: Constraint;
        expireDate: Constraint;
        account: Constraint;
        benefName: Constraint;
        benefTaxCode: Constraint;
        amount: Constraint;
        purposeCode: Constraint;
        purpose: Constraint;
      };
    };
  };

  visible: {
    handbook_employee: boolean;
    mt: boolean;
    keyboard: boolean;
    editPhone: boolean;
    accOpenClose: boolean;
    fio: boolean;
    departmentsList: boolean;
    certificatesUpadate: boolean;
    deleteCustomer: boolean;
  };

  handbooks: {
    intCounterparty: {
      hideContracts: boolean;
    };
  };

  settings: {
    customerRegistrationSingleForm: boolean;
    warningTime: number;
    defaultLanguage: string;
    design: {
      accounts: {
        flag: boolean;
        all: boolean;
      };
      mainBar: {
        fio: string;
      };
    };
  };

  accounts: {
    name: string;
    filia: boolean;
    transSearch: string;
  };

  mainPage: {
    hideGraph: boolean;
    hideTask: boolean;
    hideStatement: boolean;
    hideLicense: boolean;
  };

  demands: {
    numberCategoryInGrid: number;
    startOperDay: number;
    endOperDay: number;
    visibility: {
      createNewButton: boolean;
    };
  };

  links: {
    info: string;
    requisites: string;
    Accountability: string;
    licence: string;
  };

  env: {
    port: number;
  };

  card: {
    transaction: {
      grid_fields: string[];
    };
  };

  mail: {
    showCompanyPersonBlock: boolean;
  };
  // manager specific params
  manager?: {
    // list of disabled endpoints. Endpoints specified in this variable won't be available in front end
    disabledEndpoints?: string[];
    // forms configuration
    formConfig?: FormConfig;
    tabsConfig?: TabsConfig;
  };

  letter: {
    grid_inbox_fields: string[];
    grid_outbox_fields: string[];
  };

  organization_with_import: string[];

  private: {
    customToolbar: boolean;
    customHome: boolean;
    changeFoto: boolean;
    achievements: boolean;
  };
}

export interface FormConfig {
  [formName: string]: FieldConfig;
}

export interface FieldConfig {
  hidden?: boolean;
  disabled?: boolean;
  defaultValue?: string;
}

export interface TabsConfig {
  [tabFormName: string]: { disabledTabs: string[] };
}

// retrieves form configuration from site config using form name
export function getFormConfig(formObject: CustomizableForm<any, any, any>): FormConfig {
  const formType = formObject.constructor.name;

  if (siteConfig.manager && siteConfig.manager.formConfig) {
    return siteConfig.manager.formConfig[formType] as FormConfig;
  }

  return undefined;
}

export function getTabsConfig(tabbedComponent: TabbedView<any, any>): { disabledTabs: string[] } {
  const tabbedComponentsName = tabbedComponent.constructor.name;

  if (siteConfig.manager && siteConfig.manager.tabsConfig) {
    return siteConfig.manager.tabsConfig[tabbedComponentsName] as { disabledTabs: string[] };
  }

  return undefined;
}

export interface PaymentPageSettings {
  gridTitle?: string;
  titleCreate?: string;
  titleEdit?: string;
  titleTemplateCreate?: string;
  titleTemplateEdit?: string;
}

export interface ITransferFieldsConfig extends PaymentPageSettings {
  payer?: string[];
  creditor?: string[];
  bank?: string[];
  agent?: string[];
  detail?: string[];
  fee?: string[];
  purpose?: string[];
}

export let siteConfig: SiteConfig;

const defaultSiteConfig = require('../../../config/config.json'); // TODO: Get config from backend

export async function load() {
  // const response = await backend.get('//site/config.json');
  // backend.get('/constants/name/TECHNICAL_WORKS_MODE').then(res => {
  //   sessionStorage.setItem('serviceUnavailable', res.value);
  // });
  // siteConfig = typeof response === 'object' ? response : JSON.parse(response);
  // siteConfig.private = siteConfig.private || ({} as any);

  siteConfig = defaultSiteConfig;
}

/**
 * Method uses to split ui for privet person, legal entity or default views (for banks with only one type of person).
 * If property of site config 'splitWebForPersonType' == false or absent, method return 'def' property of 'wrappedValue'.
 * If 'personType' == LegalEntity method return 'le', else 'pp';
 *
 * @param value ValueWrapper<T>
 * @param personType or LegalEntity or PrivetPerson
 * @returns {any}
 */
export function getValueByPersonType<T>(
  value: ValueWrapper<T>,
  personType = sessionStorage.getItem(SS.PERSON_TYPE),
): T {
  if (!value) {
    return null;
  }

  if (!siteConfig.splitWebForPersonType || !personType) {
    return value.def;
  }

  return personType === PersonType.LegalEntity ? value.le : value.pp;
}

/**
 * Check is current bank equal to argument of function
 *
 * @param bank bank name
 * @returns {boolean}
 */
export function isCurrentBank(bank: 'Default'): boolean {
  return siteConfig.bank === bank;
}
