import React, { Component } from 'react';
import * as mui from 'material-ui';
import NavigationArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import NavigationArrowForward from 'material-ui/svg-icons/navigation/arrow-forward';
import { Header, classes } from '../layout/views';
import icons from '../layout/icons';
import { translate } from '../i18n/translator';
import { visibility } from '../layout/visibility';
import './grid.scss';

const debounce = require('throttle-debounce/debounce');

/** Backend should respond in this format */
export interface GridResponse<R> {
  rows: R[];
  total?: {
    count?: number;
  };
}

interface HasId {
  id: number;
}

export interface Column<R extends HasId> {
  label: string | JSX.Element;
  align?: string;
  width?: number;
  sort?: string;
  style?: React.CSSProperties;
  render(row: R, index?: number): React.ReactNode;
}

export type GridSelection = 'all' | 'none' | number[];

export type SortOrder = 'asc' | 'desc';

interface GridProps<R extends HasId> {
  selectable?: boolean;
  columns: Column<R>[];
  defaultSort?: string;
  defaultOrder?: SortOrder;
  selection?: GridSelection;
  filter?: object;
  blockedRowsForSelecting?: number[];
  defaultPageSize?: number;
  needPagination?: boolean;
  fetchRows(window: GridWindow): Promise<GridResponse<R>>; // window will also includes filter properties
  onRowSelection?(selection: GridSelection): void;
  rowClass?(row: string): string;
}

export interface GridWindow {
  // immutable
  page?: number;
  size?: number;
  sort?: string;
  order?: SortOrder;
}

interface GridState<R> {
  loading?: number;
  rows?: R[];
  total?: number;

  window?: GridWindow;
}

export class Grid<R extends HasId> extends Component<GridProps<R>, GridState<R>> {
  static defaultProps = {
    onRowSelection: () => {
      return;
    },
    defaultOrder: 'asc',
    selection: 'none',
    filter: {},
    selectable: true,
    rowClass: null,
    blockedRowsForSelecting: [],
    needPagination: true,
  };

  static gridConfigKey = 'stored_grid_config';
  static gridLocationKey = 'stored_grid_location';

  // location of currently rendered grid
  gridLocation = window.location.pathname;

  constructor(props: GridProps<R>) {
    super(props);

    // trying to load saved grid page config
    const savedPageConfig = this.loadGridConfig();

    this.state = {
      loading: 0,
      rows: [],
      total: 0,

      window: {
        page: savedPageConfig ? savedPageConfig.page : 0,
        size: savedPageConfig ? savedPageConfig.size : props.defaultPageSize || 10,
        sort: props.defaultSort || savedPageConfig.sort,
        order: savedPageConfig ? savedPageConfig.order : props.defaultOrder,
      },
    };

    // if sessionStorage has no pageConfig object for current grid, saving it
    !savedPageConfig && this.saveGridConfig(this.state.window);
  }

  // retrieve current page config from sessionStorage
  loadGridConfig(): GridWindow {
    const gridLocation = sessionStorage.getItem(Grid.gridLocationKey);

    if (gridLocation && gridLocation === this.gridLocation) {
      const gridConfig = sessionStorage.getItem(Grid.gridConfigKey);
      return gridConfig && JSON.parse(gridConfig);
    }
  }

  // save provided page configuration to browser sessionStorage for future render
  saveGridConfig(newGridConfig: GridWindow) {
    sessionStorage.setItem(Grid.gridLocationKey, this.gridLocation);

    sessionStorage.setItem(Grid.gridConfigKey, JSON.stringify(newGridConfig));
  }

  render() {
    const columnStyles = [];

    this.props.columns.forEach((col, idx) => {
      columnStyles[idx] = col.style || {};

      if (col.width) {
        columnStyles[idx].width = col.width;
      }
      if (col.align) {
        columnStyles[idx].textAlign = col.align;
      }
    });

    return (
      <div className={classes({ loading: this.state.loading > 0 }, 'grid')}>
        <mui.LinearProgress style={{ position: 'absolute' }} />
        <mui.Table multiSelectable={true} onRowSelection={this.onRowSelection} fixedHeader={false}>
          <mui.TableHeader displaySelectAll={this.props.selectable} adjustForCheckbox={this.props.selectable}>
            <mui.TableRow>
              {this.props.columns.map((col, idx) => (
                <GridHeaderColumn
                  key={idx.toString()}
                  col={col}
                  style={columnStyles[idx]}
                  sort={this.state.window.sort}
                  order={this.state.window.order}
                  setSort={(sort, order) =>
                    this.updateWindow({
                      sort,
                      order,
                      page: 0,
                    })
                  }
                />
              ))}
            </mui.TableRow>
          </mui.TableHeader>
          <mui.TableBody
            showRowHover={true}
            deselectOnClickaway={false}
            className={this.state.loading ? 'disabled' : ''}
            displayRowCheckbox={this.props.selectable}>
            {this.state.rows &&
              this.state.rows.map((row, i) => (
                <mui.TableRow
                  key={i}
                  selected={this.isRowSelected(row.id)}
                  className={this.isRowSelected(row.id) ? 'selected' : 'none'}
                  selectable={this.handleSelectableForRow(row)}>
                  {this.props.columns.map((col, colIdx) => (
                    <mui.TableRowColumn className="data" key={colIdx.toString()} style={columnStyles[colIdx]}>
                      <div className="wrap">{col.render(row, i)}</div>
                    </mui.TableRowColumn>
                  ))}
                </mui.TableRow>
              ))}
          </mui.TableBody>
          {this.props.needPagination && (
            <mui.TableFooter>
              <mui.TableRow>
                <mui.TableRowColumn style={{ paddingRight: 4 }}>
                  <GridPagination
                    total={this.state.total}
                    page={this.state.window.page}
                    pageSize={this.state.window.size}
                    onChangePageSize={size => this.updateWindow({ size, page: 0 })}
                    onChangePage={page => this.updateWindow({ page })}
                  />
                </mui.TableRowColumn>
              </mui.TableRow>
            </mui.TableFooter>
          )}
        </mui.Table>
      </div>
    );
  }

  handleSelectableForRow = (row: R) => {
    return this.props.selectable && !this.props.blockedRowsForSelecting.includes(row.id);
  };

  updateWindow = (newWindow: GridWindow) => {
    this.setState({
      window: { ...this.state.window, ...newWindow },
    });
  };

  onRowSelection = (selection: GridSelection) => {
    // table populate selection with row numbers inside the current page,
    // need to convert to row ids
    const pageSelectionToGlobal = (): GridSelection => {
      if (!(selection instanceof Array)) {
        return selection;
      }

      // convert row numbers to ids
      selection = selection.map(idx => this.state.rows[idx].id);

      // unselect only those ids that are on the current page
      if (this.props.selection instanceof Array) {
        const selectedRowsFromOtherPages = this.props.selection.slice(0);

        this.state.rows.forEach(row => {
          const idx = selectedRowsFromOtherPages.indexOf(row.id);
          if (idx >= 0) {
            selectedRowsFromOtherPages.splice(idx, 1);
          }
        });

        selection = selection.concat(...selectedRowsFromOtherPages);
        selection.sort(); // not strictly required
      }

      return selection;
    };

    selection = pageSelectionToGlobal();

    if (selection instanceof Array && selection.length === 0) {
      selection = 'none';
    }

    this.props.onRowSelection(selection);
  };

  isRowSelected(rowId: number): boolean {
    if (this.props.selection === 'none') {
      return false;
    }
    if (this.props.selection === 'all') {
      return true;
    }

    const b = (this.props.selection as number[]).indexOf(rowId) >= 0;

    return b;
  }

  componentDidMount() {
    this.load(this.state.window);
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.filter !== this.props.filter) {
      this.updateWindow({ page: 0 });
    }

    // trigger re-load
    if (nextState.window !== this.state.window) {
      this.saveGridConfig(nextState.window);
      this.load(nextState.window);
    }
  }

  load(window) {
    this.setState({ loading: this.state.loading + 1 });

    this.props.fetchRows({ ...this.props.filter, ...window }).then((r: GridResponse<R>) => {
      const nextState = { loading: this.state.loading - 1 };

      // ignore stale updates
      if (this.state.window === window) {
        Object.assign(nextState, {
          rows: r.rows,
          total: r.total.count,
        });
      }

      this.setState(nextState);
    });
  }
}

const GridHeaderColumn = ({ col, style, sort, order, setSort }, context) => {
  const iconStyle = {
    [col.align === 'left' ? 'marginRight' : 'marginLeft']: 6,
  };

  const sorterColor = context.muiTheme.tableHeaderColumn.textColor;

  const icon = !col.sort ? null : (
    <span key="icon" className={'order ' + (col.sort === sort ? order : 'asc')} style={iconStyle}>
      <mui.SvgIcon viewBox="0 0 24 24" style={{ width: 16, height: 16 }} color={sorterColor}>
        <path d="M4 12l1.41 1.41L11 7.83V20h2V7.83l5.58 5.59L20 12l-8-8-8 8z" />
      </mui.SvgIcon>
    </span>
  );

  const classes = ['data '];
  if (col.sort) {
    classes.push('sortable');
  }
  if (col.sort && col.sort === sort) {
    classes.push('active');
  }

  const updateSort = e => {
    if (!col.sort) {
      return;
    }

    if (col.sort) {
      setSort(col.sort, col.sort === sort && order === 'asc' ? 'desc' : 'asc');
    }

    e.preventDefault();
  };

  // sorter required b/c onClick is not trigged on TableHeaderColumn
  return (
    <mui.TableHeaderColumn className={classes.join(' ')} style={style}>
      <div className="sorter" onClick={updateSort}>
        {col.align === 'left' ? [icon, col.label] : [col.label, icon]}
      </div>
    </mui.TableHeaderColumn>
  );
};

GridHeaderColumn['contextTypes'] = {
  muiTheme: React.PropTypes.object.isRequired,
};

const GridPagination = ({ total, page, pageSize, onChangePage, onChangePageSize }) => {
  const lastPage = Math.floor(total / pageSize);

  function getPageItems() {
    const r = [];

    for (let i = 0; i <= lastPage; i++) {
      r.push(<mui.MenuItem key={i} value={i} primaryText={i + 1} />);
    }

    return r;
  }

  function getPageSizeItems() {
    return [10, 20, 50, 100].map(n => <mui.MenuItem key={n} value={n} primaryText={n} />);
  }

  function getWindow() {
    return `${page * pageSize + 1} - ${Math.min((page + 1) * pageSize, total)} of ${total}`;
  }

  return (
    <div className="paging">
      <div>{translate('grid_page')}:</div>

      <mui.SelectField
        value={page}
        style={{ fontSize: 13, width: 50 }}
        underlineStyle={{ borderBottom: 'none' }}
        iconStyle={{ fill: '#000', top: 11 }}
        onChange={(obj, key, value) => onChangePage(value)}>
        {getPageItems()}
      </mui.SelectField>

      <div>{translate('grid_rowsperpage')}:</div>

      <mui.SelectField
        value={pageSize}
        style={{ fontSize: 13, width: 50 }}
        underlineStyle={{ borderBottom: 'none' }}
        iconStyle={{ fill: '#000', top: 11 }}
        onChange={(obj, key, value) => onChangePageSize(value)}>
        {getPageSizeItems()}
      </mui.SelectField>

      <div className="window">{getWindow()}</div>

      <div>
        <mui.IconButton disabled={page === 0} onClick={() => onChangePage(page - 1)}>
          <NavigationArrowBack />
        </mui.IconButton>
      </div>
      <div>
        <mui.IconButton disabled={page === lastPage} onClick={() => onChangePage(page + 1)}>
          <NavigationArrowForward />
        </mui.IconButton>
      </div>
    </div>
  );
};

interface GridHeaderProps {
  headerText: string;
  selection?: GridSelection;
  searchText?: string;
  searchHint?: string;
  hideSearchButtons?: boolean;
  getButtons?(searchButton: React.ReactElement<any>): React.ReactElement<any>[];
  getSelectionButtons?(cancelSelectionButton: React.ReactElement<any>): React.ReactElement<any>[];
  cancelSelection?();
  setSearchText?(searchText: string);
}

export class GridHeader extends React.Component<GridHeaderProps, any> {
  static defaultProps = {
    selection: 'none',
    cancelSelection: () => {},

    getButtons: b => [b],
    getSelectionButtons: b => [b],
    hideSearchButtons: false,
  };

  updateSearchText: any = null;

  constructor(props: GridHeaderProps) {
    super();

    this.updateSearchText = debounce(400, (e, val) => props.setSearchText(val));
  }

  getStyle() {
    return this.props.selection === 'none'
      ? {}
      : {
          backgroundColor: (this.context as any).muiTheme.palette.pickerHeaderColor,
        };
  }

  isSearchActive() {
    return this.props.searchText != null;
  }

  getButtons(): React.ReactFragment {
    const searchButton = (
      <mui.IconButton key="search" title={translate('grid_search')} onTouchTap={e => this.props.setSearchText('')}>
        {icons.search}
      </mui.IconButton>
    );

    const cancelSearchButton = (
      <mui.IconButton key="cancel" title={translate('grid_cancelsearch')} onClick={e => this.props.setSearchText(null)}>
        {icons.clear}
      </mui.IconButton>
    );

    const cancelSelectionButton = (
      <mui.IconButton
        key="cancel"
        title={translate('grid_cancelselection')}
        onClick={e => this.props.cancelSelection()}>
        {icons.clear}
      </mui.IconButton>
    );

    if (this.props.selection === 'none') {
      if (this.props.hideSearchButtons) {
        return;
      }
      if (this.isSearchActive()) {
        return cancelSearchButton;
      }

      return this.props.getButtons(searchButton);
    }

    return this.props.getSelectionButtons(cancelSelectionButton);
  }

  renderSearchHeader() {
    return (
      <div style={{ display: 'flex', alignItems: 'center', flexGrow: 1 }}>
        {icons.search}
        <mui.TextField
          hintText={this.props.searchHint}
          fullWidth={true}
          style={{ flexGrow: 1, marginLeft: 20, height: 36 }}
          hintStyle={{ bottom: 6 }}
          onChange={this.updateSearchText}
        />
      </div>
    );
  }

  getChildren(): React.ReactFragment {
    if (this.props.selection === 'none') {
      if (this.isSearchActive()) {
        return this.renderSearchHeader();
      }

      return <div className="text">{this.props.headerText}</div>;
    }

    return <div className="text">{this.props.headerText}</div>;
  }

  render() {
    return (
      <Header style={this.getStyle()} buttons={this.getButtons()}>
        {this.getChildren()}
      </Header>
    );
  }

  static contextTypes = {
    muiTheme: React.PropTypes.object.isRequired,
  };
}

export function highlight(s: string, term: string): React.ReactFragment {
  if (s && term) {
    s = '' + s;

    const parts = s.split(new RegExp(term, 'i'));

    if (parts.length === 1) {
      return parts[0];
    }

    const r = [];

    let index = 0;

    for (const part of parts) {
      if (r.length > 0) {
        const matched = s.substr(index, term.length);
        r.push(
          <span className="hlt" key={index}>
            {matched}
          </span>,
        );

        index += term.length;
      }

      r.push(part);
      index += part.length;
    }

    return r;
  }

  return s;
}

export const GridFilter = ({
  open,
  className,
  progress,
  children,
}: {
  open: boolean;
  className?: string;
  progress?: React.ReactFragment;
  children?: React.ReactFragment;
}) => (
  <div className={classes({ open }, `grid-filter ${className}`)}>
    {progress}
    <div className="padder">{children}</div>
  </div>
);

export const GridContextMenu = ({ title, children }: { title?: string; children?: React.ReactFragment }) => {
  const iconButton = (
    <mui.IconButton title={title} onClick={e => e.stopPropagation()}>
      {icons.contextActions}
    </mui.IconButton>
  );

  return (
    <mui.IconMenu iconButtonElement={iconButton} animated={false} useLayerForClickAway={true}>
      {children}
    </mui.IconMenu>
  );
};

export interface GridRowMenuItem {
  key?: string;
  text?: string;
  leftIcon?: any;
  actions?: string[]; // if actions doesn`t  defined and == '' or null - doesn`t check visibility
  content?: JSX.Element;
  onTouchTap?(): void;
  predicate?(): boolean; // if predicate doesn`t defined or return true - add menu item
}

/**
 * Element use to show grid row menu. Element allow show menu items with checking of visibility and other predicates
 */
export const GridRowMenu = ({
  title,
  items,
  privileges,
}: {
  title?: string;
  items: GridRowMenuItem[];
  privileges: string[];
}) => {
  // default title
  if (!title) {
    title = translate('common_operations');
  }

  const checkVisibilities = (codes: string[]) => {
    let result = false;
    codes &&
      codes.map(code => {
        if (visibility(code, privileges)) {
          result = true;
        }
      });
    return result;
  };

  const list: React.ReactFragment[] = [];

  items &&
    items.map((item, key) => {
      if (!item.actions || item.actions.length === 0 || checkVisibilities(item.actions)) {
        if (!item.predicate || item.predicate()) {
          list.push(
            !item.content ? (
              <mui.MenuItem
                key={item.key || key}
                primaryText={item.text}
                leftIcon={item.leftIcon}
                onTouchTap={() => item.onTouchTap()}
              />
            ) : (
              item.content
            ),
          );
        }
      }
    });

  return list.length > 0 ? <GridContextMenu title={title}>{list}</GridContextMenu> : null;
};
