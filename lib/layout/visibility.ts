/**
 * Method compare code with stored id session storage visibility codes and return true equal code exist.
 *
 * @param {string} code action code
 * @returns {boolean} result
 */
export function visibility(code: string, privileges: string[]) {
  if (privileges) {
    const index: number = privileges.indexOf(code);
    return index >= 0 || code === '*';
  } else {
    return false;
  }
}

/**
 * Return true if any of actions is permitted for current user
 *
 * @param {Array<string>} actions - array of actions
 * @returns {boolean} result
 */
export function checkVisibilities(actions: string[], privileges: string[]) {
  const result = actions && actions.length ? actions.every(action => visibility(action, privileges)) : false;

  return result;
}

/**
 * Component wrap react component and check visibility, if action permitted show child component and if not - hide
 *
 * @param {any} children - child component
 * @param {Array<string>} actions - array of actions
 * @param predicate if false return null
 * @returns {null}
 * @constructor
 */
export const VisibilityWrapper = ({
  children,
  actions,
  predicate = true,
  privileges,
}: {
  children: any;
  actions: string[];
  predicate: boolean;
  privileges: string[];
}) => {
  if (!predicate) {
    return null;
  }
  return checkVisibilities(actions, privileges) ? children : null;
};

/**
 * List af actions
 */
export const m_actions = {
  m_managers_view: 'm_managers_view',
  m_managers_add: 'm_managers_add',
  m_managers_edit: 'm_managers_edit',
  m_managers_password: 'm_managers_password',
  m_managers_status: 'm_managers_status',

  m_users_view: 'm_users_view',
  m_users_add: 'm_users_add',
  m_users_edit: 'm_users_edit',
  m_users_password: 'm_users_password',
  m_users_status: 'm_users_status',

  m_events_history_view: 'm_events_history_view',
  m_sessions_view: 'm_sessions_view',
  m_sessions_deactivate: 'm_sessions_deactivate',
  m_requests_view: 'm_requests_view',

  m_organisations_view: 'm_organisations_view',
  m_organisations_add: 'm_organisations_add',
  m_organisations_edit: 'm_organisations_edit',
  m_organisations_delete: 'm_organisations_delete',

  m_auth_persons_view: 'm_auth_persons_view',
  m_auth_persons_add: 'm_auth_persons_add',
  m_auth_persons_edit: 'm_auth_persons_edit',

  m_otp_tokens_view: 'm_otp_tokens_view',
  m_otp_tokens_import: 'm_otp_tokens_import',

  m_quicklink_edit: 'm_quicklink_edit',

  m_news_view: 'm_news_view',
  m_news_add: 'm_news_add',
};
