import * as React from 'react';
import * as mui from 'material-ui';
import EventEmitter from 'wolfy87-eventemitter';
import './layout.scss';

export const Page = ({
  children,
  className,
  fullWidth,
  style,
}: {
  children?: any;
  className?: string;
  fullWidth?: boolean;
  style?: any;
}) => (
  <mui.Paper className={className} style={{ width: fullWidth ? '100%' : 'auto', ...style }}>
    {children}
  </mui.Paper>
);

export const Header = ({
  text,
  children,
  buttons,
  style = {},
}: {
  text?: string;
  children?: any;
  style?: object;
  buttons?: React.ReactFragment;
}) => {
  return (
    <div style={style} className="viewHeader">
      {text && <div className="text"> {text}</div>}
      {children}
      {buttons && <div className="buttons">{buttons}</div>}
    </div>
  );
};

export const Content = ({
  padding = 20,
  children,
  className,
}: {
  padding?: number;
  children?: any;
  className?: string;
}) => (
  <div style={{ padding }} className={className}>
    {children}
  </div>
);

/** For use on the HomePage only */
export const HomeWidget = ({
  title,
  progress,
  children,
  style,
}: {
  title: string;
  progress?: React.ReactElement<any>;
  children?: React.ReactElement<any>;
  style?: React.CSSProperties;
}) => (
  <mui.Paper style={{ marginBottom: 20, ...style }}>
    <Header text={title} />
    {progress}
    <Content>{children}</Content>
  </mui.Paper>
);

/** @deprecated use cls instead */
export function classes(optional: object, always: string = null): string {
  return cls(optional, always);
}

export function cls(optional: object, always: string = null): string {
  return Object.keys(optional)
    .filter(key => optional[key])
    .concat(always ? [always] : [])
    .join(' ');
}

export const editFullWidth = 560;

export let letterEventEmitter = new EventEmitter();
