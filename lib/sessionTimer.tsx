import React from 'react';
import { siteConfig } from './siteConfig';
import { translate } from './i18n/translator';

interface SessionExpire {
  seconds: number;
  updateTime: number;
}

interface SessionTimerProps {
  sessionExpire: SessionExpire;
  warning(openSessionDialog: boolean);
  stop();
  timeoutSeconds(secconds: string);
}

export class SessionTimer extends React.Component<SessionTimerProps, { dispTimer?: string; warningTime: number }> {
  constructor(props) {
    super(props);

    this.state = { dispTimer: '', warningTime: 60 };
  }

  componentWillMount() {
    if (siteConfig && siteConfig.settings && siteConfig.settings.warningTime && siteConfig.settings.warningTime > 0)
      this.setState({ warningTime: siteConfig.settings.warningTime });
  }

  private timer;

  tick = () => {
    if (this.props.sessionExpire && this.props.sessionExpire.seconds && this.props.sessionExpire.updateTime) {
      const diff: number = (Date.now() - this.props.sessionExpire.updateTime) / 1000;
      const duration_sec: number = this.props.sessionExpire.seconds - diff;
      const hours: number = (duration_sec / 3600) | 0;
      const minutes: number = (duration_sec / 60 - hours * 60) | 0;
      const seconds: number = duration_sec % 60 | 0;
      const disp: string =
        hours + ' : ' + (minutes < 10 ? '0' + minutes : minutes) + ' : ' + (seconds < 10 ? '0' + seconds : seconds);

      if (duration_sec <= 1) {
        this.props.stop();
      } else if (hours == 0 && minutes == 0) {
        this.setState({ dispTimer: translate('mainbar_sessionWillExpire') + ' ' + disp });
        this.props.timeoutSeconds(disp);
        this.props.warning(duration_sec <= this.state.warningTime);
      }
    } else if (this.state.dispTimer !== '') {
      this.setState({ dispTimer: '' });
    }
  };

  componentDidMount() {
    this.timer = setInterval(this.tick, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    return <div>{false && this.state.dispTimer}</div>;
  }
}
