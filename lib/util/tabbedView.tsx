import * as React from 'react';
import * as mui from 'material-ui';
import { translate } from '../i18n/translator';
import { getTabsConfig } from '../siteConfig';
import { HomeWidget } from '../layout/views';

export interface PrimaryTab {
  labelCode: string;
  data: JSX.Element | React.Component<any, any>;
}

export interface OptionalTab {
  labelCode: string;
  data: JSX.Element | React.Component<any, any>;
}

export interface TabbedViewData {
  homeWidgetLabel: string;
  primaryTab: PrimaryTab;
  optionalTabs?: { [name: string]: OptionalTab };
}

// TODO write documentation for this class
export abstract class TabbedView<P, S> extends React.Component<P, S> {
  private tabsConfig;
  private initializedTabsData: TabbedViewData;

  constructor() {
    super();
    this.tabsConfig = getTabsConfig(this);
  }

  abstract initTabsData(): TabbedViewData;
  abstract shouldRenderTabs(): boolean;

  private doInit() {
    this.initializedTabsData = this.initTabsData();
    this.doFilterTabsData();
  }

  private doFilterTabsData() {
    if (this.tabsConfig) {
      const disabledTabs = this.tabsConfig.disabledTabs;

      disabledTabs.forEach(disabledTab => {
        if (this.initializedTabsData.optionalTabs[disabledTab]) {
          delete this.initializedTabsData.optionalTabs[disabledTab];
        }
      });
    }
  }

  private getTabs() {
    if (!this.initializedTabsData) {
      this.doInit();
    }

    return this.initializedTabsData;
  }

  private isRenderTabs() {
    if (!this.shouldRenderTabs()) {
      return false;
    }

    if (!this.initializedTabsData.optionalTabs || Object.keys(this.initializedTabsData.optionalTabs).length == 0) {
      return false;
    }

    return true;
  }

  render() {
    const tabs = this.getTabs();
    let dataToRender;

    if (this.isRenderTabs()) {
      let tabsToRender = [];

      tabsToRender.push(
        <mui.Tab label={translate(tabs.primaryTab.labelCode)} key={tabs.primaryTab.labelCode}>
          {tabs.primaryTab.data}
        </mui.Tab>,
      );

      if (tabs.optionalTabs) {
        const optionalTabKeys = Object.keys(tabs.optionalTabs);

        optionalTabKeys.forEach(optionalTabKey => {
          const tab = tabs.optionalTabs[optionalTabKey];

          tabsToRender.push(
            <mui.Tab label={translate(tab.labelCode)} key={tab.labelCode}>
              {tab.data}
            </mui.Tab>,
          );
        });
      }

      dataToRender = <mui.Tabs>{tabsToRender}</mui.Tabs>;
    } else {
      dataToRender = tabs.primaryTab.data;
    }

    return <HomeWidget title={translate(tabs.homeWidgetLabel)}>{dataToRender}</HomeWidget>;
  }
}
