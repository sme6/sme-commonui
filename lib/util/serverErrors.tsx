import * as React from 'react';
import { translate } from '../i18n/translator';

export function errorMessage(e, handler?: (value: string) => void) {
  let objValue;
  let details = e.message;
  let obj: any;

  try {
    obj = details ? JSON.parse(details) : {};
  } catch (e) {
    details = null;
  }

  if (obj && obj.value) {
    if (handler) {
      handler(obj.value);
    } else {
      this.setState({ errorMessage: obj.value });
    }
    return;
  }

  const validationErrors = details ? obj.fieldErrors : null;

  if (validationErrors) {
    objValue = '';
    validationErrors.forEach(element => {
      objValue = objValue + ' ' + translate(element.message);
    });
  } else {
    if (details == null && e.status != null) {
      obj.value = `${e.status} ${e.statusText}`;
    } else if (obj) {
      obj.value = translate(obj.message);
    }
    objValue = obj ? obj.value || 'Server error!!!' : '';
  }

  if (handler) {
    handler(objValue);
  } else {
    this.setState({
      errorMessage: objValue,
    });
  }
}

interface PropsErrorComponent {
  errorMessage: string;
  onClose?(): void;
}

export class GlobalErrorComponent extends React.Component<PropsErrorComponent, {}> {
  constructor(props) {
    super(props);
  }

  render() {
    const errorMessage = this.props.errorMessage;
    const plashka =
      errorMessage && errorMessage.length > 0 ? (
        <div
          onClick={() => (this.props.onClose ? this.props.onClose() : null)}
          style={{
            cursor: 'pointer',
            borderBottom: '1px solid rgb(212, 212, 212)',
            padding: '10px 20px 10px 20px',
            backgroundColor: 'rgba(239,75,50,0.7)',
            color: '#fff',
          }}>
          {this.props.errorMessage}
        </div>
      ) : null;

    return <div>{plashka}</div>;
  }
}
