import { blue500 } from 'material-ui/styles/colors';
import { red500 } from 'material-ui/styles/colors';
import { orange500 } from 'material-ui/styles/colors';
import { translate } from '../i18n/translator';
import * as validator from '../validator';

/**
 @deprecated use 'common/validator.tsx', see README.md
 */

export function isNumeric(numb) {
  return numb.match(/^[\d]+$/);
}
export function validLength(value: string, length) {
  if (value == null || value === '') {
    return translate('common_mandatory_field');
  }
  if (value.length > length) {
    return length + ' ' + translate('paymentOrder_valid_lengthLimit');
  }
  return '';
}

export function validDate(value: Date) {
  if (value == null) {
    return translate('common_mandatory_field');
  }
  return '';
}

export function validAmount(value) {
  if (value == null || value === '' || value === 0) {
    return false;
  }
  if (!value.match(new RegExp('^[1-9][0-9]*(.?[0-9]{0,2})+$'))) {
    return false;
  }
  return true;
}

export function validAccount1(value) {
  if (value.number == null || value.number === '') {
    return translate('common_mandatory_field ');
  }
  return '';
}

export function validAccount2(value) {
  value.replace(/ /g, '');
  value = value.toUpperCase();

  if (value == null || value === '') {
    return translate('common_mandatory_field');
  }
  let res = '';
  if (!value.match(new RegExp('^[a-zA-Z0-9]+$'))) {
    res += translate('paymentOrder_valid_invalidCharacters') + ' .';
  }
  if (value.length !== 20) {
    res += ` ` + translate('paymentOrder_valid_20chars');
  }
  if (!value.match(new RegExp('^[KZ]'))) {
    res += ' ' + translate('paymentOrder_valid_firstCharsKZ');
  }

  if (iso7064Mod97_10(iso13616Prepare(value)) !== 1) {
    res += ' ' + translate('paymentOrder_valid_wrongFormat');
  }

  return res;
}

function iso13616Prepare(iban) {
  iban = iban.toUpperCase();
  iban = iban.substr(4) + iban.substr(0, 4);
  return iban
    .split('')
    .map(function(n) {
      return '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'.indexOf(n);
    })
    .join('');
}

function iso7064Mod97_10(iban) {
  let remainder = iban,
    block;

  while (remainder.length > 2) {
    block = remainder.slice(0, 9);
    remainder = (parseInt(block, 10) % 97) + remainder.slice(block.length);
  }

  return parseInt(remainder, 10) % 97;
}

export function validTaxCode(value: string) {
  if (value == null || value === '') {
    return translate('common_mandatory_field');
  }
  if (!value.match(new RegExp('^[0-9]+$'))) {
    return translate('paymentOrder_valid_invalidCharacters');
  }

  const weight1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
  const weight2 = [3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2];

  function controlSum(weight) {
    let r = 0;

    for (let i = 0; i < weight.length; i++) {
      r += weight[i] * parseInt(value[i]);
    }

    return r % 11;
  }

  let cs = controlSum(weight1);

  if (cs === 10) {
    cs = controlSum(weight2);
  }

  return '' + cs === value[11] ? `` : translate('paymentOrder_valid_wrongCode');
}

export function validLocalBankBik(value) {
  let res = '';
  if (value == null || value === '') {
    return translate('common_mandatory_field');
  }
  if (!value.match(new RegExp('^[A-Z0-9]+$'))) {
    res += ' ' + translate('paymentOrder_valid_invalidCharacters') + ' .';
  }
  if (value.length > 11) {
    res += ' 11 ' + translate('paymentOrder_valid_lengthLimit');
  }
  return res;
}

export function validPurposeCode(value: string) {
  let res = '';

  if (value == null || value === '') {
    return translate('common_mandatory_field');
  }
  if (value.length > 3) {
    res += ' 3 ' + translate('paymentOrder_valid_lengthLimit');
  }
  if (!value.match(new RegExp('^[0-9]+$'))) {
    res += ' ' + translate('paymentOrder_valid_invalidCharacters') + ' .';
  }
  return res;
}

export function validBudgetCode(value: string) {
  if (value == null || value === '') {
    return '';
  }
  let res = '';
  if (value.length > 6) {
    res += ' 6 ' + translate('paymentOrder_valid_lengthLimit');
  }
  if (!value.match(new RegExp('^[0-9]+$'))) {
    res += ' ' + translate('paymentOrder_valid_invalidCharacters') + ' .';
  }
  return res;
}

export function validVinCode(value: string) {
  if (value == null || value === '') {
    return '';
  }
  let res = '';
  if (value.length > 20) {
    res += ' 20 ' + translate('paymentOrder_valid_lengthLimit');
  }
  if (!value.match(new RegExp('^[A-Z0-9]+$'))) {
    res += ' ' + translate('paymentOrder_valid_invalidCharacters') + ' .';
  }
  return res;
}

export function validResidencyCode(value: string) {
  let res = '';
  if (value == null || value === '') {
    return translate('common_mandatory_field');
  }
  if (value.length > 2) {
    res += ' 2 ' + translate('paymentOrder_valid_lengthLimit');
  }
  if (!value.match(new RegExp('^[1-2]+[0-9, A]'))) {
    res += ' ' + translate('paymentOrder_valid_wrongFormat');
  }
  return res;
}

export function isEmpty(val: any) {
  if (val == null || val === undefined || val === false) {
    return true;
  }

  if (typeof val === 'string' && val.trim().length === 0) {
    return true;
  }
  return false;
}
const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export function validateEmail(email: string) {
  return re.test(email);
}

export const VALIDATION_STYLES = {
  warningStyle: {
    color: blue500,
  },
  errorStyle: {
    color: red500,
  },
  defaultStyle: {
    color: orange500,
  },
};

export const floatingLabelStyle = (state, fieldErrors, fieldName) => {
  if (fieldErrors[fieldName]) {
    return VALIDATION_STYLES.errorStyle;
  }

  const warning = fieldName + 'Warning';

  if (state[warning]) {
    return VALIDATION_STYLES.warningStyle;
  }

  return null;
};

export const cropText = (e, fieldName, constraints, isRegexpActive?: boolean) => {
  let maxLength = null;
  let message = null;
  let value = e.target === undefined ? e : e.target.value === undefined ? e.target : e.target.value;
  const state = {};
  const warning = fieldName + 'Warning';

  if (constraints[fieldName] !== undefined) {
    maxLength = constraints[fieldName].maxLength;
    message = validator.message({ maxLength }, value);

    if (isRegexpActive === true) {
      const regexp = constraints[fieldName].regexp;
      const messageRegexp = validator.message({ regexp }, value);

      if (messageRegexp) {
        return {};
      }
    }
  }

  if (message) {
    value = value.substring(0, maxLength);
    state[warning] = message;
  } else {
    state[warning] = '';
  }

  state[fieldName] = value;
  return state;
};

export function validAccountNumber(value) {
  value.replace(/ /g, '');
  value = value.toUpperCase();

  if (value == null || value === '') {
    return translate('common_mandatory_field');
  }
  let res = '';
  if (!value.match(new RegExp('^[a-zA-Z0-9]+$'))) {
    res += translate('paymentOrder_valid_invalidCharacters') + ' .';
  }
  if (value.length !== 20) {
    res += ` ` + translate('paymentOrder_valid_20chars');
  }
  if (!value.match(new RegExp('^[KZ]'))) {
    res += ' ' + translate('paymentOrder_valid_firstCharsKZ');
  }

  if (iso7064Mod97_10(iso13616Prepare(value)) !== 1) {
    res += ' ' + translate('paymentOrder_valid_wrongFormat');
  }

  return res;
}
