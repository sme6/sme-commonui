import * as React from 'react';
import { FieldConfig, getFormConfig } from '../siteConfig';

// Class is used to enable form customization.
// TODO write documentation
export abstract class CustomizableForm<F, P, S> extends React.Component<P, S> {
  private formConfig;
  private initializedFormData: { [fieldName: string]: F } = null;

  constructor() {
    super();
    this.formConfig = getFormConfig(this);
  }

  abstract initFormData(): { [fieldName: string]: F };

  // setState is overridden to reset form data object. Theese object should be updated after state changes
  setState<K extends keyof S>(state: Pick<S, K>, callback?: () => any): void {
    this.initializedFormData = null;
    super.setState(state, callback);
  }

  // remove unnecessary fields from rendering queue
  private doFilterFormData(fieldsObj: { [fieldName: string]: F }) {
    if (this.formConfig) {
      Object.keys(this.formConfig).forEach(configFieldName => {
        let fieldConfig = this.formConfig[configFieldName] as FieldConfig;

        if (fieldConfig.hidden) {
          delete fieldsObj[configFieldName];
        }
      });
    }
  }

  private doInit() {
    this.initializedFormData = this.initFormData();
    this.doFilterFormData(this.initializedFormData);
  }

  private getFormData(fieldNames?: Array<string>): { [fieldName: string]: F } {
    if (!this.initializedFormData) {
      this.doInit();
    }

    // avoid mutating source object. Just copying data from source to empty object
    let fieldsObject;

    if (fieldNames && fieldNames.length > 0) {
      const fieldsObject = {};

      let fieldKeys = Object.keys(this.initializedFormData);

      fieldKeys.forEach(fieldKey => {
        if (this.initializedFormData[fieldKey]) {
          fieldsObject[fieldKey] = this.initializedFormData[fieldKey];
        }
      });
    } else {
      fieldsObject = Object.assign({}, this.initializedFormData);
    }

    return fieldsObject;
  }

  // builds initial form state based on formConfig default values.
  getDefaultFieldsState() {
    // processing form config and retrieving default field values
    if (this.formConfig) {
      let defaultFormState = {};

      // iterating through filed configurations on form config and looking for available default values
      Object.keys(this.formConfig).forEach(fieldKey => {
        let field = this.formConfig[fieldKey] as FieldConfig;

        if (field && field.defaultValue) {
          defaultFormState[fieldKey] = field.defaultValue;
        }
      });

      return defaultFormState;
    }
  }

  // checks whether field is disabled or not. This method should be used in subclasses
  // NOTE: this field must be used manually for every element. Model fieldName must be supplied
  isFieldDisabled(fieldName: string): boolean {
    return this.formConfig && this.formConfig[fieldName] && (this.formConfig[fieldName] as FieldConfig).disabled;
  }

  // get fields object for future rendering.
  getFormFieldsAsObject(fieldNames?: Array<string>): { [fieldName: string]: F } {
    return this.getFormData(fieldNames);
  }

  // get filtered (optional) fields array.
  getFormFieldsAsArray(fieldNames?: Array<string>): Array<F> {
    const formFieldsObj = this.getFormData(fieldNames);
    const formFieldsObjKeys = Object.keys(formFieldsObj);

    let fieldsArray = formFieldsObjKeys.map(key => {
      return formFieldsObj[key];
    });

    return fieldsArray;
  }
}
