const DEFAULT_VALUE = '';

export const RENDER_VALUE_UTIL = (val: any) => {
  if (val) {
    let trimmedValue = val.trim();

    if (trimmedValue === 'undefined' || trimmedValue === 'null') {
      return DEFAULT_VALUE;
    } else {
      return trimmedValue;
    }
  } else {
    DEFAULT_VALUE;
  }
};
