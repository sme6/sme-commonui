import { JSEncrypt } from 'jsencrypt';
import { get } from '../backend';

interface PKey {
  key: string;
}

export async function loadPublicKey() {
  let key: string;
  await get('/customer/key').then(
    (response: PKey) => {
      key = response.key;
    },
    e => {
      console.error(e);
      key = null;
    },
  );
  sessionStorage.setItem('publicKey', key);
}

export function encrypt(message) {
  const publicKey = sessionStorage.getItem('publicKey');
  const encrypt = new JSEncrypt();
  encrypt.setPublicKey(publicKey);
  return encrypt.encrypt(message);
}
