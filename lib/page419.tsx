import * as React from 'react';
import * as mui from 'material-ui';
import { Link } from 'react-router';
import { Content, Header } from './layout/views';
import { translate } from './i18n/translator';

export class Page419 extends React.Component<any, any> {
  constructor() {
    super();
  }

  render() {
    return (
      <mui.Paper>
        <Header text={translate('errorpage419_title', 'Session blocked')} />

        <Content>
          {translate('errorpage419_message', 'Чтобы войти в систему, нажмите') + ' '}
          <Link to="/login"> {translate('login_enter', 'Enter')}</Link>
        </Content>
      </mui.Paper>
    );
  }
}
