import * as React from 'react';
import moment from 'moment';

import { Code } from './types';

//
// Dates
//
/**
 * @deprecated dateformat doesn't support parsing dates;
 * 1) make this field private
 * 2) use public method to conert to specific format
 *
 */
export let dateFormat = require('dateformat');

export function date(d: Date, nullValue = '-'): string {
  if (!d) {
    return nullValue;
  }

  return moment(d).format('DD.MM.YYYY');
}

export function dateField(d: Date): string {
  return date(d, '');
}

export function parseDate(s): Date {
  if (!s) {
    return null;
  }
  return moment(s, 'DD.MM.YYYY').toDate();
}

export function time(d: Date, nullValue = '-'): string {
  if (!d) {
    return nullValue;
  }

  return moment(d).format('HH.mm');
}

export function parseTime(s): Date {
  if (!s) {
    return null;
  }
  return moment(s, 'HH.mm').toDate();
}

/**
 * @deprecated format.tsx is for UI-related formatting;
 * for backend formatting put corresponding code into backend.tsx.
 * For example backend.tsx already knows how to convert request params from Date to correct format
 */
export function localDate(date: Date): string {
  return dateFormat(date, 'yyyy-mm-dd');
}

export function dateTime(date: Date): string {
  if (!date) {
    return '-';
  }

  return dateFormat(date, 'dd.mm.yyyy HH:MM');
}

export function shortMonthName(d: Date): string {
  if (!date) {
    return '';
  }

  const s = moment(d)
    .format('MMM YYYY')
    .replace('.', '');

  return s[0].toUpperCase() + s.slice(1);
}

export function monthNominal(d: Date): string {
  if (!date) {
    return '';
  }

  const s = moment(d)
    .format('MMMM')
    .replace('.', '');

  return s[0].toUpperCase() + s.slice(1);
}

export function monthDay(d: Date): string {
  if (!date) {
    return '';
  }

  const s = moment(d)
    .format('MMM DD')
    .replace('.', '');

  return s[0].toUpperCase() + s.slice(1);
}

export function dateTimeFull(date: Date): string {
  if (!date) {
    return '-';
  }

  return dateFormat(date, 'dd.mm.yyyy HH:MM:ss');
}

export function dateTime24(date: Date): string {
  if (!date) {
    return '-';
  }

  return dateFormat(date, 'dd.mm.yyyy HH:MM');
}

export function time24(date: Date): string {
  if (!date) {
    return '-';
  }
  return dateFormat(date, 'HH:MM');
}

export function today(): string {
  return date(new Date());
}

export function dateRange(from: Date, to: Date): string {
  if (!from && !to) {
    return '';
  }

  return dateField(from) + ' - ' + dateField(to);
}

export function parseRangeFrom(s: string): Date {
  if (!s) {
    return null;
  }

  const idx = s.indexOf('-');
  const part = (idx < 0 ? s : s.substring(0, idx)).trim();

  return parseDate(part);
}

export function parseRangeTo(s: string): Date {
  if (!s) {
    return null;
  }

  const idx = s.indexOf('-');
  if (idx < 0) {
    return null;
  }

  const part = s.substring(idx + 1).trim();

  return parseDate(part);
}

export function convertToUTC0(date: Date) {
  if (date == null || date === undefined) {
    return null;
  }
  let res = new Date(date);
  res = new Date(res.setMinutes(res.getMinutes() - res.getTimezoneOffset()));
  return res;
}

export function convertFromUTC0(date: Date) {
  if (date == null || date === undefined) {
    return null;
  }
  let res: Date = new Date(date);
  res = new Date(res.setMinutes(res.getMinutes() + res.getTimezoneOffset()));
  return res;
}

//
// Amount
//

// let BigNumber = require('bignumber.js');

/**
 * @param integer n: length of fieldDecimal
 * @param integer x: length of whole part
 * @param mixed   s: sections delimiter
 * @param mixed   c: fieldDecimal delimiter
 */
export function numFormat(numb, n, x, s, c) {
  const re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')';
  const num = Number(numb).toFixed(Math.max(0, ~~n));

  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
}

export function amount(n: number, currency: Code): string {
  if (!n) {
    return '-';
  }

  return numFormat(n, 2, 3, ' ', ',') + ' ' + currency.label;
}

export function amountNoSeparators(n: number, precision = 2) {
  if (!n) {
    return '';
  }

  return numFormat(n, precision, 3, '_', ',').replace(/_/g, '');
}

export function optionalAmount(n: number): string {
  if (!n) {
    return '';
  }

  return numFormat(n, 2, 3, ' ', ',');
}

export function parseOptionalAmount(s, precision = 2): number {
  if (!s) {
    return null;
  }

  s = s.replace(/,/g, '.').replace(/ /g, '');

  if (s.indexOf('.') === -1 && s.length > 14) {
    s = s.substring(0, 14);
  }

  const parsed = parseFloat(s);
  const roundDecimal = Math.pow(10, precision); // default 100.0

  return (parsed * roundDecimal) / roundDecimal;
}

export function amountClean(n: number, precision = 2): string {
  if (!n) {
    return '0,00';
  }

  return numFormat(n, precision, 3, ' ', ',');
}

const maxAmountLength = 14;
// Method return masked amount in next view 2 100 000,00
// return null if value integral part length greater then integralPartMaxLength
export function convertToMaskedAmount(
  value,
  precision = 2,
  integralPartMaxLength = maxAmountLength,
  lengthLimit = true,
) {
  const decimalSeparator = ',';
  const thousandSeparator = ' ';
  if (precision < 0) {
    precision = 0;
  } // precision cannot be negative
  if (precision > maxAmountLength) {
    precision = maxAmountLength;
  } // precision cannot be greater than maxAmountLength
  if (integralPartMaxLength > maxAmountLength || integralPartMaxLength < 0) {
    integralPartMaxLength = maxAmountLength;
  }

  if (value === null || value === undefined) {
    return '0,00';
  }

  value = String(value);

  value = value.replace(/,/g, '.').replace(/ /g, '');

  if (value.length === 0) {
    return '0,00';
  }

  if (value.length > integralPartMaxLength + precision + 1 && lengthLimit) {
    return null;
  }
  value = Number(value).toFixed(precision);

  const digits = value.match(/\d/g) || ['0'];

  while (digits.length <= precision) {
    digits.unshift('0');
  }

  if (precision > 0) {
    // add the decimal separator
    digits.splice(digits.length - precision, 0, '.');
  }

  let decimalpos = digits.length - precision - 1; // -1 needed to position the decimal separator before the digits.
  if (precision > 0) {
    // set the final decimal separator
    digits[decimalpos] = decimalSeparator;
  } else {
    // when precision is 0, there is no decimal separator.
    decimalpos = digits.length;
  }

  // add in any thousand separators
  for (let x = decimalpos - 3; x > 0; x = x - 3) {
    digits.splice(x, 0, thousandSeparator);
  }
  return digits.join('').trim();
}

export function setMaskedAmount(value, setAmountFunction: (value) => void, precision = 2, integralPartMaxLength = 14) {
  const res = convertToMaskedAmount(value, precision, integralPartMaxLength);

  if (res) {
    setAmountFunction(res);
  }
}

export function decodeMaskedAmount(value) {
  if (!value) {
    return 0;
  }

  if (typeof value === 'number') {
    value = value.toString();
  }

  return value.replace(/,/g, '.').replace(/ /g, '');
}

//
// Other
//

export function formatPhone(s: string): string {
  if (!s) {
    return '';
  }

  const g1 = s.substring(1, 2);
  const g2 = s.substring(2, 5);
  const g3 = s.substring(5, 8);
  const g4 = s.substring(8, 10);
  const g5 = s.substring(10, 12);

  return `+${g1} (${g2}) ${g3} ${g4} ${g5}`;
}

export function highlight(s: string, term: string): React.ReactFragment {
  if (s && term) {
    s = '' + s;

    const parts = s.split(new RegExp(term, 'i'));

    if (parts.length === 1) {
      return parts[0];
    }

    const r = [];

    let index = 0;

    for (const part of parts) {
      if (r.length > 0) {
        const matched = s.substr(index, term.length);
        r.push(
          <span className="hlt" key={index}>
            {matched}
          </span>,
        );

        index += term.length;
      }

      r.push(part);
      index += part.length;
    }

    return r;
  }

  return s;
}

export function changePeriod(state, period) {
  if (state === period) {
    return '';
  } else {
    return period;
  }
}

export function dateFrom(s) {
  if (!s) {
    return null;
  }

  const idx = s.indexOf('-');
  const text = (idx < 0 ? s : s.substring(0, idx)).trim();
  return format(text);
}

export function dateTo(s) {
  if (!s) {
    return null;
  }

  const idx = s.indexOf('-');
  if (idx < 0) {
    return null;
  }

  const text = s.substring(idx + 1).trim();
  return format(text);
}

function format(text) {
  const d = text.substring(0, 2);
  const m = text.substring(3, 5);
  const y = text.substring(6, 11);
  return `${y}-${m}-${d}`;
}

export function transliterate(text) {
  //function to put latin words in apostrophe(')
  if (!text) return '';

  let newText = '';
  let currentLatin = false;
  let lastLetter = '';

  text.split('').forEach(letter => {
    if (/[a-zA-Z]/.test(letter) && !currentLatin && letter !== "'") {
      if (lastLetter === "'") {
        newText = newText.trim();
        newText = newText.substring(0, newText.length - 1) + ` ${letter}`;
      } else {
        newText += `'${letter}`;
      }
      currentLatin = true;
      lastLetter = letter;
    } else if (!/[a-zA-Z]/.test(letter) && currentLatin && letter !== "'" && letter !== '"') {
      newText += `'${letter}`;
      currentLatin = false;
      if (letter !== ' ') {
        lastLetter = letter;
      } else {
        lastLetter = "'";
      }
    } else if (letter !== "'" && letter !== '"') {
      newText += letter;
      lastLetter = letter !== ' ' ? letter : '';
    }
  });

  if (currentLatin) newText += "'";
  return newText;
}
