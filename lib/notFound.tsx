import * as React from 'react';
import { Page, Header, Content } from './layout/views';

export class NotFound extends React.Component<any, any> {
  constructor() {
    super();
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <Page>
        <Header>
          <span style={{ fontSize: '2em' }} />
        </Header>
        <Content>
          <div style={{ margin: 'auto', textAlign: 'center', fontSize: 26, paddingTop: 100 }}>
            Доступ к сервису временно ограничен администратором системы
          </div>
        </Content>
      </Page>
    );
  }
}
