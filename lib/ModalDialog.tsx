import * as React from 'react';

import * as mui from 'material-ui';
import { translate } from './i18n/translator';

interface DataChangeProps {
  open: boolean;
  title?: string;
  handleYes();
  handleNo();
}
interface DataChangeState {
  open?: boolean;
  requiredTextCode?: string;
}
export default class MDialog extends React.Component<DataChangeProps, DataChangeState> {
  state = {
    open: false,
  };

  getTitle(title: string) {
    if (title && title.trim().length > 0) {
      return title;
    } else {
      return 'Запрос подтверждения действия';
    }
  }
  render() {
    const actions = [
      <mui.FlatButton label={translate('common_cancel', 'Отменить')} primary={true} onTouchTap={this.props.handleNo} />,
      <mui.FlatButton
        label={translate('common_confirm', 'Подтвердить')}
        primary={true}
        onTouchTap={this.props.handleYes}
      />,
    ];

    return (
      <div>
        <mui.Dialog title={this.getTitle(this.props.title)} actions={actions} modal={true} open={this.props.open} />
      </div>
    );
  }
}
