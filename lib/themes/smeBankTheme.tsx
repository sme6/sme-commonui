import { white, darkBlack, fullBlack } from 'material-ui/styles/colors';
import spacing from 'material-ui/styles/spacing';
import { fade } from 'material-ui/utils/colorManipulator';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

const smeBankTheme = getMuiTheme({
  spacing: spacing,
  fontFamily: 'Signika',
  palette: {
    primary1Color: '#007D94',
    primary2Color: '#78c6ff',
    primary3Color: '#0c7299',
    accent1Color: '#666666',
    accent2Color: '#cccccc',
    accent3Color: '#666666',
    textColor: darkBlack,
    alternateTextColor: white,
    canvasColor: white,
    borderColor: '#dddddd',
    disabledColor: fade(darkBlack, 0.3),
    pickerHeaderColor: '#9C014B',
    clockCircleColor: fade(darkBlack, 0.07),
    shadowColor: fullBlack,
  },
});

export default smeBankTheme;
