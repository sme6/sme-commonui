import fetch from 'isomorphic-fetch';

import { CodeCategory, Option, Code } from './types';
import { siteConfig } from './siteConfig';
import { v4 as uuidv4 } from 'uuid';

// hello world
export const props = {
  apiPrefix: '/api',
  target: process.env.target || '',
  // target: "https://customer-dev-back.anthill.fortebank.com",
  socketTarget: process.env.socketTarget || '',
  ecomWidgetTarget: process.env.ecomWidgetTarget || '',
  vat: 0.18,
  v1: '/v1',
};

export const SS = {
  TOKEN: 'token',
  LOGIN_MODE: 'loginmode',
  QUICK_LINK: 'quick_link',
  AUTH_PERSON: 'authPerson',
  USER_QUICK_LINK: 'user_quick_link',
  VISIBILITY: 'visibility',
  PERSON_TYPE: 'personType',
  LOCALE: 'locale',
};


export function get(resource, params?, headers = {}, options?, skipParseResult?: boolean) {
  return rest('GET', resource, params, undefined, headers, options, skipParseResult);
}

export function post(resource, body = {}, qs?, headers = {}, options?, skipParseResult?: boolean) {
  return rest('POST', resource, qs, body, headers, options, skipParseResult);
}

export function put(resource, body = {}, qs?, headers = {}, options?, skipParseResult?: boolean) {
  return rest('PUT', resource, qs, body, headers, options, skipParseResult);
}

export function del(resource, headers = {}, options?, skipParseResult?: boolean) {
  return rest('DELETE', resource, undefined, undefined, headers, options, skipParseResult);
}

let authToken;

export function setAuthToken(token) {
  authToken = token;
}

async function confirmSuccessResponse(response) {
  if (response.status >= 200 && response.status < 300) {
    const session_expire = { seconds: response.headers.get('session_expire'), updateTime: Date.now() };
    if (session_expire.seconds && session_expire.seconds > 0) {
      sessionExpireCallback(session_expire);
    }
    return response;
  }
  // if (response.status == 400) {
  //     return response;
  // }

  const errorCode = response.headers.get('X-ibankApp-error');
  const errorParamCode = response.headers.get('X-ibankApp-params');

  if (
    response.status === 420 ||
    ((response.status === 401 || response.status === 403) && errorCode && errorCode === '401/1')
  ) {
    logoutCallback();
    if (siteConfig.response && siteConfig.response.code420 && siteConfig.response.code420.redirect) {
      navigate.to(siteConfig.response.code420.redirect);
    } else {
      navigate.to('/login');
    }
  }

  if (response.status === 419 || response.status === 418 || response.status === 401) {
    navigate.to('/login');
  }

  const error: any = new Error((await response.text()) || response.statusText);
  error.code = errorParamCode;
  throw error;
}

async function parseResponse(response, skipParseResult: boolean) {
  const contentType = response.headers.get('content-type');

  const text = await response.text();

  if (!contentType || contentType.indexOf('application/json') === -1) {
    return text;
  }

  

  // convert LocalDate, LocalDateTime to js Date object
  return skipParseResult ? JSON.parse(text) : JSON.parse(text, (key, val) => {
    if (typeof val === 'string') {
    //   if (/(\d{4})-(\d{2})-(\d{2})/.test(val)){
    //     return moment(val).format('YYYY-MM-DD')
    //   }
      // LocalDateTime
      if (/^\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d.\d\d\dZ$/.test(val)) {
        return new Date(val);
      }

      // LocalDate
      if (/^\d\d\d\d-\d\d-\d\d$/.test(val)) {
        return new Date(val);
      }
    }

    return val;
  });
}

function formatParam(param) {
  function pad2(n) {
    if (n < 10) {
      return '0' + n;
    }

    return '' + n;
  }

  if (param instanceof Date) {
    // 2007-12-03 is backend accepted format, see JacksonConfiguration
    return `${param.getFullYear()}-${pad2(1 + param.getMonth())}-${pad2(param.getDate())}`;
  }

  return '' + param;
}

function renderQueryString(qs) {
  if (!qs) {
    return '';
  }

  let r = '';

  Object.keys(qs).forEach(key => {
    if (qs[key] == null) {
      return;
    }

    if (r !== '') {
      r += '&';
    }

    r += `${key}=${encodeURIComponent(formatParam(qs[key]))}`;
  });

  return `?${r}`;
}

function getCookie(name) {
  const matches = document.cookie.match(
    new RegExp('(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'),
  );
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

export function rest(method, url, queryString, body, headers = {}, options?, skipParseResult?: boolean): Promise<any> {
  if (!url.startsWith('/')) {
    url = '/' + url;
  }

  url = !url.startsWith('//') ? props.apiPrefix + props.v1 + url : url.substring(1);

  url = props.target + url + renderQueryString(queryString);

  if (typeof body === 'object' || typeof body === 'number') {
    // JSON request
    if (!(body instanceof FormData)) {
      headers['Content-Type'] = 'application/json';
      body = JSON.stringify(body);
    }
  }

  if (authToken) {
    headers['Authorization'] = `Bearer ${authToken}`;
  }

  const chosenLang = getCookie('TRANSLATE_LANGUAGE') || 'ru';
  if (chosenLang) {
    headers['Translate-Language'] = chosenLang;
  }

  headers['request_id'] = uuidv4();

  return new Promise((resolve, reject) => {
    fetch(url, {
      method,
      body,
      headers,
      cache: 'no-cache',
      credentials: 'same-origin',
      ...options,
    })
      .then(confirmSuccessResponse)
      .then(response => parseResponse(response, skipParseResult))
      .then(response => resolve(response))
      .catch(error => reject(error));
  });
}

export function clearCodeStore() {
  Object.keys(sessionStorage)
    .filter(key => key.startsWith('storeOp.') || key.startsWith('store.'))
    .forEach(key => sessionStorage.removeItem(key));
}

export function getCodeOptions(category: CodeCategory): Promise<Option[]> {
  return new Promise((resolve, reject) => {
    const result: string = sessionStorage.getItem('storeOp.' + category);
    if (result) {
      resolve(JSON.parse(result));
    } else {
      get(`/codes/options/${CodeCategory[category]}`)
        .then(response => {
          sessionStorage.setItem('storeOp.' + category, JSON.stringify(response));
          return response;
        })
        .then(response => resolve(response))
        .catch(error => reject(error));
    }
  });
}

export function getCodes(category: CodeCategory): Promise<Code[]> {
  return new Promise((resolve, reject) => {
    const result: string = sessionStorage.getItem('store.' + category);
    if (result) {
      resolve(JSON.parse(result));
    } else {
      get(`/codes/by_cat/${CodeCategory[category]}`)
        .then(response => {
          sessionStorage.setItem('store.' + category, JSON.stringify(response));
          return response;
        })
        .then(response => resolve(response))
        .catch(error => reject(error));
    }
  });
}

export class DocumentView {
  id: number;
  number: string;
  type: Code;
  created: any;
  state: Code;
  manager: string;
  bankResponse: string;

  constructor(...inits: Partial<DocumentView>[]) {
    Object.assign(this, ...inits);
  }
}

export function getDocument(documentId): Promise<DocumentView> {
  return get(`/customer/documents/${documentId}`) as Promise<DocumentView>;
}

let logoutCallback = () => {};

export function setLogoutCallback(f) {
  logoutCallback = f;
}

let sessionExpireCallback = session_expire => {};

export function setSessionExpireTime(f) {
  sessionExpireCallback = f;
}
