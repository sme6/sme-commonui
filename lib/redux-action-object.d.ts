// Type definitions for redux-action-object v0.1.2
// Project: https://github.com/vasyas/redux-action-object

declare module 'redux-action-object' {
  export function bind(actionCreators, dispatch);
  export function split(object);
  export function sideEffect(f);
  export function withSideEffects(next);
  export function creator(target: any, name: string, descriptor: PropertyDescriptor);
}
