import * as React from 'react';
import * as mui from 'material-ui';
import { Link } from 'react-router';
import { translate } from './i18n/translator';
import { Content, Header } from './layout/views';

export class Page418 extends React.Component<any, any> {
  constructor() {
    super();
  }

  render() {
    return (
      <mui.Paper>
        <Header text={translate('errorpage418_title')} />

        <Content>
          {translate('errorpage418_message', 'Чтобы войти в систему, нажмите') + ' '}
          <Link to="/login"> {translate('login_enter', 'Enter')}</Link>
        </Content>
      </mui.Paper>
    );
  }
}
